﻿using GameAPI.Packets;
using GameServer.Configs;
using GameServer.Game;
using GameServer.Game.Definitions;
using GameServer.Networking;
using System;
using System.Diagnostics;
using System.Management;
using System.Net;
using System.Threading;
using Tarul;
using WebSocketSharp.Server;
using System.Linq;
using GameAPI;
using System.Reflection;
using Services.SSH;

namespace GameServer
{

    public class TerminalServer
    {
        public static TerminalServer Singleton { get; private set; } = new TerminalServer();

        private static object logLock = new object();

        public PerformanceCounter CpuCounter { get; private set; }

        private volatile bool exit;


        public readonly AutoResetEvent ExitWait = new AutoResetEvent(false);
        public readonly AutoResetEvent InitialisedWait = new AutoResetEvent(false);

        private WebSocketServer webSocket;

        private IGameWorld gameWorld;

         /// <summary>
        /// Initialises the server
        /// </summary>
        private void Initialise(params string[] args)
        {
            Log.OnLogMessage += Log_OnLogMessage;

            AppDomain.CurrentDomain.FirstChanceException += (sender, eventArgs) =>
            {
                Log.Exception(eventArgs.Exception);
            };

            AppDomain.MonitoringIsEnabled = true;

            new WindowsHardwareInfo((int)LowLevelHelper.GetProcessorStat<uint>("MaxClockSpeed"), (int)LowLevelHelper.GetProcessorStat<uint>("NumberOfCores"));           

            CpuCounter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName);

            System.IO.Directory.CreateDirectory("logs");
            System.IO.Directory.CreateDirectory("configs");

            Config<DatabaseConfig>.Load("configs/DatabaseConfig.json");
            Config<ServerConfig>.Load("configs/ServerConfig.json");

            Log.Info("Registering packets");
            PacketSystem.Initialise(typeof(EnterWorldPacket).Assembly);

            Log.Info("Loading database");
            if (!Database.Initialise())
            {
                Stop();
                return;
            }

            Log.Info("Initialising computer management");
            ComputerManager.Initialise();

            Log.Info("Initialising services");
            ServiceManager.Initialise(Assembly.GetEntryAssembly());
            ServiceManager.Initialise(typeof(IRCService).Assembly);

            Log.Info("Loading game world");
            gameWorld = new GameWorld();
            gameWorld.Start();

            var serverConfig = Config<ServerConfig>.Instance;

            Log.Info($"Starting web socket server at address {serverConfig.Address} on port '{serverConfig.Port}' at service path '{serverConfig.ServicePath}'");
            webSocket = new WebSocketServer(IPAddress.Parse(serverConfig.Address), serverConfig.Port, serverConfig.Secure);
            webSocket.AddWebSocketService<TerminalWebSocketConnection>(serverConfig.ServicePath);
            webSocket.Start();

            InitialisedWait.Set();

            Log.Info("Server started!");
        }

        private void Log_OnLogMessage(Log.Level level, ConsoleColor tagColor, ConsoleColor textColor, string message)
        {
            lock (logLock)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(DateTime.Now);

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" - ");

                Console.ForegroundColor = tagColor;
                Console.Write($"[{level}] ");

                Console.ForegroundColor = textColor;
                Console.WriteLine(message);

                Console.ForegroundColor = ConsoleColor.White;

                System.IO.Directory.CreateDirectory($"logs/{DateTime.Now.ToShortDateString().Replace('/', '-')}");

                System.IO.File.AppendAllText($"logs/{DateTime.Now.ToShortDateString().Replace('/', '-')}/{level.ToString()}.log", $"{DateTime.Now.ToLongTimeString()} - [{level}] {message}\n");
            }
        }

        /// <summary>
        /// Processes a frame
        /// </summary>
        public void Update(double delta)
        {
            var serverConfig = Config<ServerConfig>.Instance;

            if (webSocket.WebSocketServices.TryGetServiceHost(serverConfig.ServicePath, out var host))
            {
                int open = 0;
                int connecting = 0;
                int closed = 0;
                int closing = 0;

                foreach (var session in host.Sessions.Sessions)
                {
                    if (session.State == WebSocketSharp.WebSocketState.Connecting)
                        connecting++;
                    if (session.State == WebSocketSharp.WebSocketState.Open)
                        open++;

                    if (session.State == WebSocketSharp.WebSocketState.Closing)
                        closing++;
                    if (session.State == WebSocketSharp.WebSocketState.Closed)
                        closed++;
                }

                Console.Title = $"Terminal - Game Server # Sessions({host.Sessions.Count}) Connecting({connecting}) Open({open}) Closing({closing}) Closed({closed})";
            }

            WindowsHardwareInfo.CurrentProcessMhzUsage = (CpuCounter.NextValue() / 100f) * WindowsHardwareInfo.MaxClockSpeed;
        }

        /// <summary>
        /// Shuts down this server
        /// </summary>
        private void Shutdown()
        {
            Log.Info("Stopping server...");

            webSocket?.Stop();
            Database.Shutdown();

            Log.Info("Server successfully stopped!");

            ExitWait.Set();
        }

        /// <summary>
        /// Runs the server
        /// </summary>
        public void Run(params string[] args)
        {
            // Initialise
            Initialise(args);

            // Loop until it's time to quit
            FrameScheduler.Start(1);
            while (!exit)
            {
                FrameScheduler.Dispatch(Update);
            }

            // Shut down
            Shutdown();
        }

        public void Stop()
        {
            exit = true;
        }
    }
}

