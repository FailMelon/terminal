﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Configs
{
    public class ServerConfig : IConfig
    {
        public string Address = "127.0.0.1";
        public ushort Port = 27019;
        public string ServicePath = "/terminal_game";
        public bool Secure = false;
    }
}
