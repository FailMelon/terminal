﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Configs
{
    public class AdminAccount
    {
        public string Username, Password;
    }

    public class DatabaseConfig : IConfig
    {
        public string Address = "192.241.154.86";
        public ushort Port = 27017;
        public string AuthenticationDatabase = "terminal-dev";
        public AdminAccount AdminAccount = new AdminAccount
        {
            Username = "terminal",
            Password = ""
        };

    }
}
