﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public static class LowLevelHelper
    {
        public static T GetProcessorStat<T>(string stat)
        {
            var searcher = new ManagementObjectSearcher($"select {stat} from Win32_Processor");

            T data = default;
            foreach (var item in searcher.Get())
                data = (T)item[$"{stat}"];

            return data;
        }

    }
}
