﻿using GameAPI;
using System;
using System.Runtime.InteropServices;

namespace GameServer
{
    class Program
    {
        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        private static EventHandler _handler;


        private static bool Handler(CtrlType sig)
        {
            Log.Warning("Application exit detected, attempting to shutdown properly before closing.");

            TerminalServer.Singleton.Stop();

            TerminalServer.Singleton.ExitWait.WaitOne();

            return true;
        }


        static void Main(string[] args)
         {
            Console.Title = "Terminal - Game Server";

            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            TerminalServer.Singleton.Run();
        }
    }
}
