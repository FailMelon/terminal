﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public interface IWhitelistBatch : IDisposable
    {
        void AllowNamespaceOfTypes(params Type[] types);

        void AllowTypes(params Type[] types);

        void AllowMembers(params MemberInfo[] members);
    }
}
