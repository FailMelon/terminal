﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    /// <summary>
    /// Determines what quantity a given type key should represent (see individual members)
    /// </summary>
    internal enum TypeKeyQuantity
    {
        /// <summary>
        /// No quantity
        /// </summary>
        None,

        /// <summary>
        /// This specific member only
        /// </summary>
        ThisOnly,

        /// <summary>
        /// This and all nested members
        /// </summary>
        AllMembers,
    }
}
