﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using System.Reflection;
using GameServer.Collections;
using System.IO;
using System.Collections.Immutable;
using GameAPI;

namespace GameServer.Scripting
{
    public enum TErrorSeverity
    {
        Notice = 0, //Definitions overriding
        Warning = 1, // Missing file extension
        Error = 2,   // Missing model
        Critical = 3,// Broken definition, other things which cause mod not to load 
    }

    public class ScriptOutOfRangeException : Exception
    {
    }

    public class ProgramCompiler
    {
        public struct Message
        {
            public readonly TErrorSeverity Severity;
            public readonly string Text;

            public Message(TErrorSeverity severity, string text) : this()
            {
                Severity = severity;
                Text = text;
            }
        }

        readonly List<MetadataReference> m_metadataReferences = new List<MetadataReference>();
        readonly ProgramWhitelist m_whitelist;
        readonly CSharpCompilationOptions m_debugCompilationOptions;
        readonly CSharpCompilationOptions m_runtimeCompilationOptions;
        readonly WhitelistDiagnosticAnalyzer m_WhitelistDiagnosticAnalyzer;
        readonly HashSet<string> m_assemblyLocations = new HashSet<string>();
        readonly HashSet<string> m_implicitScriptNamespaces = new HashSet<string>();
        readonly HashSet<string> m_ignoredWarnings = new HashSet<string>();
        readonly HashSet<Type> m_unblockableIngameExceptions = new HashSet<Type>();
        readonly HashSet<string> m_conditionalCompilationSymbols = new HashSet<string>();
        readonly CSharpParseOptions m_conditionalParseOptions;

        public ProgramCompiler()
        {
            AddReferencedAssemblies(
               typeof(int).Assembly.Location,
               typeof(IComputer).Assembly.Location,
               typeof(System.Collections.ObjectModel.ObservableCollection<>).Assembly.Location
            );

            AddImplicitIngameNamespacesFromTypes(
                typeof(System.Object)
            );

            AddUnblockableIngameExceptions(typeof(ScriptOutOfRangeException));

            m_debugCompilationOptions = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).
                WithOptimizationLevel(OptimizationLevel.Debug).
                WithPlatform(Platform.AnyCpu);

            m_runtimeCompilationOptions = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).
                WithOptimizationLevel(OptimizationLevel.Release).
                WithPlatform(Platform.AnyCpu);

            m_whitelist = new ProgramWhitelist(this);
            m_WhitelistDiagnosticAnalyzer = new WhitelistDiagnosticAnalyzer(m_whitelist);
            m_conditionalParseOptions = new CSharpParseOptions();
        }

        /// <summary>
        ///     Gets the assembly locations to be referenced by the scripts
        /// </summary>
        public HashSetReader<string> AssemblyLocations
        {
            get { return m_assemblyLocations; }
        }

        /// <summary>
        ///     Gets the namespaces that are to be added to the ingame script using list
        /// </summary>
        public HashSetReader<string> ImplicitIngameScriptNamespaces
        {
            get { return m_implicitScriptNamespaces; }
        }

        /// <summary>
        ///     Gets the exception types that are to be made unblockable in ingame scripts
        /// </summary>
        public HashSetReader<Type> UnblockableIngameExceptions
        {
            get { return m_unblockableIngameExceptions; }
        }

        /// <summary>
        ///     Gets the conditional compilation symbols scripts are compiled with.
        /// </summary>
        public HashSetReader<string> ConditionalCompilationSymbols
        {
            get { return m_conditionalCompilationSymbols; }
        }

        /// <summary>
        ///     If this property is set, the compiler will write altered scripts and diagnostics to this
        ///     folder.
        /// </summary>
        public string DiagnosticOutputPath { get; set; }

        /// <summary>
        ///     Gets the whitelist being used for this compiler.
        /// </summary>
        public ProgramWhitelist Whitelist
        {
            get { return m_whitelist; }
        }

        /// <summary>
        ///     Contains the diagnostic codes of warnings that should not be reported by the compiler.
        /// </summary>
        public HashSet<string> IgnoredWarnings
        {
            get { return m_ignoredWarnings; }
        }

        /// <summary>
        ///     Determines whether debug information is enabled on a global level. This decision can be made on a per-script
        ///     fashion on each of the compile methods, but if this property is set to <c>true</c>, it will override any
        ///     parameter value.
        /// </summary>
        public bool EnableDebugInformation { get; set; }

        /// <summary>
        ///     Compiles a script.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="assemblyName"></param>
        /// <param name="script"></param>
        /// <param name="messages"></param>
        /// <param name="enableDebugInformation"></param>
        /// <returns></returns>
        public Task<Assembly> Compile(AppDomain domain, string assemblyName, Program program, string errorpath, List<Message> messages, bool enableDebugInformation = false)
        {
            return Compile(domain, assemblyName, new[] { program }, errorpath, messages, enableDebugInformation);
        }

        public async Task<Module> Compile(string moduleName, Assembly assembly, Program program, string errorpath, List<Message> messages, bool enableDebugInformation = false)
        {
            messages.Clear();

            var opts = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).
                WithOptimizationLevel(OptimizationLevel.Release).
                WithPlatform(Platform.AnyCpu).
                WithModuleName(moduleName);

            CSharpCompilation compilation = CreateCompilation(moduleName, program, opts);
            //await WriteDiagnostics(assemblyName, compilation.SyntaxTrees).ConfigureAwait(false);

            CompilationWithAnalyzers analyticCompilation = compilation.WithAnalyzers(ImmutableArray.Create<DiagnosticAnalyzer>(m_WhitelistDiagnosticAnalyzer));
            compilation = (CSharpCompilation)analyticCompilation.Compilation;

            using (var strm = new MemoryStream())
            {
                var result = compilation.Emit(strm);
                var success = result.Success;
                AnalyzeDiagnostics(result.Diagnostics, errorpath, messages, ref success);
                if (analyticCompilation != null)
                {
                    AnalyzeDiagnostics(await analyticCompilation.GetAllDiagnosticsAsync().ConfigureAwait(false), errorpath, messages, ref success);
                }

                // await WriteDiagnostics(assemblyName, messages, success).ConfigureAwait(false);

                if (success)
                {
                    strm.Seek(0, SeekOrigin.Begin);
                    var module = assembly.LoadModule(moduleName, strm.ToArray());

                    return module;
                }

                return null;
            }
        }

        public async Task<Assembly> Compile(AppDomain domain, string assemblyName, IEnumerable<Program> programs, string errorpath, List<Message> messages, bool enableDebugInformation = false)
        {
            messages.Clear();

            CSharpCompilation compilation = CreateCompilation(assemblyName, programs, EnableDebugInformation || enableDebugInformation ? m_debugCompilationOptions : m_runtimeCompilationOptions);
            //await WriteDiagnostics(assemblyName, compilation.SyntaxTrees).ConfigureAwait(false);

            CompilationWithAnalyzers analyticCompilation = compilation.WithAnalyzers(ImmutableArray.Create<DiagnosticAnalyzer>(m_WhitelistDiagnosticAnalyzer));
            compilation = (CSharpCompilation)analyticCompilation.Compilation;

            using (var assemblyStream = new MemoryStream())
            {
                var result = compilation.Emit(assemblyStream);
                var success = result.Success;
                AnalyzeDiagnostics(result.Diagnostics, errorpath, messages, ref success);
                if (analyticCompilation != null)
                {
                    AnalyzeDiagnostics(await analyticCompilation.GetAllDiagnosticsAsync().ConfigureAwait(false), errorpath, messages, ref success);
                }

               // await WriteDiagnostics(assemblyName, messages, success).ConfigureAwait(false);

                if (success)
                {
                    assemblyStream.Seek(0, SeekOrigin.Begin);
                    return domain.Load(assemblyStream.ToArray());
                }

                return null;
            }
        }

        private void AnalyzeDiagnostics(ImmutableArray<Diagnostic> diagnostics, string errorpath, List<Message> messages, ref bool success)
        {
            success = success && !diagnostics.Any(d => d.Severity == DiagnosticSeverity.Error);

            var orderedDiagnostics = diagnostics
                .Where(d => d.Severity >= DiagnosticSeverity.Warning)
                .OrderByDescending(d => d.Severity);

            foreach (var diagnostic in orderedDiagnostics)
            {
                // Warnings are only added for a successful build to reduce confusion.
                if (diagnostic.Severity == DiagnosticSeverity.Warning)
                {
                    if (!success || m_ignoredWarnings.Contains(diagnostic.Id))
                    {
                        continue;
                    }
                }

                var severity = diagnostic.Severity == DiagnosticSeverity.Warning ? TErrorSeverity.Warning : TErrorSeverity.Error;
                var lineSpan = diagnostic.Location.GetMappedLineSpan();
                var message = string.Format("{0}({1},{2}): {3}: {4}",
                    errorpath,
                    lineSpan.StartLinePosition.Line + 1,
                    lineSpan.StartLinePosition.Character,
                    severity,
                    diagnostic.GetMessage());

                messages.Add(new Message(severity, message));
            }
        }

        bool GetDiagnosticsOutputPath(string assemblyName, out string outputPath)
        {
            outputPath = this.DiagnosticOutputPath;
            if (outputPath == null)
            {
                return false;
            }
            if (assemblyName == null)
            {
                throw new ArgumentNullException("assemblyName");
            }
            outputPath = Path.Combine(DiagnosticOutputPath, Path.GetFileNameWithoutExtension(assemblyName));
            return true;
        }

        async Task WriteDiagnostics(string assemblyName, IEnumerable<Message> messages, bool success)
        {
            string outputPath;
            if (!GetDiagnosticsOutputPath(assemblyName, out outputPath))
            {
                return;
            }
            var fileName = Path.Combine(outputPath, "log.txt");
            var builder = new StringBuilder();
            builder.AppendLine("Success: " + success);
            builder.AppendLine();
            foreach (var line in messages)
            {
                builder.AppendLine(line.Severity + " " + line.Text);
            }
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                var writer = new StreamWriter(stream);
                await writer.WriteAsync(builder.ToString()).ConfigureAwait(false);
                await writer.FlushAsync().ConfigureAwait(false);
            }
        }

        async Task WriteDiagnostics(string assemblyName, IEnumerable<SyntaxTree> syntaxTrees, string suffix = null)
        {
            string outputPath;
            if (!GetDiagnosticsOutputPath(assemblyName, out outputPath))
            {
                return;
            }
            suffix = suffix ?? "";
            foreach (var syntaxTree in syntaxTrees)
            {
                var root = await syntaxTree.GetRootAsync().ConfigureAwait(false);
                var normalizedTree = CSharpSyntaxTree.Create((CSharpSyntaxNode)root.NormalizeWhitespace(), path: syntaxTree.FilePath);
                var fileName = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(syntaxTree.FilePath) + suffix + Path.GetExtension(syntaxTree.FilePath));
                if (!fileName.EndsWith(".cs", StringComparison.OrdinalIgnoreCase))
                {
                    fileName += ".cs";
                }
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    var writer = new StreamWriter(stream);
                    await writer.WriteAsync(normalizedTree.ToString()).ConfigureAwait(false);
                    await writer.FlushAsync().ConfigureAwait(false);
                }
            }
        }

        internal CSharpCompilation CreateCompilation(string assemblyFileName, IEnumerable<Program> programs)
        {
            return CreateCompilation(assemblyFileName, programs, m_runtimeCompilationOptions);
        }

        internal CSharpCompilation CreateCompilation(string assemblyFileName, Program program, CSharpCompilationOptions opts)
        {
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(program.Code, m_conditionalParseOptions.WithPreprocessorSymbols(ConditionalCompilationSymbols), program.Name);
            var compilation = CSharpCompilation.Create(assemblyFileName, references: m_metadataReferences, options: opts).
                AddSyntaxTrees(syntaxTree);

            return compilation;
        }

        internal CSharpCompilation CreateCompilation(string assemblyFileName, IEnumerable<Program> programs, CSharpCompilationOptions opts)
        {
            IEnumerable<SyntaxTree> syntaxTrees = null;
            if (programs != null)
            {
                syntaxTrees = programs.Select(s => CSharpSyntaxTree.ParseText(s.Code, m_conditionalParseOptions.WithPreprocessorSymbols(ConditionalCompilationSymbols), s.Name));
            }
            if (assemblyFileName != null)
            {
                assemblyFileName = Path.GetFileName(assemblyFileName);
            }
            else
            {
                assemblyFileName = "scripts.dll";
            }
            var compilation = CSharpCompilation.Create(assemblyFileName, syntaxTrees, m_metadataReferences, opts);
            return compilation;
        }

        public void AddImplicitIngameNamespacesFromTypes(params Type[] types)
        {
            for (var i = 0; i < types.Length; i++)
            {
                var type = types[i];
                if (type == null)
                {
                    throw new ArgumentNullException("types");
                }
                m_implicitScriptNamespaces.Add(types[i].Namespace);
            }
        }

        public void AddReferencedAssemblies(params string[] assemblyLocations)
        {
            for (var i = 0; i < assemblyLocations.Length; i++)
            {
                var location = assemblyLocations[i];
                if (location == null)
                {
                    throw new ArgumentNullException("assemblyLocations");
                }
                if (m_assemblyLocations.Add(location))
                {
                    m_metadataReferences.Add(MetadataReference.CreateFromFile(location));
                }
            }
        }

        public void AddUnblockableIngameExceptions(params Type[] types)
        {
            for (var i = 0; i < types.Length; i++)
            {
                var type = types[i];
                if (type == null)
                {
                    throw new ArgumentNullException("types");
                }
                if (!typeof(Exception).IsAssignableFrom(type))
                {
                    throw new ArgumentException(type.FullName + " is not an exception", "types");
                }
                if (type.IsGenericType || type.IsGenericTypeDefinition)
                {
                    throw new ArgumentException("Generic exceptions are not supported", "types");
                }
                m_unblockableIngameExceptions.Add(type);
            }
        }

        public void AddConditionalCompilationSymbols(params string[] symbols)
        {
            for (var i = 0; i < symbols.Length; i++)
            {
                var symbol = symbols[i];
                if (symbol == null)
                {
                    throw new ArgumentNullException("symbols");
                }
                else if (symbol == string.Empty)
                {
                    continue;
                }
                m_conditionalCompilationSymbols.Add(symbols[i]);
            }
        }

        public Program GetIngameScript(string code, string className, string inheritance, string modifiers = "sealed")
        {
            if (string.IsNullOrEmpty(className))
            {
                throw new ArgumentException("Argument is null or empty", "className");
            }
            var usings = string.Join(";\nusing ", m_implicitScriptNamespaces);
            modifiers = modifiers ?? "";
            inheritance = string.IsNullOrEmpty(inheritance) ? "" : ": " + inheritance;
            code = code ?? "";
            return new Program(className, string.Format("using {0};\n" +
                                                       "public {1} class {2} {3}{{\n" +
                                                       "#line 1 \"{2}\"\n" +
                                                       "{4}\n" +
                                                       "}}\n",
                usings,
                modifiers,
                className,
                inheritance,
                code));
            //return new Script(className, $"using {usings};\n" +
            //                             $"public {modifiers} class {className} {inheritance}{{\n" +
            //                             $"#line 1 \"{className}\"\n" +
            //                             $"{code}\n" +
            //                             $"}}\n");
        }
    }
}
