﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public class ProgramBlacklistBatch : Batch, IBlacklistBatch
    {
        public ProgramBlacklistBatch(ProgramWhitelist whitelist) : base(whitelist)
            { }

        public void AddNamespaceOfTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);
                var namespaceSymbol = typeSymbol.ContainingNamespace;
                if (namespaceSymbol == null || namespaceSymbol.IsGlobalNamespace)
                {
                    continue;
                }

                Whitelist.m_ingameBlacklist.Add(namespaceSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers));
            }
        }

        public void RemoveNamespaceOfTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);
                var namespaceSymbol = typeSymbol.ContainingNamespace;
                if (namespaceSymbol == null || namespaceSymbol.IsGlobalNamespace)
                {
                    continue;
                }

                Whitelist.m_ingameBlacklist.Remove(namespaceSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers));
            }
        }

        public void AddTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);

                Whitelist.m_ingameBlacklist.Add(typeSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers));
            }
        }

        public void RemoveTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);

                Whitelist.m_ingameBlacklist.Remove(typeSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers));
            }
        }

        public void AddMembers(Type type, params string[] memberNames)
        {
            if (type == null)
            {
                throw new MyWhitelistException("Must specify the target type");
            }
            if (memberNames == null || memberNames.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one member name");
            }
            AssertVitality();
            var members = new List<string>();
            GetMemberWhitelistKeys(type, memberNames, members);
            for (var index = 0; index < members.Count; index++)
            {
                var member = members[index];
                Whitelist.m_ingameBlacklist.Add(member);
            }
        }

        public void RemoveMembers(Type type, params string[] memberNames)
        {
            if (type == null)
            {
                throw new MyWhitelistException("Must specify the target type");
            }
            if (memberNames == null || memberNames.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one member name");
            }
            AssertVitality();
            var members = new List<string>();
            GetMemberWhitelistKeys(type, memberNames, members);
            for (var index = 0; index < members.Count; index++)
            {
                var member = members[index];
                Whitelist.m_ingameBlacklist.Remove(member);
            }
        }

        void GetMemberWhitelistKeys(Type type, string[] memberNames, List<string> members)
        {
            var typeSymbol = ResolveTypeSymbol(type);
            for (var index = 0; index < memberNames.Length; index++)
            {
                var memberName = memberNames[index];
                var count = members.Count;
                foreach (var memberSymbol in typeSymbol.GetMembers())
                {
                    if (memberSymbol.Name != memberName)
                    {
                        continue;
                    }
                    switch (memberSymbol.DeclaredAccessibility)
                    {
                        case Accessibility.ProtectedOrInternal:
                        case Accessibility.Protected:
                        case Accessibility.Public:
                            break;
                        default:
                            continue;
                    }
                    members.Add(memberSymbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly));
                }
                if (count == members.Count)
                {
                    throw new MyWhitelistException("Cannot find any members named " + memberName);
                }
            }
        }
    }
}
