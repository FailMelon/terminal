﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    /// <summary>
    ///     Represents a named script.
    /// </summary>
    public struct Program
    {
        public Program(string name, string code)
        {
            Name = name;
            Code = code;
        }

        /// <summary>
        ///     The name of the script.
        /// </summary>
        public readonly string Name;

        /// <summary>
        ///     The code content of the script.
        /// </summary>
        public readonly string Code;
    }
}
