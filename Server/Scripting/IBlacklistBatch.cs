﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public interface IBlacklistBatch : IDisposable
    {
        void AddNamespaceOfTypes(params Type[] types);
        void RemoveNamespaceOfTypes(params Type[] types);

        void AddTypes(params Type[] types);
        void RemoveTypes(params Type[] types);

        void AddMembers(Type type, params string[] memberNames);
        void RemoveMembers(Type type, params string[] memberNames);
    }
}
