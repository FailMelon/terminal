﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    class InstructionThreading : CSharpSyntaxRewriter
    {

        private readonly ProgramCompiler programCompiler;
        private readonly CSharpCompilation cSharpCompilation;
        private SyntaxTree tree;

        public InstructionThreading(ProgramCompiler compiler, CSharpCompilation compilation, SyntaxTree syntaxTree)
        {
            programCompiler = compiler;
            cSharpCompilation = compilation;
            tree = syntaxTree;
        }
    }
}
