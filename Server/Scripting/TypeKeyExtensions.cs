﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    internal static class TypeKeyExtensions
    {
        public static string GetWhitelistKey(this ISymbol symbol, TypeKeyQuantity quantity)
        {
            var namespaceSymbol = symbol as INamespaceSymbol;
            if (namespaceSymbol != null)
            {
                return namespaceSymbol.GetWhitelistKey(quantity);
            }
            var typeSymbol = symbol as ITypeSymbol;
            if (typeSymbol != null)
            {
                return typeSymbol.GetWhitelistKey(quantity);
            }
            var methodSymbol = symbol as IMethodSymbol;
            if (methodSymbol != null)
            {
                // Account for generic methods, we must check their definitions, not specific implementations
                if (methodSymbol.IsGenericMethod && !methodSymbol.IsDefinition)
                {
                    methodSymbol = methodSymbol.OriginalDefinition;
                    return methodSymbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                            + ", "
                           + symbol.ContainingAssembly.Name;
                }
            }

            var memberSymbol = (symbol is IEventSymbol || symbol is IFieldSymbol || symbol is IPropertySymbol || symbol is IMethodSymbol) ? symbol : null;
            if (memberSymbol == null)
            {
                throw new ArgumentException("Invalid symbol type: Expected namespace, type or type member", "symbol");
            }

            return memberSymbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                    + ", "
                   + symbol.ContainingAssembly.Name;
        }

        public static string GetWhitelistKey(this ITypeSymbol symbol, TypeKeyQuantity quantity)
        {
            symbol = ResolveRootType(symbol);

            switch (quantity)
            {
                case TypeKeyQuantity.ThisOnly:
                    return symbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                           + ", "
                           + symbol.ContainingAssembly.Name;

                case TypeKeyQuantity.AllMembers:
                    return symbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                           + "+*, "
                           + symbol.ContainingAssembly.Name;

                default:
                    throw new ArgumentOutOfRangeException("quantity", quantity, null);
            }
        }

        static ITypeSymbol ResolveRootType(ITypeSymbol symbol)
        {
            if (symbol is INamedTypeSymbol namedTypeSymbol && namedTypeSymbol.IsGenericType && !namedTypeSymbol.IsDefinition)
            {
                symbol = namedTypeSymbol.OriginalDefinition;
                //if (symbol.SpecialType == SpecialType.System_Nullable_T)
                //    return namedTypeSymbol.TypeArguments[0];
                return symbol;
            }

            if (symbol is IPointerTypeSymbol pointerTypeSymbol)
                return pointerTypeSymbol.PointedAtType;

            return symbol;
        }

        public static string GetWhitelistKey(this INamespaceSymbol symbol, TypeKeyQuantity quantity)
        {
            switch (quantity)
            {
                case TypeKeyQuantity.ThisOnly:
                    return symbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                           + ", "
                           + symbol.ContainingAssembly.Name;

                case TypeKeyQuantity.AllMembers:
                    return symbol.ToDisplayString(SymbolDisplayFormat.CSharpErrorMessageFormat)
                           + ".*, "
                           + symbol.ContainingAssembly.Name;

                default:
                    throw new ArgumentOutOfRangeException("quantity", quantity, null);
            }
        }
    }
}
