﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public abstract class Batch : IDisposable
    {
        readonly Dictionary<string, IAssemblySymbol> m_assemblyMap;
        bool m_isDisposed;

        protected Batch(ProgramWhitelist whitelist)
        {
            Whitelist = whitelist;
            var compilation = Whitelist.CreateCompilation();
            m_assemblyMap = compilation.References
                .Select(compilation.GetAssemblyOrModuleSymbol)
                .OfType<IAssemblySymbol>()
                .ToDictionary(symbol => symbol.ToDisplayString(SymbolDisplayFormat.FullyQualifiedFormat));
        }

        ~Batch()
        {
            m_isDisposed = true;
            Debug.Assert(false, "Undisposed whitelist batch!");
        }

        protected ProgramWhitelist Whitelist { get; private set; }

        [DebuggerNonUserCode]
        protected void AssertVitality()
        {
            if (m_isDisposed)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }

        protected INamedTypeSymbol ResolveTypeSymbol(Type type)
        {
            if (!m_assemblyMap.TryGetValue(type.Assembly.FullName, out IAssemblySymbol assemblySymbol))
            {
                throw new MyWhitelistException(string.Format("Cannot add {0} to the batch because {1} has not been added to the compiler.",
                    type.FullName, type.Assembly.FullName));
            }

            var typeSymbol = assemblySymbol.GetTypeByMetadataName(type.FullName);
            if (typeSymbol == null)
            {
                throw new MyWhitelistException(string.Format("Cannot add {0} to the batch because its symbol variant could not be found.",
                    type.FullName));
            }

            return typeSymbol;
        }

        public void Dispose()
        {
            if (m_isDisposed)
                return;

            m_isDisposed = true;
            OnDispose();
            GC.SuppressFinalize(this);
        }

        protected virtual void OnDispose()
        {
        }
    }
}
