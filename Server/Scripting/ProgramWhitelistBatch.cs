﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    class ProgramWhitelistBatch : Batch, IWhitelistBatch
    {
        public ProgramWhitelistBatch(ProgramWhitelist whitelist) : base(whitelist)
            { }

        /// <summary>
        ///     Adds the entire namespace of one or more given types.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="types"></param>
        public void AllowNamespaceOfTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);
                var namespaceSymbol = typeSymbol.ContainingNamespace;
                if (namespaceSymbol == null || namespaceSymbol.IsGlobalNamespace)
                {
                    continue;
                }

                Whitelist.Register(namespaceSymbol, type);
            }
        }

        /// <summary>
        ///     Adds one or more specific types and all their members to the whitelist.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="types"></param>
        public void AllowTypes(params Type[] types)
        {
            if (types == null || types.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one type");
            }
            AssertVitality();
            for (var index = 0; index < types.Length; index++)
            {
                var type = types[index];
                if (type == null)
                {
                    throw new MyWhitelistException("The type in index " + index + " is null");
                }
                var typeSymbol = ResolveTypeSymbol(type);

                Whitelist.Register(typeSymbol, type);
            }
        }

        /// <summary>
        ///     Adds only the specified members to the whitelist.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="members"></param>
        public void AllowMembers(params MemberInfo[] members)
        {
            if (members == null || members.Length == 0)
            {
                throw new MyWhitelistException("Needs at least one member");
            }
            AssertVitality();
            for (var index = 0; index < members.Length; index++)
            {
                var member = members[index];
                if (member == null)
                {
                    throw new MyWhitelistException("Element " + index + " is null");
                }

                var typeSymbol = ResolveTypeSymbol(member.DeclaringType);
                var candidates = typeSymbol.GetMembers().Where(m => m.MetadataName == member.Name).ToList();
                var method = member as MethodInfo;
                ParameterInfo[] methodParameters = null;
                if (method != null)
                {
                    // Sanity check. I don't think this is actually possible.
                    Debug.Assert(candidates.All(m => m is IMethodSymbol), "Illogical failure: Found more than one non-method with the same name?!?");
                    methodParameters = method.GetParameters();
                    candidates.RemoveAll(s => ((IMethodSymbol)s).Parameters.Length != methodParameters.Length);
                    if (method.IsGenericMethodDefinition)
                    {
                        candidates.RemoveAll(s => !((IMethodSymbol)s).IsGenericMethod);
                    }
                    else
                    {
                        candidates.RemoveAll(s => ((IMethodSymbol)s).IsGenericMethod);
                    }

                    if (method.IsSpecialName && method.Name.StartsWith("get_") || method.Name.StartsWith("set_"))
                    {
                        throw new MyWhitelistException("Whitelist the actual properties, not their access methods");
                    }
                }

                switch (candidates.Count)
                {
                    case 0:
                        throw new MyWhitelistException(string.Format("Cannot add {0} to the whitelist because its symbol variant could not be found.", member));

                    case 1:
                        Whitelist.RegisterMember(candidates[0], member);
                        break;

                    default:
                        // Sanity check. I don't think this is actually possible.
                        Debug.Assert(method != null, "Illogical failure: Found more than one non-method with the same name?!?");

                        var methodSymbol = FindMethodOverload(candidates, methodParameters);
                        if (methodSymbol == null)
                        {
                            throw new MyWhitelistException(string.Format("Cannot add {0} to the whitelist because its symbol variant could not be found.", member));
                        }

                        Whitelist.RegisterMember(methodSymbol, member);
                        break;
                }
            }
        }

        IMethodSymbol FindMethodOverload(IEnumerable<ISymbol> candidates, ParameterInfo[] methodParameters)
        {
            foreach (var candidate in candidates)
            {
                var methodSymbol = (IMethodSymbol)candidate;
                var candidateParameters = methodSymbol.Parameters;
                var success = true;
                for (var i = 0; i < candidateParameters.Length; i++)
                {
                    var parameter = methodParameters[i];
                    var parameterType = parameter.ParameterType;
                    var candidateParameter = candidateParameters[i];
                    var candidateParameterType = candidateParameter.Type;

                    if (parameter.IsOut)
                    {
                        if (candidateParameter.RefKind != RefKind.Out)
                        {
                            success = false;
                            break;
                        }
                    }

                    if (parameterType.IsByRef)
                    {
                        if (candidateParameter.RefKind != RefKind.Ref)
                        {
                            success = false;
                            break;
                        }
                        parameterType = parameterType.GetElementType();
                    }

                    if (parameterType.IsPointer)
                    {
                        if (!(candidateParameterType is IPointerTypeSymbol))
                        {
                            success = false;
                            break;
                        }
                        candidateParameterType = ((IPointerTypeSymbol)candidateParameterType).PointedAtType;
                        parameterType = parameterType.GetElementType();
                    }

                    if (parameterType.IsArray)
                    {
                        if (!(candidateParameterType is IArrayTypeSymbol))
                        {
                            success = false;
                            break;
                        }
                        candidateParameterType = ((IArrayTypeSymbol)candidateParameterType).ElementType;
                        parameterType = parameterType.GetElementType();
                    }

                    if (!Equals(ResolveTypeSymbol(parameterType), candidateParameterType))
                    {
                        success = false;
                        break;
                    }
                }
                if (success)
                {
                    return methodSymbol;
                }
            }
            return null;
        }

    }
}
