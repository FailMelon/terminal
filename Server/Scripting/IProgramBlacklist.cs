﻿using GameServer.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public interface IProgramBlacklist
    {
        HashSetReader<string> GetWhitelist();
        HashSetReader<string> GetBlacklistedIngameEntries();
        IBlacklistBatch OpenIngameBlacklistBatch();
    }
}
