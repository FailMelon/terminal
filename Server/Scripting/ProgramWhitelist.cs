﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using GameServer.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using GameAPI;
using GameAPI.Services.SSH;

namespace GameServer.Scripting
{
    /// <summary>
    /// Exceptions during registration of whitelisted type members
    /// </summary>
    [Serializable]
    public class MyWhitelistException : Exception
    {
        public MyWhitelistException()
        { }

        public MyWhitelistException(string message) : base(message)
        { }

        public MyWhitelistException(string message, Exception inner) : base(message, inner)
        { }

        protected MyWhitelistException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        { }
    }

    public class ProgramWhitelist : IProgramBlacklist
    {
        public readonly HashSet<string> m_ingameBlacklist = new HashSet<string>();
        readonly ProgramCompiler m_programCompiler;
        readonly HashSet<string> m_whitelist = new HashSet<string>();

        public ProgramWhitelist(ProgramCompiler programCompiler)
        {
            m_programCompiler = programCompiler;

            using (var handle = OpenBatch())
            {
                handle.AllowNamespaceOfTypes(
                    typeof(IComputer),
                    typeof(SSHScriptBase)
                    // Our API interface with the game

                );

                handle.AllowTypes(
                    typeof(object),
                    typeof(string),
                    typeof(int),
                    typeof(short),
                    typeof(long),
                    typeof(uint),
                    typeof(ushort),
                    typeof(ulong),
                    typeof(double),
                    typeof(float),
                    typeof(bool),
                    typeof(char),
                    typeof(byte),
                    typeof(sbyte),
                    typeof(decimal),
                    typeof(Random),
                    typeof(Func<>),
                    typeof(System.Collections.ObjectModel.ObservableCollection<>),
                    typeof(System.Collections.ObjectModel.Collection<>),
                    typeof(System.Collections.Specialized.NotifyCollectionChangedEventArgs),
                    typeof(System.Collections.Specialized.NotifyCollectionChangedAction)
                );

                handle.AllowMembers(typeof(System.Reflection.MemberInfo).GetProperty("Name"));

                handle.AllowMembers(
                    typeof(Type).GetProperty("FullName"),
                    typeof(Type).GetMethod("GetTypeFromHandle"),
                    typeof(Type).GetMethod("GetFields", new[] { typeof(System.Reflection.BindingFlags) }),
                    typeof(Type).GetMethod("IsEquivalentTo"),
                    typeof(Type).GetMethod("op_Equality"),
                    typeof(Type).GetMethod("ToString")
                );

                handle.AllowMembers(
                    typeof(ValueType).GetMethod("Equals"),
                    typeof(ValueType).GetMethod("GetHashCode"),
                    typeof(ValueType).GetMethod("ToString")
                );

                handle.AllowMembers(
                    typeof(Environment).GetProperty("NewLine", BindingFlags.Static | BindingFlags.Public)
                );

            }
        }

        public IWhitelistBatch OpenBatch()
        {
            return new ProgramWhitelistBatch(this);
        }

        internal bool IsWhitelisted(ISymbol symbol)
        {
            var typeSymbol = symbol as INamedTypeSymbol;
            if (typeSymbol != null)
            {
                return IsWhitelisted(typeSymbol) != TypeKeyQuantity.None;
            }

            if (symbol.IsMemberSymbol())
            {
                return IsMemberWhitelisted(symbol);
            }

            // This is not a symbol we need concern ourselves with.
            return true;
        }

        bool IsBlacklisted(ISymbol symbol)
        {
            if (symbol.IsMemberSymbol())
            {
                if (m_ingameBlacklist.Contains(symbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly)))
                    return true;
                symbol = symbol.ContainingType;
            }

            var typeSymbol = symbol as ITypeSymbol;
            while (typeSymbol != null)
            {
                if (m_ingameBlacklist.Contains(typeSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers)))
                    return true;
                typeSymbol = typeSymbol.ContainingType;
            }
            return false;
        }

        TypeKeyQuantity IsWhitelisted(INamespaceSymbol namespaceSymbol)
        {
            if (m_whitelist.Contains(namespaceSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers)))
            {
                return TypeKeyQuantity.AllMembers;
            }
            return TypeKeyQuantity.None;
        }

        TypeKeyQuantity IsWhitelisted(INamedTypeSymbol typeSymbol)
        {
            // Delegates are allowed directly in checking, as they are harmless since you have to call 
            // or store something in it which is also checked.
            if (IsDelegate(typeSymbol))
            {
                return TypeKeyQuantity.AllMembers;
            }

            if (IsBlacklisted(typeSymbol))
            {
                return TypeKeyQuantity.None;
            }

            var result = IsWhitelisted(typeSymbol.ContainingNamespace);
            if (result == TypeKeyQuantity.AllMembers)
            {
                return result;
            }

            if (m_whitelist.Contains(typeSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers)))
            {
                return TypeKeyQuantity.AllMembers;
            }

            if (m_whitelist.Contains(typeSymbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly)))
            {
                return TypeKeyQuantity.ThisOnly;
            }

            return TypeKeyQuantity.None;
        }

        bool IsDelegate(INamedTypeSymbol typeSymbol)
        {
            while (typeSymbol != null)
            {
                if (typeSymbol.SpecialType == SpecialType.System_Delegate || typeSymbol.SpecialType == SpecialType.System_MulticastDelegate)
                {
                    return true;
                }
                typeSymbol = typeSymbol.BaseType;
            }
            return false;
        }

        bool IsMemberWhitelisted(ISymbol memberSymbol)
        {
            while (true)
            {
                if (IsBlacklisted(memberSymbol))
                {
                    return false;
                }

                var result = IsWhitelisted(memberSymbol.ContainingType);
                if (result == TypeKeyQuantity.AllMembers)
                {
                    return true;
                }

                if (m_whitelist.Contains(memberSymbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly)))
                {
                    return true;
                }

                if (memberSymbol.IsOverride)
                {
                    memberSymbol = memberSymbol.GetOverriddenSymbol();
                    if (memberSymbol != null)
                    {
                        continue;
                    }
                }

                return false;
            }
        }

        public CSharpCompilation CreateCompilation()
        {
            return m_programCompiler.CreateCompilation(null, null);
        }

        public void RegisterMember(ISymbol symbol, MemberInfo member)
        {
            if (!(symbol is IEventSymbol || symbol is IFieldSymbol || symbol is IPropertySymbol || symbol is IMethodSymbol))
            {
                throw new MyWhitelistException("Unsupported symbol type " + symbol);
            }

            var namespaceSymbol = symbol.ContainingNamespace;
            if (namespaceSymbol != null && !namespaceSymbol.IsGlobalNamespace)
            {
                var namespaceKey = namespaceSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers);
                if (m_whitelist.Contains(namespaceKey))
                {
                    throw new MyWhitelistException("The member " + member + " is covered by the " + namespaceKey + " rule");
                }
            }

            var typeSymbol = symbol.ContainingType;
            while (typeSymbol != null)
            {
                var typeKey = typeSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers);
                if (m_whitelist.Contains(typeKey))
                {
                    throw new MyWhitelistException("The member " + member + " is covered by the " + typeKey + " rule");
                }

                // If there is no previous registration of the containing type, or the current registration
                // is less restrictive than the one we're currently asking for, it needs to be updated.
                // (this will only allow the reference of a type, but none of its members).
                typeKey = typeSymbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly);
                if (!m_whitelist.Contains(typeKey))
                {
                    m_whitelist.Add(typeKey);
                }

                typeSymbol = typeSymbol.ContainingType;
            }

            var whitelistKey = symbol.GetWhitelistKey(TypeKeyQuantity.ThisOnly);
            if (m_whitelist.Contains(whitelistKey))
            {
                throw new MyWhitelistException("Duplicate registration of the whitelist key " + whitelistKey + " retrieved from " + member);
            }
            m_whitelist.Add(whitelistKey);
        }

        public void Register(INamespaceSymbol symbol, Type type)
        {
            var whitelistKey = symbol.GetWhitelistKey(TypeKeyQuantity.AllMembers);
            if (m_whitelist.Contains(whitelistKey))
            {
                throw new MyWhitelistException("Duplicate registration of the whitelist key " + whitelistKey + " retrieved from " + type);
            }
            m_whitelist.Add(whitelistKey);
        }

        public void Register(ITypeSymbol symbol, Type type)
        {
            var namespaceSymbol = symbol.ContainingNamespace;
            if (namespaceSymbol != null && !namespaceSymbol.IsGlobalNamespace)
            {
                var namespaceKey = namespaceSymbol.GetWhitelistKey(TypeKeyQuantity.AllMembers);
                if (m_whitelist.Contains(namespaceKey))
                {
                    throw new MyWhitelistException("The type " + type + " is covered by the " + namespaceKey + " rule");
                }
            }

            var whitelistKey = symbol.GetWhitelistKey(TypeKeyQuantity.AllMembers);
            if (m_whitelist.Contains(whitelistKey))
            {
                throw new MyWhitelistException("Duplicate registration of the whitelist key " + whitelistKey + " retrieved from " + type);
            }
            m_whitelist.Add(whitelistKey);
        }

        /// <summary>
        ///     Clears the whitelist.
        /// </summary>
        public void Clear()
        {
            // This one feels a bit weird, can't really see the need but it was in the old one so I guess I'll keep it here as well.
            m_whitelist.Clear();
            m_ingameBlacklist.Clear();
        }

        public HashSetReader<string> GetWhitelist()
        {
            return new HashSetReader<string>(m_whitelist);
        }

        public HashSetReader<string> GetBlacklistedIngameEntries()
        {
            return m_ingameBlacklist;
        }

        public IBlacklistBatch OpenIngameBlacklistBatch()
        {
            return new ProgramBlacklistBatch(this);
        }
    }
}
