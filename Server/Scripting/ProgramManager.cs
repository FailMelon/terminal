﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    public class ProgramManager
    {
        public static ProgramManager Instance = new ProgramManager();

        private ProgramCompiler compiler;

        public ProgramManager()
        {
            compiler = new ProgramCompiler();
        }

        public void Startup()
        {

        }

        public async Task<Assembly> Compile(AppDomain domain, string name, string code, string errorpath, List<ProgramCompiler.Message> messages)
        {
            return await compiler.Compile(domain, name, new Program(name, code), errorpath, messages);
        }

        public async Task<Module> CompileModule(string modulename, Assembly assembly, string code, string errorpath, List<ProgramCompiler.Message> messages)
        {
            return await compiler.Compile(modulename, assembly, new Program(modulename, code), errorpath, messages);
        }

        public async Task<Assembly> CompileEntryPoint(AppDomain domain, string name, string code, string errorpath, List<ProgramCompiler.Message> messages, bool debug = false)
        {
            StringBuilder entrypoint = new StringBuilder();
            entrypoint.AppendLine("using System;");
            entrypoint.AppendLine("using GameAPI;");
            entrypoint.AppendLine("using GameAPI.Services.SSH;");

            entrypoint.AppendLine("public class MyUserScript : SSHScriptBase");
            entrypoint.AppendLine("{");
                entrypoint.AppendLine(code);
            entrypoint.AppendLine("}");

            return await compiler.Compile(domain, name, new Program(name, entrypoint.ToString()), errorpath, messages, debug);
        }
    }
}
