﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Scripting
{
    internal static class AnalysisExtensions
    {
        public static ISymbol GetOverriddenSymbol(this ISymbol symbol)
        {
            if (!symbol.IsOverride)
                return null;

            if (symbol is ITypeSymbol typeSymbol)
                return typeSymbol.BaseType;

            if (symbol is IEventSymbol eventSymbol)
                return eventSymbol.OverriddenEvent;

            if (symbol is IPropertySymbol propertySymbol)
                return propertySymbol.OverriddenProperty;

            if (symbol is IMethodSymbol methodSymbol)
                return methodSymbol.OverriddenMethod;

            return null;
        }

        public static bool IsMemberSymbol(this ISymbol symbol)
        {
            return (symbol is IEventSymbol || symbol is IFieldSymbol || symbol is IPropertySymbol || symbol is IMethodSymbol);
        }

        public static BaseMethodDeclarationSyntax WithBody(this BaseMethodDeclarationSyntax item, BlockSyntax body)
        {
            if (item is ConstructorDeclarationSyntax constructor)
                return constructor.WithBody(body);

            if (item is ConversionOperatorDeclarationSyntax conversion)
                return conversion.WithBody(body);

            if (item is DestructorDeclarationSyntax destructor)
                return destructor.WithBody(body);

            if (item is MethodDeclarationSyntax method)
                return method.WithBody(body);

            if (item is OperatorDeclarationSyntax @operator)
                return @operator.WithBody(body);

            throw new ArgumentException("Unknown " + typeof(BaseMethodDeclarationSyntax).FullName, "item");
        }

        public static AnonymousFunctionExpressionSyntax WithBody(this AnonymousFunctionExpressionSyntax item, CSharpSyntaxNode body)
        {
            if (item is AnonymousMethodExpressionSyntax anonMethod)
                return anonMethod.WithBody(body);

            if (item is ParenthesizedLambdaExpressionSyntax parenLambda)
                return parenLambda.WithBody(body);

            if (item is SimpleLambdaExpressionSyntax simpleLambda)
                return simpleLambda.WithBody(body);

            throw new ArgumentException("Unknown " + typeof(AnonymousFunctionExpressionSyntax).FullName, "item");
        }

        public static bool IsInSource(this ISymbol symbol)
        {
            for (var i = 0; i < symbol.Locations.Length; i++)
            {
                if (!symbol.Locations[i].IsInSource)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
