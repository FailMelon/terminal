﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public static class ByteMath
    {
        public const int Kilobyte = 1024;
        public const int Megabyte = 1048576;
        public const int Gigabyte = 1073741824;
    }
}
