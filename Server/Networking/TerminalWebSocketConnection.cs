﻿using GameAPI.Packets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Tarul;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;

namespace GameServer.Networking
{
    public interface INetConnection
    {
        void SendPacket(IBitSerialisable packet);

        void SubscribeToPacket<T>(PacketReceived<T> callback) where T : struct;

        event Action<INetConnection, string> Disconnected;

        AuthData Auth { get; }
    }

    public struct AuthData
    {
        public string Username;
    }

    public delegate void PacketReceived<T>(INetConnection sender, T packet) where T : struct;

    public class TerminalWebSocketConnection : WebSocketBehavior, INetConnection
    {
        public class JsonPacket
        {
            public string name;
            public JObject data;
        }

        public event Action<INetConnection, string> Disconnected;

        public Game.User User { get; private set; }

        public AuthData Auth { get; private set; }

        private IDictionary<Type, HashSet<Delegate>> callbacks;
        private object syncroot;

        private bool authenticated;

        public TerminalWebSocketConnection()
        {
            CookiesValidator = ValidateCookies;
        }

        private bool ValidateCookies(CookieCollection request, CookieCollection response)
        {
            if (request.Count >= 2)
            {
                var usernameCookie = request["username"];
                var sessionidCookie = request["sessionid"];

                if (usernameCookie != null && sessionidCookie != null)
                {
                    if (Database.UserLogin(usernameCookie.Value.ToLower(), sessionidCookie.Value))
                    {
                        Auth = new AuthData { Username = usernameCookie.Value.ToLower() };
                        authenticated = true;
                        return true;
                    }
                }

            }

            return false;
        }

        protected override void OnOpen()
        {
            if (authenticated)
            {
                callbacks = new Dictionary<Type, HashSet<Delegate>>();
                syncroot = new object();

                User = new Game.User(this);
            }
            else
                Drop("Invalid session (not authenticated)", CloseStatusCode.InvalidData);
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (!authenticated)
            {
                Drop("Invalid session (not authenticated)", CloseStatusCode.InvalidData);
                return;
            }

            if (e.IsText && User != null)
            {
                var packetRaw = JsonConvert.DeserializeObject<JsonPacket>(e.Data);

                var packetType = PacketSystem.GetPacketType(packetRaw.name);

                if (packetType != null)
                {
                    var packet = packetRaw.data.ToObject(packetType) as IBitSerialisable;

                    if (callbacks.TryGetValue(packetType, out var handlers))
                    {
                        foreach (Delegate handler in handlers)
                            handler.DynamicInvoke(this, packet);
                    }
                    else
                    {
                        Log.Warn($"No event handlers for event '{packetType.Name}'");
                    }
                }
            }
        }

        protected override void OnClose(CloseEventArgs e)
        {
            Disconnected?.Invoke(this, e.Reason);

            if (callbacks?.Count > 0)
                ClearSubscriptions();
        }

        public void SubscribeToPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                if (!callbacks.TryGetValue(t, out var set))
                {
                    set = new HashSet<Delegate>();
                    callbacks.Add(t, set);
                }
                set.Add(callback);
            }
        }

        public void UnsubscribeFromPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                if (!callbacks.TryGetValue(t, out var set)) return;
                set.Remove(callback);
            }
        }

        public void ClearSubscriptions()
        {
            lock (syncroot)
                callbacks.Clear();
        }

        public void SendPacket(IBitSerialisable packet)
        {
            if (State == WebSocketState.Open)
            {
                var name = PacketSystem.GetPacketName(packet.GetType());

                var packetRaw = new JsonPacket
                {
                    name = name,
                    data = JObject.FromObject(packet)
                };

                Send(JsonConvert.SerializeObject(packetRaw));
            }
        }

        public void Drop(string reason, CloseStatusCode code = CloseStatusCode.Normal)
        {
            Sessions.CloseSession(ID, code, reason);
        }
    }
}
