﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameServer.Threading
{
    public class ManualResetEventResponse<T>
    {
        public bool IsWaiting { get; private set; }

        private ManualResetEventSlim mre;
        private T data;

        public ManualResetEventResponse()
        {
            mre = new ManualResetEventSlim(false);
            data = default(T);
        }

        public void Set(T value)
        {
            data = value;
            mre.Set();
            IsWaiting = false;
        }

        public T Wait()
        {
            IsWaiting = true;
            mre.Wait();
            mre.Reset();
            return data;
        }
    }
}
