﻿using GameAPI;
using GameAPI.Missions;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game
{
    public class MissionManager : MarshalByRefObject, IMissionManager
    {
        private IUser user;

        private List<IMission> currentMissions;

        public MissionManager(IUser user)
        {
            this.user = user;
            currentMissions = new List<IMission>();
        }

        public void RegisterUserInteraction(MissionTask task)
        {
            var missiontoRemove = new List<IMission>();
            foreach(var mission in currentMissions)
            {
                if (!mission.IsComplete && mission.OnUserCompletedTask(task))
                {
                    mission.Complete();
                    mission.Shutdown();
                    Database.RemoveMission(mission);
                    missiontoRemove.Add(mission);
                }
            }

            missiontoRemove.ForEach((toRemoveMission) => currentMissions.Remove(toRemoveMission));
        }

        public void StartMission<T>() where T : IMission, new()
        {
            var mission = new T
            {
                User = user
            };
            mission.ID = ObjectId.GenerateNewId();
            mission.OnStart();
            currentMissions.Add(mission);
        }

        public void LoadMissions()
        {
            var mission = Database.LoadMission(user);

            if (mission != null)
                currentMissions.Add(mission);
        }

        public void ShutdownMissions()
        {
            foreach(var mission in currentMissions)
            {
                Database.SaveMission(user, mission);
                mission.Shutdown();
            }

            currentMissions.Clear();
        }
    }
}
