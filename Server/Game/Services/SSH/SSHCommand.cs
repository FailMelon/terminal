﻿using GameAPI;
using GameAPI.IO;
using GameAPI.Services.SSH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GameServer.Game.Services.SSH
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SSHCommandAttribute : Attribute
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public SSHCommandAttribute(string name, string description = "none")
        {
            Name = name;
            Description = description;
        }

    }
    
    public abstract class SSHCommandBase : ISSHExecutable
    {
        private static IDictionary<string, Type> commands;
        private static IList<SSHCommandAttribute> commandAttributes;

        public static void Initialise()
        {
            commands = new Dictionary<string, Type>();
            commandAttributes = new List<SSHCommandAttribute>();

            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (typeof(ISSHExecutable).IsAssignableFrom(type))
                {
                    var attributes = type.GetCustomAttributes(typeof(SSHCommandAttribute), true);

                    if (attributes.Length > 0)
                    {
                        var attribute = attributes.First() as SSHCommandAttribute;
                        commandAttributes.Add(attribute);

                        commands.Add(attribute.Name, type);
                    }
                }
            }
        }

        public IList<SSHCommandAttribute> GetCommandsDescription()
        {
            return commandAttributes;
        }

        public static ISSHExecutable CreateCommand(string typeName)
        {
            if (commands.TryGetValue(typeName, out Type type))
                return (ISSHExecutable)Activator.CreateInstance(type);

            return null;
        }

        public IUser User { get; set; }

        public ISSHService Service { get; set; }

        public IList<string> Arguments { get; set; }

        public bool IsCompleted { get; private set; }

        public bool IsRunning { get; private set; }

        public virtual bool AskForInput => true;

        public void HandleInput(IUserInput input)
        {
            if (!IsRunning)
            {
                IsRunning = true;

                if (OnStartCommand())
                    Complete();
            }
            else if (!IsCompleted)
            {
                HandleInputUpdate(input);
            }
        }

        protected abstract bool OnStartCommand();

        protected abstract void HandleInputUpdate(IUserInput input);

        protected abstract void OnEndCommand();

        protected IDirectory FindDirectory(string pathToNavTo)
        {
            return Service.FindDirectory(User, pathToNavTo);
        }

        protected IDirectory FindDirectory(Path pathToNavTo)
        {
            return Service.FindDirectory(User, pathToNavTo);
        }

        protected void Complete()
        {
            IsCompleted = true;

            if (IsRunning)
            {
                OnEndCommand();
                IsRunning = false;
            }
        }

    }
}
