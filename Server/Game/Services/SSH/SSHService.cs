﻿using GameAPI;
using GameAPI.IO;
using GameAPI.Missions;
using GameAPI.Services.SSH;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameServer.Game.Services.SSH
{
    [Service("ssh")]
    public class SSHService : MarshalByRefObject, IService, ISSHService
    {
        public string Name => "SSH";

        public ushort Port => 22;

        public IServiceManager Manager { get; set; }

        private IFile logFile;

        private IDictionary<IUser, IDirectory> usersCurrentDirectory;
        private IDictionary<IUser, ISSHExecutable> usersCurrentExecutable;

        public SSHService()
        {
            usersCurrentDirectory = new Dictionary<IUser, IDirectory>();
            usersCurrentExecutable = new Dictionary<IUser, ISSHExecutable>();
        }

        public void OnStart()
        {
            SSHCommandBase.Initialise();
        }

        public void OnConnect(IUser user, bool isOwner)
        {
            user.Console.SetSlowMode(true);

            usersCurrentExecutable.Add(user, null);

            user.Console.WriteLine($"Welcome to terminal 1.0");

            user.Console.WriteLine($"Type \"help\" to see commands!");

            if (Manager.Count == 1)
                user.Console.WriteLine($"{Manager.Count} service is currently running on this machine");
            else
                user.Console.WriteLine($"{Manager.Count} Services are currently running on this machine");

            usersCurrentDirectory.Add(user, Manager.LocalComputer.GetBootDriver().GetRootDirectory());

            WaitForInput(user);
        }

        public void OnDisconnect(IUser user)
        {
            user.Console.SetSlowMode(false);

            usersCurrentExecutable.Remove(user);
            usersCurrentDirectory.Remove(user);

            user.Console.WriteLine($"Goodbye.");
        }

        public void OnReceiveAPICall(IService caller, IApiPacket packet)
        {

        }

        public void HandleInput(IUser user, IUserInput input)
        {
            if (usersCurrentExecutable[user] == null)
            {
                if (input is UserLineInput lineInput)
                {
                    var inputData = lineInput.Line.SplitCommandLine();

                    var strExecutable = inputData.FirstOrDefault();
                    var args = inputData.Skip(1).ToArray();

                    if (!String.IsNullOrEmpty(strExecutable))
                    {
                        var executable = SSHCommandBase.CreateCommand(strExecutable);

                        if (executable == null)
                        {
                            var scriptFile = Manager.LocalComputer.GetBootDriver().GetRootDirectory().FindDirectory("bin")?.FindFile(strExecutable + ".script");

                            if (scriptFile != null && scriptFile.GetContent() is ScriptContent script)
                            {
                                executable = (ISSHExecutable)Activator.CreateInstance(script.GetScriptType());
                            }
                     
                        }

                        if (executable != null)
                        {
                            executable.Arguments = args.ToList();
                            executable.User = user;
                            executable.Service = this;

                            executable.HandleInput(input);

                            if (!executable.IsRunning || executable.IsCompleted)
                            {
                                if (executable.AskForInput)
                                {
                                    Log("Executed: " + lineInput.Line + "\n");
                                    WaitForInput(user);
                                }
                            }
                            else
                            {
                                usersCurrentExecutable[user] = executable;
                                user.Console.ReadLine();
                            }
                        }
                        else
                        {
                            Log($"'Failed executing '{strExecutable}' is not a recognized command or executable.\n");

                            user.Console.WriteLine($"'{strExecutable}' is not a recognized command or executable.\n");
                            WaitForInput(user);
                        }
                    }
                    else
                        WaitForInput(user);

                }
            }
            else
            {
                var executable = usersCurrentExecutable[user];

                executable.HandleInput(input);

                if (!executable.IsRunning || executable.IsCompleted)
                {
                    usersCurrentExecutable[user] = null;
                    WaitForInput(user);
                }
                else
                    user.Console.ReadLine();
            }
        }

        public IDirectory FindDirectory(IUser user, string pathToNavTo)
        {
            if (usersCurrentDirectory.TryGetValue(user, out IDirectory currentDirectory))
            {
                var segs = StringUtil.Segements(pathToNavTo);

                var newDir = pathToNavTo.StartsWith("/") ? currentDirectory.FileSystem.Root : currentDirectory;

                foreach (var seg in segs)
                {
                    if (Path.HasExtension(seg))
                        break;

                    if (seg == ".." && newDir.Name != "/")
                        newDir = newDir.PreviousDirectory;
                    else if (seg != ".")
                        newDir = newDir.FindDirectory(seg);

                    if (newDir == null)
                        break;
                }

                return newDir;
            }

            return null;
        }

        public IDirectory FindDirectory(IUser user, Path pathToNavTo)
        {
            if (usersCurrentDirectory.TryGetValue(user, out IDirectory currentDirectory))
            {
                var newDir = pathToNavTo.IsRoot ? currentDirectory.FileSystem.Root : currentDirectory;

                foreach (var seg in pathToNavTo.GetSegments())
                {
                    if (seg == ".." && newDir.Name != "/")
                        newDir = newDir.PreviousDirectory;
                    else if (seg != ".")
                        newDir = newDir.FindDirectory(seg);

                    if (newDir == null)
                        break;
                }

                return newDir;
            }

            return null;
        }

        public void ChangeDirectory(IUser user, IDirectory directory)
        {
            if (usersCurrentDirectory.ContainsKey(user))
            {
                usersCurrentDirectory.Remove(user);
                usersCurrentDirectory.Add(user, directory);
            }
        }

        public IDirectory GetCurrentDirectory(IUser user)
        {
            usersCurrentDirectory.TryGetValue(user, out IDirectory currentDirectory);
            return currentDirectory;
        }

        public void WaitForInput(IUser user)
        {
            if (usersCurrentDirectory.TryGetValue(user, out IDirectory currentDirectory))
            {
                user.Console.Write($"{Manager.LocalComputer.Address}:{currentDirectory.GetAbsolutePath()}>");
                user.Console.ReadLine();
            }
        }

        public void Log(string message)
        {
            if (logFile == null || logFile.IsDeleted)
                logFile = Manager.LocalComputer.GetBootDriver().GetRootDirectory().FindOrCreateDirectory("logs", true).FindOrAddFile("terminal_log.txt", true);

            if (logFile == null) return;

            if (logFile.GetContent() is TextContent terminalText)
                terminalText.Data += message;
            else
                terminalText = new TextContent(message);

            logFile.SetContent(terminalText);
        }

        public bool Equals(IService a, IService b) => a.Name == b.Name;
        public int GetHashCode(IService service) => base.GetHashCode() ^ service.GetHashCode();
    }
}
