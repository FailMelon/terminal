﻿using GameAPI;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("cd", "Allows you to change directory")]
    public class SSHChangeDirectoryCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                var newDir = FindDirectory(Arguments[0]);

                if (newDir != null)
                    Service.ChangeDirectory(User, newDir);
                else
                    User.Console.WriteLine($"cd: {Arguments[0]} No such file or directory");
            }
            else
                User.Console.WriteLine("cd: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
