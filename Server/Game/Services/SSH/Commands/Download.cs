﻿using GameAPI;
using GameAPI.IO;
using GameAPI.Missions;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("download", "Download a file from local computer to network")]
    public class SSHDownloadCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Path.TryParse(Arguments[0], out Path path))
                {
                    var dir = FindDirectory(path.GetPathWithoutEnd());
                    if (dir != null)
                    {
                        var file = dir.FindFile(path.GetEndNameWithExtension());
                        if (file != null && !file.IsDeleted)
                        {
                            Service.Log($"{User.LocalComputer.Address} downloading file {file.Name}");
                            User.Console.WriteLine($"Downloading file {file.Name}....");
                            
                            int i = 0;
                            var newFile = User.LocalComputer.GetBootDriver<HardDrive>().FindOrCreateDirectory("downloads", true).AddFile((() =>
                            {
                                if (i == 0)
                                {
                                    i++;
                                    return file.Name;
                                }
                                else
                                    return $"({i++}){file.Name}";
                            }), true);

                            var content = file.GetContent();
                            if (content != null)
                                newFile.SetContent(content);

                            User.LocalMissionManager.RegisterUserInteraction(new MissionTask(Service.Manager.LocalComputer, file, MissionTaskType.DownloadedFile, typeof(SSHService)));

                            file.Remove();

                            User.Console.WriteLine($"Downloaded file {file.Name}....");
                            Service.Log($"{User.LocalComputer.Address} downloaded file {file.Name}");
                        }
                    }
                }
            }

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
