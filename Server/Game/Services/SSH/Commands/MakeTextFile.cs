﻿using GameAPI;
using GameAPI.IO;
using GameServer.Game.IO;
using System.Text;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("mktxt", "Makes a text file")]
    public class SSHMakeTextCommand : SSHCommandBase
    {
        private IFile newFile;
        private StringBuilder newFileData;

        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Path.TryParse(Arguments[0], out Path path))
                {
                    var dir = FindDirectory(path.GetPathWithoutEnd());
                    if (dir != null)
                    {
                        newFileData = new StringBuilder();

                        newFile = dir.AddFile(path.GetEndNameWithExtensionOrDefault("txt"), true);
                        if (newFile != null)
                            return false;
                        else
                            User.Console.WriteLine($"mktxt: {Arguments[0]} file already exists!");
                    }
                    else
                        User.Console.WriteLine($"mktxt: {Arguments[0]} Couldn't find directory in path");
                }
                else
                    User.Console.WriteLine($"mktxt: {Arguments[0]} Invalid path either illegal characters or wrongly typed");
            }
            else
                User.Console.WriteLine("mktxt: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput genericInput)
        {

            if (genericInput is UserLineInput input && newFile != null)
            {
                if (input.Line == "!")
                    Complete();
                else
                    newFileData.AppendLine(input.Line);
            }
            else
                Complete();
        }

        protected override void OnEndCommand()
        {
            newFile?.SetContent(new TextContent(newFileData.ToString()));
        }
    }
}
