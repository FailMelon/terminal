﻿using GameAPI;
using GameAPI.IO;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("mkdir", "Makes folder in your current directory")]
    public class SSHMakeDirectoryCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Path.TryParse(Arguments[0], out Path path))
                {
                    var dir = FindDirectory(path.GetPathWithoutEnd());
                    if (dir != null)
                        dir.AddDirectory(path.GetEndName(), true);
                    else
                        User.Console.WriteLine($"mkdir: {Arguments[0]} Couldn't find directory in path");
                }
                else
                    User.Console.WriteLine($"mkdir: {Arguments[0]} Couldn't find directory in path");
            }
            else
                User.Console.WriteLine("mkdir: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }

    }
}
