﻿using GameAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarul.Structures;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("ls", "Lists all files and directories in current directory")]
    public class SSHListCommand : SSHCommandBase
    {
        private FuckMarshalColor black = new FuckMarshalColor(0, 0, 0);
        private FuckMarshalColor green = new FuckMarshalColor(0, 140, 0);
        private FuckMarshalColor red = new FuckMarshalColor(140, 0, 0);
        private FuckMarshalColor white = new FuckMarshalColor(255, 255, 255);


        protected override bool OnStartCommand()
        {
            var dirs = Service.GetCurrentDirectory(User).Directories;
            var files = Service.GetCurrentDirectory(User).Files;

            for (int i = 0; i < dirs.Count; i++)
            {
                User.Console.BackgroundColor = green;
                User.Console.Write("/" + dirs[i].Name);
                User.Console.BackgroundColor = black;


                if (i < dirs.Count - 1)
                    User.Console.Write(" ");
            }

            if (files.Count > 0 && dirs.Count > 0)
                User.Console.Write(" ");

            for (int i = 0; i < files.Count; i++)
            {
                User.Console.BackgroundColor = red;
                User.Console.Write(files[i].Name);
                User.Console.BackgroundColor = black;

                if (i < files.Count - 1)
                    User.Console.Write(" ");
            }

            User.Console.Write("\n");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
