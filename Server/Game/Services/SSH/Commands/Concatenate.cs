﻿using GameAPI;
using GameAPI.IO;
using GameServer.Game.IO;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("cat", "Allows to create single or multiple files, view contain of file, concatenate files and redirect output in terminal or files.")]
    public class SSHConcatenateCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Path.TryParse(Arguments[0], out Path path))
                {
                    var dir = FindDirectory(path.GetPathWithoutEnd());
                    if (dir != null)
                    {
                        var file = dir.FindFile(path.GetEndNameWithExtension());
                        if (file != null && !file.IsDeleted)
                        {
                            if (file.GetContent() is TextContent textContent)
                            {
                                using (textContent)
                                    User.Console.WriteLine(textContent.Data);
                            }
                            else
                                User.Console.WriteLine($"cat: {Arguments[0]} is not a readable file!");
                        }
                        else
                            User.Console.WriteLine($"cat: {Arguments[0]} No such file");
                    }
                    else
                        User.Console.WriteLine($"cat: {Arguments[0]} Couldn't find directory in path");
                }
                else
                    User.Console.WriteLine($"cat: {Arguments[0]} Invalid path either illegal characters or wrongly typed");
            }
            else
                User.Console.WriteLine("cat: invalid arguments");

            return true;
        }


        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }

    }
}
