﻿using GameAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("help")]
    public class SSHHelpCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            var builder = new StringBuilder();
            foreach (var cmd in GetCommandsDescription())
            {
                if (cmd.Name != "help")
                    builder.AppendLine(cmd.Name + " - " + cmd.Description);
            }

            builder.Remove(builder.Length - 1, 1);

            User.Console.WriteLine(builder.ToString());

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }

    }
}
