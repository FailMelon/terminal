﻿using GameServer.Game;
using GameAPI;
using System.Reflection;
using System.Text;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("service", "Manage Services start and stop and open")]
    public class SSHServiceCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count >= 1)
            {
                switch (Arguments[0].ToLower())
                {
                    case "start":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Start(Arguments[1]);
                        break;
                    case "stop":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Stop(Arguments[1]);
                        break;
                    case "restart":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Restart(Arguments[1]);
                        break;
                    case "open":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Open(Arguments[1]);
                        break;
                    case "close":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Close(Arguments[1]);
                        break;
                    default:
                        Help();
                        break;
                }
            }
            else
                Help();

            return true;
        }

        public void Help()
        {
            var builder = new StringBuilder();
            builder.AppendLine("Service allows you to manage Services running on this machine\n");

            builder.AppendLine("service start name - Starts a service");
            builder.AppendLine("service stop name - Stops a service");
            builder.AppendLine("service restart name - Restart a service");

            builder.AppendLine("service open name port - Opens a service so others can connect");
            builder.AppendLine("service close name - Closes a service so others cannot connect");

            User.Console.WriteLine(builder.ToString());
        }

        public void Start(string serviceName)
        {
            if (serviceName.ToLower() == "ssh")
            {
                User.Console.WriteLine("Cannot stop ssh service");
                return;
            }

            var serviceType = ServiceManager.GetServiceType(serviceName.ToLower());

            if (serviceType != null)
            {
                User.Console.WriteLine($"Starting {serviceName}");
                if (Service.Manager.Start(serviceType, false))
                    User.Console.WriteLine($"Started {serviceName}");
                else
                    User.Console.WriteLine($"Failed to start service {serviceName}");

            }
            else
                User.Console.WriteLine($"Failed to start {serviceName}");
        }

        public bool Stop(string serviceName)
        {
            if (serviceName.ToLower() == "ssh")
            {
                User.Console.WriteLine("Cannot stop ssh service");
                return false;
            }

            var Services = Service.Manager.GetServices();

            foreach(var service in Services)
            {
                var serviceAttrib = service.GetType().GetCustomAttribute<ServiceAttribute>();

                if (serviceAttrib.Name == serviceName.ToLower())
                {
                    User.Console.WriteLine($"Stopping {service.Name}");
                    if (Service.Manager.Stop(service))
                        User.Console.WriteLine($"Stopped {service.Name}");
                    else
                        User.Console.WriteLine($"Failed to stop {service.Name}");

                    return true;
                }
            }

            User.Console.WriteLine($"Couldn't find service {serviceName}");
            return false;
        }

        public void Restart(string serviceName)
        {
            if (Stop(serviceName))
                Start(serviceName);
        }

        public void Open(string serviceName)
        {
            var Services = Service.Manager.GetServices();

            foreach (var service in Services)
            {
                var serviceAttrib = service.GetType().GetCustomAttribute<ServiceAttribute>();

                if (serviceAttrib.Name == serviceName.ToLower())
                {
                    User.Console.WriteLine($"Opening service {service.Name}");
                    service.Manager.Open(service.Port);
                    return;
                }
            }

            User.Console.WriteLine($"Failed to open service {serviceName}");
        }

        public void Close(string serviceName)
        {
            var Services = Service.Manager.GetServices();

            foreach (var service in Services)
            {
                var serviceAttrib = service.GetType().GetCustomAttribute<ServiceAttribute>();

                if (serviceAttrib.Name == serviceName.ToLower())
                {
                    User.Console.WriteLine($"Opening service {service.Name}");
                    service.Manager.Close(service.Port);
                    break;
                }
            }

            User.Console.WriteLine($"Failed to open service {serviceName}");
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
