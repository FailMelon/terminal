﻿using GameAPI;
using GameAPI.IO;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("mv", "Moves a file to another directory")]
    public class SSHMoveCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 2)
            {
                if (Path.TryParse(Arguments[0], out Path existingFilePath) && Path.TryParse(Arguments[1], out Path newFilePath))
                {
                    var existingDir = Service.FindDirectory(User, existingFilePath);
                    if (existingDir != null)
                    {
                        var existingFile = existingDir.FindFile(existingFilePath.GetEndNameWithExtension());

                        if (existingFile != null)
                        {
                            var newDir = Service.FindDirectory(User, newFilePath);
                            if (newDir != null)
                            {
                                if (newDir.FindFile(newFilePath.GetEndNameWithExtension()) == null)
                                    existingDir.MoveFile(existingFile, newFilePath.GetEndName() + existingFilePath.GetExtension(), newDir);
                                else
                                    User.Console.WriteLine($"mv: {Arguments[1]} file already exists in path");

                            }
                            else
                                User.Console.WriteLine($"mv: {Arguments[1]} Couldn't find directory in path");
                        }
                        else
                            User.Console.WriteLine($"mv: {Arguments[0]} Couldn't find file in path");
                    }
                    else
                        User.Console.WriteLine($"mv: {Arguments[0]} Couldn't find directory in path");
                }
            }

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
