﻿using GameAPI;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("clear", "Clears the terminal screen")]
    public class SSHClearCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            User.Console.Clear();

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
