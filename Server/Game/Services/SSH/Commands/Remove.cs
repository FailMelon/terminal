﻿using GameAPI;
using GameAPI.IO;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("rm", "Removes a file or directory")]
    public class SSHRemoveCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Path.TryParse(Arguments[0], out Path path))
                {
                    var dir = FindDirectory(path);
                    if (dir != null)
                    {
                        dir.RemoveFile(path.GetEndNameWithExtension());
                    }
                    else
                        User.Console.WriteLine($"rm: {Arguments[0]} Couldn't find directory in path");
                }
                else
                    User.Console.WriteLine($"rm: {Arguments[0]} Invalid path either illegal characters or wrongly typed");
            }
            else
                User.Console.WriteLine("rm: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
