﻿using GameAPI;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("disconnect", "Disconnects you back to your local computer")]
    public class SSHDisconnectCommand : SSHCommandBase
    {
        public override bool AskForInput => false;

        protected override bool OnStartCommand()
        {
            ComputerManager.Instance.Disconnect(User);
            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
