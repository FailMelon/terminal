﻿using GameAPI;
using System;
using System.Net;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("connect", "Connects to another computer via an address")]
    public class SSHConnectCommand : SSHCommandBase
    {
        protected override bool OnStartCommand()
        {
            if (Arguments.Count == 1)
            {
                if (Dns.Get().TryResolve(Arguments[0], out var endPoint))
                {
                    if (!ComputerManager.Instance.AttemptConnect(User, endPoint.Address, endPoint.Port))
                        Console.WriteLine("Could not connect to remote host or already connected!");
                }
                else
                {
                    User.Console.WriteLine($"connect: invalid ip address or hostname ({Arguments[0]})");
                }
            }
            else
                User.Console.WriteLine("connect: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
