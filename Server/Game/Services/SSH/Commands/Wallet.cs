﻿using GameAPI;
using GameAPI.IO;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Game.Services.SSH.Commands
{
    [SSHCommand("wal", "Makes a new wallet file")]
    public class SSHWalletCommand : SSHCommandBase
    {
        private struct RequestData
        {
            public Path WalletPath { get; set; }
            public IFileSystem WalletFileSystem { get; set; }
            public long TimeStamp { get; set; }
        }

        private static Dictionary<string, RequestData> requestDatas = new Dictionary<string, RequestData>();

        private static string GenerateRequestID()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            int len = 5;

            var rnd = new Random();
            var b = new StringBuilder(len);

            for (int i = 0; i < len; i++)
                b.Append(chars[rnd.Next(chars.Length)]);

            return b.ToString();
        }

        protected override bool OnStartCommand()
        {
            if (Arguments.Count >= 1)
            {
                switch (Arguments[0].ToLower())
                {
                    case "make":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            CreateWallet(Arguments[1]);
                        break;
                    case "bal":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            Balance(Arguments[1]);
                        break;
                    case "req":
                        if (Arguments.Count != 2)
                            goto default;
                        else
                            OneTimeRequest(Arguments[1]);
                        break;
                    case "send":
                        if (Arguments.Count != 4)
                            goto default;
                        else
                            SendRequest(Arguments[1], Arguments[2], Arguments[3]);
                        break;
                    default:
                        HelpWallet();
                        break;
                }
            }
            else
                HelpWallet();

            return true;
        }

        public void HelpWallet()
        {
            var builder = new StringBuilder();
            builder.AppendLine("Wallets are used to store and send money\n");

            builder.AppendLine("wal bal file - shows the balance of the wallet");
            builder.AppendLine("wal make file - Creates a new wallet file");
            builder.AppendLine("wal req file - Requests a 60 second unique id assigned to the wallet for people to pay into");
            builder.AppendLine("wal send file id amount - Sends a specified amount of money to the wallet request id");

            User.Console.WriteLine(builder.ToString());
        }

        public void SendRequest(string strPath, string requestId, string strCoins)
        {
            if (Path.TryParse(strPath, out Path path))
            {
                var dir = FindDirectory(path.GetPathWithoutEnd());
                if (dir != null)
                {
                    var file = dir.FindFile(path.GetEndName("wal"));
                    if (file != null)
                    {
                        if (file.GetContent() is WalletContent myWalletContent)
                        {
                            if (requestDatas.TryGetValue(requestId, out RequestData data))
                            {
                                DateTime time = new DateTime(data.TimeStamp);
                                time = time.AddMinutes(1);

                                if (time.Subtract(DateTime.Now).TotalSeconds <= 0)
                                {
                                    requestDatas.Remove(requestId);
                                    User.Console.WriteLine($"wal send: id {requestId} has timed out!");
                                }
                                else
                                {
                                    if (decimal.TryParse(strCoins, out var coins))
                                    {
                                        if (Database.GetFileContent(data.WalletFileSystem, data.WalletPath) is WalletContent otherWalletContent)
                                        {
                                            if (myWalletContent.TryTakeCoins(coins))
                                            {
                                                otherWalletContent.AddCoins(coins);
                                                Database.SetFileContent(data.WalletFileSystem, data.WalletPath, otherWalletContent);
                                                file.SetContent(myWalletContent);

                                                User.Console.WriteLine($"{coins} have been sent to {requestId}!");
                                            }
                                            else
                                                User.Console.WriteLine($"wal send: you don't have {coins} to send to this id!");
                                        }
                                        else
                                            User.Console.WriteLine($"wal send: not a valid wallet file");
                                    }
                                    else
                                        User.Console.WriteLine($"wal send: invalid coin amount {strCoins}");
                                }
                            }
                            else
                                User.Console.WriteLine($"wal send: no id {requestId} exists or has timed out!");
                        }
                        else
                            User.Console.WriteLine($"wal send: {strPath} file is not a wallet file!");

                    }
                }
            }
        }

        public void OneTimeRequest(string strPath)
        {
            if (Path.TryParse(strPath, out Path path))
            {
                var dir = FindDirectory(path.GetPathWithoutEnd());
                if (dir != null)
                {
                    var file = dir.FindFile(path.GetEndName("wal"));
                    if (file != null)
                    {
                        if (file.GetContent() is WalletContent walletContent)
                        {
                            var id = GenerateRequestID();

                            while (requestDatas.TryGetValue(id, out RequestData data))
                            {
                                DateTime time = new DateTime(data.TimeStamp);
                                time = time.AddMinutes(1);

                                if (time.Subtract(DateTime.Now).TotalSeconds <= 0)
                                    requestDatas.Remove(id);

                                id = GenerateRequestID();
                            }

                            requestDatas.Add(id, new RequestData
                            {
                                WalletFileSystem = file.FileSystem,
                                WalletPath = file.AbsolutePath,
                                TimeStamp = DateTime.Now.Ticks,
                            });

                            User.Console.WriteLine($"Request id ({id}) you have 60 seconds before the request is deleted!");
                        }
                        else
                            User.Console.WriteLine($"wal request: {strPath} file is not a wallet file!");
                    }
                }
            }
        }

        public void CreateWallet(string strPath)
        {
            if (Path.TryParse(strPath, out Path path))
            {
                var dir = FindDirectory(path.GetPathWithoutEnd());
                if (dir != null)
                {
                    var file = dir.AddFile(path.GetEndName("wal"), true);
                    if (file != null)
                        file.SetContent(new WalletContent(0m));
                    else
                        User.Console.WriteLine($"wal create: {strPath} file already exists!");
                }
                else
                    User.Console.WriteLine($"wal create: {strPath} Couldn't find directory in path");
            }
            else
                User.Console.WriteLine($"wal create: {strPath} Invalid path either illegal characters or wrongly typed");
        }

        public void Balance(string strPath)
        {
            if (Path.TryParse(strPath, out Path path))
            {
                var dir = FindDirectory(path.GetPathWithoutEnd());
                if (dir != null)
                {
                    var file = dir.FindFile(path.GetEndName("wal"));
                    if (file != null)
                    {
                        if (file.GetContent() is WalletContent walletContent)
                            User.Console.WriteLine(walletContent.Coins + " Coins");
                        else
                            User.Console.WriteLine($"wal balance: {strPath} is not a readable wallet!");
                    }
                    else
                        User.Console.WriteLine($"wal balance: {strPath} No such file");
                }
                else
                    User.Console.WriteLine($"wal balance: {strPath} Couldn't find directory in path");
            }
            else
                User.Console.WriteLine($"wal balance: {strPath} Invalid path either illegal characters or wrongly typed");
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEndCommand() { }
    }
}
