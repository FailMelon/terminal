﻿using GameAPI;
using GameAPI.Services.SSH;
using System.Linq;
using System.Net;
using System.Text;

namespace GameServer.Game.Services.SSH.Commands
{
    public class ProbeScript : SSHScriptBase
    {
        protected override bool OnStart()
        {
            if (Arguments.Count == 1)
            {
                if (Dns.Get().TryResolve(Arguments[0], out IPEndPoint endPoint))
                {
                    if (ComputerManager.Instance.TryProbeComputer(endPoint.Address, out var Services))
                    {
                        if (Services.Count > 0)
                        {
                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine("Current Services running on this machine:");
                            foreach (var service in Services.OrderBy((s) => s.Port))
                            {
                                var isOpen = service.Manager.IsOpenService(service.Port);
                                if (isOpen)
                                    builder.AppendLine($"{service.Name} - Port: {service.Port} - Open");
                                else
                                    builder.AppendLine($"{service.Name} - Port: {service.Port} - Closed");
                            }

                            Write(builder.ToString());
                        }
                        else
                            WriteLine("There are no Services running on this computer!");
                    }
                    else
                        WriteLine("Could not connect to remote host");
                }
            }
            else
                WriteLine("probe: invalid arguments");

            return true;
        }

        protected override void HandleInputUpdate(IUserInput input) { }

        protected override void OnEnd() { }
    }
}
