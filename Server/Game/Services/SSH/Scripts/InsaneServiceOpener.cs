﻿using GameAPI;
using GameAPI.Services.SSH;
using System.Net;

namespace GameServer.Game.Services.SSH.Scripts
{
    public class InsaneServiceOpenerScript : SSHScriptBase
    {
        protected override bool OnStart()
        {
            if (Arguments.Count == 1)
            {
                if (StringUtil.TryGetIPEndPoint(Arguments[0], out IPEndPoint endPoint))
                {
                    if (ComputerManager.Instance.TryOpenService(endPoint))
                        WriteLine("Successfully opened port!");
                    else
                        WriteLine("Failed to open port");

                }
            }
            else
                WriteLine("service-opener: invalid arguments");


            return true;
        }

        protected override void HandleInputUpdate(IUserInput input)
        {

        }

        protected override void OnEnd()
        {

        }
    }
}
