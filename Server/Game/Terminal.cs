﻿using GameAPI;
using GameAPI.Packets;
using System;
using Tarul.Structures;

namespace GameServer.Game
{
    public class Terminal : MarshalByRefObject, ITerminal
    {
        private FuckMarshalColor m_backgroundColor;
        private FuckMarshalColor m_foregroundColor;

        public FuckMarshalColor BackgroundColor
        {
            get
            {
                return m_backgroundColor;
            }
            set
            {
                m_backgroundColor = value;

                ColorUpdatePacket pkt;
                pkt.foreground = new Color(m_foregroundColor.R, m_foregroundColor.G, m_foregroundColor.B);
                pkt.background = new Color(m_backgroundColor.R, m_backgroundColor.G, m_backgroundColor.B);
                User.SendPacket(pkt);
            }
        }
        public FuckMarshalColor ForegroundColor
        {
            get
            {
                return m_foregroundColor;
            }
            set
            {
                m_foregroundColor = value;

                ColorUpdatePacket pkt;
                pkt.foreground =  new Color(m_foregroundColor.R, m_foregroundColor.G, m_foregroundColor.B);
                pkt.background = new Color(m_backgroundColor.R, m_backgroundColor.G, m_backgroundColor.B);
                User.SendPacket(pkt);
            }
        }

        public User User { get; private set; }

        public int WindowWidth { get; private set; }
        public int WindowHeight { get; private set; }
        public int BufferWidth { get; private set; }
        public int BufferHeight { get; private set; }

        public Terminal(User user, int windowWidth, int windowHeight, int bufferWidth, int bufferHeight)
        {
            User = user;
            WindowWidth = windowWidth;
            WindowHeight = windowHeight;
            BufferWidth = bufferWidth;
            BufferHeight = bufferHeight;

            m_backgroundColor = new FuckMarshalColor(0, 0, 0);
            m_foregroundColor = new FuckMarshalColor(255, 255, 255);
        }

        public void SaveCursorPosition()
        {
            SaveCursorAndRestorePositionPacket pkt;
            pkt.save = true;
            User.SendPacket(pkt);
        }

        public void RestoreCursorPosition()
        {
            SaveCursorAndRestorePositionPacket pkt;
            pkt.save = false;
            User.SendPacket(pkt);
        }

        public void SetSlowMode(bool slowMode)
        {
            SlowModePacket pkt;
            pkt.slowMode = slowMode;
            User.SendPacket(pkt);
        }

        public void Write(string value)
        {
            WritePacket pkt;
            pkt.data = value;
            User.SendPacket(pkt);
        }

        public void WriteLine(string value)
        {
            WriteLinePacket pkt;
            pkt.data = value;
            User.SendPacket(pkt);
        }

        public void ResetColor()
        {
            m_foregroundColor = new FuckMarshalColor(255, 255, 255);
            m_backgroundColor = new FuckMarshalColor(0, 0, 0);

            ColorUpdatePacket pkt;
            pkt.foreground = new Color(255, 255, 255);
            pkt.background = new Color(0, 0, 0);
            User.SendPacket(pkt);
        }

        public void SetCursorPosition(int chPos, int lnPos)
        {
            CaretPositionPacket pkt;
            pkt.chPos = chPos;
            pkt.colPos = lnPos;
            User.SendPacket(pkt);
        }

        public void ReadLine()
        {
            if (!User.isWaitingForInput)
            {
                WaitForInputPacket pkt;
                pkt.lineInput = true;
                pkt.keyInput = false;
                User.SendPacket(pkt);

                User.isWaitingForInput = true;
            }
        }

        public void ReadKey()
        {
            if (!User.isWaitingForInput)
            {
                WaitForInputPacket pkt;
                pkt.lineInput = false;
                pkt.keyInput = true;
                User.SendPacket(pkt);

                User.isWaitingForInput = true;
            }
        }

        public void MoveBufferArea(int sourceX, int sourceY, int sourceWidth, int sourceHeight, int targetX, int targetY)
        {
            MoveBufferAreaPacket pkt;
            pkt.sourceX = sourceX;
            pkt.sourceY = sourceY;
            pkt.sourceWidth = sourceWidth;
            pkt.sourceHeight = sourceHeight;
            pkt.targetX = targetX;
            pkt.targetY = targetY;
            User.SendPacket(pkt);
        }

        public void Clear()
        {
            ClearPacket pkt;
            pkt.type = TerminalClearType.Buffer;
            User.SendPacket(pkt);
        }

        public void ClearLine()
        {
            ClearPacket pkt;
            pkt.type = TerminalClearType.Line;
            User.SendPacket(pkt);
        }
    }
}
