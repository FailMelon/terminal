﻿using System;
using System.Net;
using GameAPI;
using MongoDB.Bson;
using GameServer.Game.Definitions;
using GameServer.Game.IO;
using GameAPI.Missions;

namespace GameServer.Game
{
    public class AIUser : MarshalByRefObject, IUser, IDisposable 
    {
        public IComputer LocalComputer { get; private set; }

        public IComputer TargetComputer { get; private set; }

        public IMissionManager LocalMissionManager { get; private set; }

        public ITerminal Console { get; private set; }

        public string Username { get; set; }

        public string DatabaseName { get; private set; }

        public AIUser(string databaseName)
        {
            DatabaseName = databaseName;

            Console = new FakeTerminal(this);

            LocalMissionManager = new MissionManager(this);

            LocalComputer = Database.LoadComputerInstant(this);

            if (LocalComputer == null)
            {
                var cpuDef = Definition.FindDefinition<CPUDefinition>("cpu_test");
                var memDef = Definition.FindDefinition<MemoryDefinition>("mem_test");

                var localComputer = new Computer(new Hardware(cpuDef, memDef));
                localComputer.SetAddress(ComputerManager.Instance.GenerateAddress());
                localComputer.AssignUser(this);
                ComputerManager.Instance.RegisterComputer(localComputer);

                LocalComputer = localComputer;
            }

            if (!LocalComputer.HasBootDrive)
            {
                var hardDef = Definition.FindDefinition<HardDriveDefinition>("hd_test");
                LoadBootDrive(new FileSystem(), hardDef, true);
            }
        }


        public HardDrive LoadBootDrive(HardDriveDefinition def, bool isWritable)
        {
            var bootDrive = def.FactoryCreate(isWritable);
            LocalComputer.SetBootDrive(bootDrive);
            return bootDrive;
        }

        public HardDrive LoadBootDrive(FileSystem fs, HardDriveDefinition def, bool isWritable)
        {
            var bootDrive = new HardDrive(isWritable, def, fs);
            LocalComputer.SetBootDrive(bootDrive);
            return bootDrive;
        }

        public void SetTargetComputer(IComputer target)
        {
            TargetComputer = target;
        }

        public void Startup()
        {
            Database.CreateUserComputer(this, (Computer)LocalComputer);
        }

        public void Shutdown()
        {
            LocalComputer.Shutdown();

            Database.UnReserveIPAddress(LocalComputer.Address);
        }

        public void SendLineInput(string line)
        {
            TargetComputer?.HandleInput(this, new UserLineInput(line));
        }

        public void Dispose()
        {
            Database.CreateUserComputer(this, (Computer)LocalComputer);
        }

        public bool Equals(IUser a, IUser b)
        {
            if (a is AIUser userA && b is AIUser userB)
                return userA.DatabaseName == userB.DatabaseName;

            return false;
        }

        public int GetHashCode(IUser obj)
        {
            return base.GetHashCode() ^ (obj as AIUser).DatabaseName.GetHashCode();
        }
    }
}
