﻿using GameAPI;
using GameAPI.IO;
using MongoDB.Bson;
using GameServer.Game.Definitions;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game
{

    public class HardDrive : MarshalByRefObject, IDrive
    {
        public HardDriveDefinition Definition { get; private set; }

        public string MountPath { get; private set; }

        public bool IsWritable { get; private set; }

        [BsonIgnore]
        public bool IsMounted { get; private set; }

        [BsonIgnore]
        private IFileSystem m_fileSystem;

        public IFileSystem FileSystem
        {
            get => m_fileSystem;

            private set
            {
                m_fileSystem = value;
                m_fileSystem.AssignDrive(this);
            }
        }

        [BsonConstructor()]
        public HardDrive() { }

        public HardDrive(bool isWritable, HardDriveDefinition definition)
            : this(isWritable, definition, new FileSystem()) { }

        public HardDrive(bool isWritable, HardDriveDefinition definition, FileSystem filesystem)
        {
            IsWritable = isWritable;

            FileSystem = filesystem;

            Definition = definition;
        }

        public void Mount(Path path)
        {
            if (!IsMounted)
            {
                IsMounted = true;
                MountPath = path;
            }
        }

        public void UnMount()
        {
            IsMounted = false;
            MountPath = null;
        }


        public IFile AddFile(Path path, bool saveToDB)
        {
            return FileSystem.AddFile(path, saveToDB);
        }

        public IFile FindOrCreateFile(Path path, bool saveToDB)
        {
            var file = FileSystem.FindFile(path);
            return file ?? AddFile(path, saveToDB);
        }

        public IDirectory AddDirectory(Path path, bool saveToDB)
        {
            return FileSystem.AddDirectory(path, saveToDB);
        }

        public void RemoveDirectory(Path path)
        {
            FileSystem.RemoveDirectory(path);
        }

        public IDirectory FindDirectory(Path path)
        {
            return FileSystem.FindDirectory(path);
        }

        public IDirectory FindOrCreateDirectory(Path path, bool saveToDB)
        {
            var dir = FileSystem.FindDirectory(path);
            return dir ?? AddDirectory(path, saveToDB);
        }

        public IEnumerable<IDirectory> ListDirectories(Path path)
        {
            return FileSystem.ListDirectories(path);
        }

        public IEnumerable<IFile> ListFiles(Path path, bool recursive = false)
        {
            return FileSystem.ListFiles(path, recursive);
        }

        public IDirectory GetRootDirectory()
        {
            return FileSystem.Root;
        }

        public ObjectId GetFileSystemID()
        {
            if (FileSystem is FileSystem fs)
                return fs.ID;

            return ObjectId.Empty;
        }

        public void Remove()
        {
            Database.RemoveFileSystem(FileSystem);
        }
    }
}
