﻿using GameAPI;
using System;

namespace GameServer.Game
{
    public class FakeTerminal : MarshalByRefObject, ITerminal
    {
        public FuckMarshalColor BackgroundColor { get; set; }
        public FuckMarshalColor ForegroundColor { get; set; }

        public AIUser User { get; private set; }

        public int WindowWidth { get; private set; }
        public int WindowHeight { get; private set; }
        public int BufferWidth { get; private set; }
        public int BufferHeight { get; private set; }

        public FakeTerminal(AIUser user)
        {
            User = user;
            WindowWidth = 0;
            WindowHeight = 0;
            BufferWidth = 0;
            BufferHeight = 0;
        }

        public void SaveCursorPosition() { }

        public void RestoreCursorPosition() { }

        public void SetSlowMode(bool slowMode) { }

        public void Write(string value) { }

        public void WriteLine(string value) { }

        public void ResetColor() { }

        public void SetCursorPosition(int chPos, int lnPos) { }

        public void ReadLine() { }

        public void ReadKey() { }

        public void MoveBufferArea(int sourceX, int sourceY, int sourceWidth, int sourceHeight, int targetX, int targetY) { }

        public void Clear() { }

        public void ClearLine() { }
    }
}
