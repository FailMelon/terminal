﻿using GameAPI.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace GameServer.Game.IO
{
    public class File : MarshalByRefObject, IEqualityComparer<File>, IFile
    {
        public IFileSystem FileSystem { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IDirectory Owner { get; private set; }

        /// <summary>
        /// Gets the path of this file
        /// </summary>
        public string Name { get; private set; }

        public bool IsDeleted { get; private set; }

        public Path AbsolutePath { get; private set; }

        public File(string name, IDirectory owner, IFileSystem fileSystem)
        {
            Name = name;
            Owner = owner;
            FileSystem = fileSystem;
            AbsolutePath = Path.Combine(owner.GetAbsolutePath(), Name);
        }

        public void SetContent(IFileContent content)
        {
            Database.SetFileContent(this, content);
        }

        public IFileContent GetContent(params string[] projection)
        {
            if (projection != null && projection.Length > 0)
                return Database.GetFileContent(this, projection);
            else
                return Database.GetFileContent(this);
        }

        public void Remove()
        {
            Owner.RemoveFile(this);
        }

        public void MarkDeleted()
        {
            IsDeleted = true;
        }

        #region IEquality

        public bool Equals(File x, File y)
        {
            return x.AbsolutePath == y.AbsolutePath;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public int GetHashCode(File obj)
        {
            return obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ AbsolutePath.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
