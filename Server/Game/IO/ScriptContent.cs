﻿using GameAPI.IO;
using GameAPI.Services.SSH;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Reflection;

namespace GameServer.Game.IO
{
    public class ScriptContent : MarshalByRefObject, IFileContent
    {
        [BsonElement]
        private string type;

        [BsonElement]
        private string assembly;


        public ScriptContent() { }

        public ScriptContent SetType<T>() where T : ISSHExecutable
        {
            type = typeof(T).ToString();
            return this;
        }

        public ScriptContent SetType(Type executableType)
        {
            if (typeof(ISSHExecutable).IsAssignableFrom(executableType))
            {
                type = executableType.FullName;
                assembly = executableType.Assembly.FullName;
                return this;
            }

            return null;
        }

        public Type GetScriptType()
        {
            if (UserNode.Assemblies.TryGetValue(assembly, out Assembly val))
                return val.GetType(type);

            return Type.GetType(type);
        }
    }
}
