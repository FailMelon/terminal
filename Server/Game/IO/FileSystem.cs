﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using GameAPI;
using GameAPI.IO;
using System;
using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game.IO
{
    public class FileSystemSerializer : SerializerBase<FileSystem>
    {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, FileSystem value)
        {
            context.Writer.WriteStartDocument();
            context.Writer.WriteName("fs_id");
            context.Writer.WriteObjectId(value.ID);

            context.Writer.WriteEndDocument();
        }

        public override FileSystem Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            context.Reader.ReadStartDocument();
            context.Reader.ReadString();
                context.Reader.ReadStartDocument();
                ObjectId id = context.Reader.ReadObjectId();
                context.Reader.ReadEndDocument();
            context.Reader.ReadEndDocument();

            return Database.LoadFileSystem(id);
        }
    }

    [BsonSerializer(typeof(FileSystemSerializer))]
    public class FileSystem : MarshalByRefObject, IFileSystem
    {
        public ObjectId ID { get; private set; }

        public IDirectory Root { get; private set; }

        public IDrive Drive { get; private set; }

        public FileSystem()
            : this(ObjectId.GenerateNewId())
        {
            Database.CreateDirectory(this, Root);
        }

        public FileSystem(ObjectId id)
        {
            ID = id;

            Root = new Directory("/", this, null);
        }

        public void AssignDrive(IDrive drive)
        {
            Drive = drive;
        }

        public IFile AddFile(Path path, bool saveToDB)
        {
            IDirectory currentdir = Root;

            var segments = path.GetSegments();
            for (int i = 0; i < segments.Count; i++)
            {
                var segment = segments[i];

                currentdir = currentdir.FindDirectory(segment);
                if (currentdir == null)
                    break;
            }

            return currentdir?.AddFile(path.GetEndNameWithExtension(), saveToDB);
        }

        public IFile FindFile(string path)
        {
            IDirectory currentdir = Root;

            var segs = path.Segements();
            foreach (string seg in segs)
            {
                if (!string.IsNullOrEmpty(seg))
                {
                    if (seg.Contains("."))
                    {
                        return currentdir.FindFile(seg);
                    }
                    else
                    {
                        currentdir = currentdir.FindDirectory(seg);

                        if (currentdir == null)
                            return null;
                    }
                }
            }

            return null;
        }

        public IEnumerable<IDirectory> ListDirectories(Path path)
        {
            var directory = FindDirectory(path);

            if (directory != null)
            {
                foreach (var dir in directory.Directories)
                    yield return dir;
            }
        }

        public IEnumerable<IFile> ListFiles(Path path, bool recursive = false)
        {
            var directory = FindDirectory(path);

            if (directory != null)
            {
                if (recursive)
                {
                    var files = Internal_DirectoryRecursiveSearch(directory);

                    foreach (var file in files)
                        yield return file;
                }
                else
                {
                    foreach (var file in directory.Files)
                        yield return file;
                }
            }
        }

        private IEnumerable<IFile> Internal_DirectoryRecursiveSearch(IDirectory directory)
        {
            foreach (var dir in directory.Directories)
            {
                foreach (var file in Internal_DirectoryRecursiveSearch(dir))
                    yield return file;
            }

            foreach (var file in directory.Files)
                yield return file;
        }

        public IDirectory AddDirectory(Path path, bool saveToDB, bool recursive = false)
        {
            IDirectory currentdir = Root;

            var segments = path.GetSegments();
            for (int i = 0; i < segments.Count; i++)
            {
                var segment = segments[i];

                currentdir = currentdir.FindDirectory(segment);
                if (currentdir == null)
                    break;

            }

            return currentdir?.AddDirectory(path.GetEndName(), saveToDB);
        }

        public void RemoveDirectory(Path path)
        {

        }

        public IDirectory FindDirectory(Path path)
        {
            IDirectory currentdir = Root;

            var segments = path.GetAllSegments();
            foreach (string segment in segments)
            {
                if (!string.IsNullOrEmpty(segment))
                {
                    currentdir = currentdir.FindDirectory(segment);

                    if (currentdir == null)
                        return null;
                }
            }

            return currentdir;
        }

        public void LoadFileSystem(List<string> paths)
        {
            foreach (string path in paths)
            {
                Directory directory = (Directory)Root;

                var segs = StringUtil.Segements(path);
                foreach (string seg in segs)
                {
                    if (directory != null)
                    {
                        if (seg.Contains("."))
                        {
                            directory.Internal_AddFile(seg);
                            break;
                        }
                        else
                        {
                            if (directory.FindDirectory(seg) is Directory dir)
                                directory = dir;
                            else
                                directory = directory.Internal_AddDirectory(seg);
                        }
                    }
                }
            }
        }
    }
}
