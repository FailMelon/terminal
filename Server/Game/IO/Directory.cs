﻿using System.Collections.Generic;
using System.Linq;
using System;
using GameAPI.IO;

namespace GameServer.Game.IO
{

    public class Directory : MarshalByRefObject, IEqualityComparer<Directory>, IDirectory
    {
        public IFileSystem FileSystem { get; private set; }

        /// <summary>
        /// Gets the path of this directory
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Previous directory this directory is in
        /// </summary>
        public IDirectory PreviousDirectory { get; private set; }

        private List<IDirectory> _directories;
        /// <summary>
        /// Directories within our directory
        /// </summary>
        public IReadOnlyList<IDirectory> Directories => _directories.AsReadOnly();

        private List<IFile> _files;
        /// <summary>
        /// Files within our directory
        /// </summary>
        public IReadOnlyList<IFile> Files => _files.AsReadOnly();

        public Directory(string name, IFileSystem fileSystem, IDirectory previousDirectory)
        {
            Name = name;
            FileSystem = fileSystem;
            PreviousDirectory = previousDirectory;
            _directories = new List<IDirectory>();
            _files = new List<IFile>();
        }

        public Directory Internal_AddDirectory(string directoryName)
        {
            if (_directories.Find((f) => f.Name == directoryName) == null)
            {
                var dir = new Directory(directoryName, FileSystem, this);
                _directories.Add(dir);

                return dir;
            }

            return null;
        }

        public IDirectory AddDirectory(string directoryName, bool saveToDB)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return null;

            if (_directories.Find((f) => f.Name == directoryName) == null)
            {
                var dir = new Directory(directoryName, FileSystem, this);
                _directories.Add(dir);

                if (saveToDB)
                    Database.CreateDirectory(FileSystem, dir);

                return dir;
            }

            return null;
        }

        public void RemoveDirectory(string directory)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return;

            _directories.RemoveAll((d) => d.Name == directory);
        }


        public File Internal_AddFile(string fileName)
        {
            if (!fileName.Contains(".")) fileName = fileName + ".file";

            if (_files.Find((f) => f.Name == fileName) == null)
            {
                var file = new File(fileName, this, FileSystem);
                _files.Add(file);
                return file;
            }

            return null;
        }

        public IFile AddFile(string fileName, bool saveToDB)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return null;

            if (!fileName.Contains(".")) fileName = fileName + ".file";

            if (_files.Find((f) => f.Name == fileName) == null)
            {
                var file = new File(fileName, this, FileSystem);
                _files.Add(file);

                if (saveToDB)
                {
                    Database.CreateFile(FileSystem, file);
                }

                return file;
            }

            return null;
        }

        public IFile AddFile(Func<string> func, bool saveToDB)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return null;

            var newFileName = func();
            if (!newFileName.Contains(".")) newFileName = newFileName + ".file";

            while (_files.Find((f) => f.Name == newFileName) != null)
            {
                newFileName = func();
                if (!newFileName.Contains(".")) newFileName = newFileName + ".file";
            }

            var file = new File(newFileName, this, FileSystem);
            _files.Add(file);

            if (saveToDB)
                Database.CreateFile(FileSystem, file);

            return file;
        }

        public void RemoveFile(string fileName)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return;

            if (!fileName.Contains(".")) fileName = fileName + ".file";

            var file = _files.Find((f) => f.Name == fileName);
            if (_files.Remove(file))
            {
                Database.RemoveFile(file);

                (file as File).MarkDeleted();
            }
        }

        public void RemoveFile(IFile file)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return;

            if (_files.Remove(file))
            {
                Database.RemoveFile(file);
                (file as File).MarkDeleted();
            }
        }

        public void MoveFile(IFile oldFile, string newFileName, IDirectory newDirectory)
        {
            if (FileSystem.Drive == null || !FileSystem.Drive.IsWritable) return;

            Database.MoveFile(oldFile.FileSystem, oldFile, Path.Combine(newDirectory.GetAbsolutePath(), newFileName));

            if (_files.Remove(oldFile))
                newDirectory.AddFile(newFileName, false);
        }

        public IFile FindFile(string fileName)
        {
            if (!fileName.Contains(".")) fileName = fileName + ".file";

            return Files.FirstOrDefault((d) => d.Name == fileName);
        }

        public IFile FindOrAddFile(string fileName, bool saveToDB)
        {
            var file = FindFile(fileName);
            return file ?? AddFile(fileName, saveToDB);
        }

        public IDirectory FindDirectory(string directoryName)
        {
            return Directories.FirstOrDefault((d) => d.Name == directoryName);
        }

        public IDirectory FindOrCreateDirectory(string directoryName, bool saveToDB)
        {
            var dir = FindDirectory(directoryName);
            return dir ?? AddDirectory(directoryName, saveToDB);
        }

        public Path GetAbsolutePath()
        {
            if (Name == "/") return "/";

            var path = new List<string>() { Name };
            var currentDir = PreviousDirectory;
            while(currentDir != null && currentDir.Name != "/")
            {
                path.Add(currentDir.Name);
                currentDir = currentDir.PreviousDirectory;
            }

            path.Reverse();

            return "/" + string.Join("/", path);
        }

        #region IEquality

        public bool Equals(Directory x, Directory y)
        {
            return x.Name == y.Name;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public int GetHashCode(Directory obj)
        {
            return obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Name.GetHashCode();
        }
        
        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
