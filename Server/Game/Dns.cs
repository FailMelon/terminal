﻿using System;
using System.Collections.Generic;
using System.Net;

namespace GameServer.Game
{
    public class Dns : MarshalByRefObject
    {
        private static Dns Instance;

        public static Dns Get()
        {
            return Instance;
        }

        public static void Set(Dns dns)
        {
            Instance = dns;
        }

        public static void New()
        {
            Instance = new Dns();
        }

        private Dictionary<string, IPEndPoint> dnsEntry;
        public Dns()
        {
            dnsEntry = new Dictionary<string, IPEndPoint>();
        }

        public bool RegisterDnsEntry(string lookup, IPEndPoint endPoint)
        {
            lookup = lookup.ToLower();

            if (dnsEntry.ContainsKey(lookup))
                return false;

            dnsEntry.Add(lookup, endPoint);
            return true;
        }

        public IPEndPoint Resolve(string lookup)
        {
            IPEndPoint endpoint = null;

            if (lookup.Contains(":"))
            {
                var splitParse = lookup.Split(':');

                if (IPAddress.TryParse(splitParse[0], out IPAddress address))
                {
                    if (ushort.TryParse(splitParse[1], out ushort port))
                        endpoint = new IPEndPoint(address, port);
                    else
                        endpoint = new IPEndPoint(address, 22);
                }
                else
                {
                    if (dnsEntry.TryGetValue(splitParse[0], out IPEndPoint endPoint))
                    {
                        if (ushort.TryParse(splitParse[1], out ushort port))
                            endpoint = new IPEndPoint(endPoint.Address, port);
                        else
                            endpoint = endPoint;
                    }
                }
            }
            else
            {
                if (IPAddress.TryParse(lookup, out IPAddress address))
                {
                    endpoint = new IPEndPoint(address, 22);
                }
                else
                {
                    if (dnsEntry.TryGetValue(lookup, out IPEndPoint endPoint))
                    {
                        endpoint = endPoint;
                    }
                }
            }

            return endpoint;
        }

        public bool TryResolve(string lookup, out IPEndPoint endpoint)
        {
            endpoint = null;

            lookup = lookup.ToLower();

            if (lookup.Contains(":"))
            {
                var splitParse = lookup.Split(':');

                if (IPAddress.TryParse(splitParse[0], out IPAddress address))
                {
                    if (ushort.TryParse(splitParse[1], out ushort port))
                        endpoint = new IPEndPoint(address, port);
                    else
                        endpoint = new IPEndPoint(address, 22);
                }
                else
                {
                    if (dnsEntry.TryGetValue(splitParse[0], out IPEndPoint endPoint))
                    {
                        if (ushort.TryParse(splitParse[1], out ushort port))
                            endpoint = new IPEndPoint(endPoint.Address, port);
                        else
                            endpoint = endPoint;
                    }
                }
            }
            else
            {
                if (IPAddress.TryParse(lookup, out IPAddress address))
                {
                    endpoint = new IPEndPoint(address, 22);
                }
                else
                {
                    if (dnsEntry.TryGetValue(lookup, out IPEndPoint endPoint))
                    {
                        endpoint = endPoint;
                    }
                }
            }

            return endpoint != null;
        }
    }
}
