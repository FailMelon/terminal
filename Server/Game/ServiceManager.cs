﻿using GameAPI;
using GameAPI.Missions;
using GameServer.Game.Services.SSH;
using Services.SSH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.Threading.Tasks;

namespace GameServer.Game
{
    public class ServiceManager : MarshalByRefObject, IServiceManager
    {
        private static Dictionary<string, Type> serviceTypes;

        static ServiceManager()
        {
            serviceTypes = new Dictionary<string, Type>();
        }

        public static void Initialise(Assembly assembly)
        {
            foreach(var type in assembly.GetTypes())
            {
                if (typeof(IService).IsAssignableFrom(type))
                {
                    var serviceAttrib = type.GetCustomAttribute<ServiceAttribute>();
                    if (serviceAttrib != null)
                        serviceTypes.Add(serviceAttrib.Name, type);
                }
            }
        }

        public static Type GetServiceType(string serviceName)
        {
            if (serviceTypes.TryGetValue(serviceName, out Type serviceType))
                return serviceType;

            return null;
        }

        private sealed class ServiceInfo
        {
            public IService Service { get; set; }
            public bool Loopback { get; set; }
            public AppDomain Domain { get; set; }
        }

        public IComputer LocalComputer { get; private set; }

        public int Count => services.Count;

        public IDictionary<IUser, int> ConnectedUsers { get; private set; }

        private IDictionary<int, ServiceInfo> services;

        public ServiceManager(IComputer localComputer)
        {
            services = new Dictionary<int, ServiceInfo>();
            ConnectedUsers = new Dictionary<IUser, int>();

            LocalComputer = localComputer;
        }

        #region User Handling

        public bool Connect(int port, IUser user, bool isLoopback)
        {
            if (!ConnectedUsers.ContainsKey(user))
            {
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                {
                    if (!serviceInfo.Loopback || isLoopback)
                    {
                        user.LocalMissionManager.RegisterUserInteraction(new MissionTask(LocalComputer, null, MissionTaskType.Connect, serviceInfo.Service.GetType()));

                        serviceInfo.Service.OnConnect(user, isLoopback);
                        ConnectedUsers.Add(user, serviceInfo.Service.Port);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool CanConnect(int port, IUser user, bool isLoopback)
        {
            if (!ConnectedUsers.TryGetValue(user, out int connectedPort) || connectedPort != port)
            {
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                    return !serviceInfo.Loopback || isLoopback;
            }

            return false;
        }

        public bool Disconnect(IUser user)
        {
            if (ConnectedUsers.TryGetValue(user, out int port))
            {
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                {
                    user.LocalMissionManager.RegisterUserInteraction(new MissionTask(LocalComputer, null, MissionTaskType.Disconnect, serviceInfo.Service.GetType()));

                    serviceInfo.Service.OnDisconnect(user);
                    ConnectedUsers.Remove(user);

                    return true;
                }
            }

            return false;
        }

        public void HandleUserInput(IUser user, IUserInput input)
        {
            if (ConnectedUsers.TryGetValue(user, out int port))
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                {
                    serviceInfo.Service.HandleInput(user, input);
                }

        }

        public bool IsConnected(IUser user, IService service)
        {
            if (ConnectedUsers.TryGetValue(user, out int port))
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                    return service.Name == serviceInfo.Service.Name;

            return false;
        }

        public bool IsConnected<T>(IUser user) where T : IService
        {
            if (ConnectedUsers.TryGetValue(user, out int port))
                if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                    return serviceInfo.Service is T;

            return false;
        }


        #endregion

        #region Service Handling

        public bool Start(Type serviceType, bool openPort)
        {
            IService service;
            if (serviceType == typeof(SSHService))
            {
                service = (IService)Activator.CreateInstance(serviceType);

                if (services.ContainsKey(service.Port))
                    return false;

                service.Manager = this;

                services.Add(service.Port, new ServiceInfo { Service = service, Loopback = !openPort, Domain = null });
            }
            else
            {


                var evidence = new Evidence();


                var domain = AppDomain.CreateDomain($"ComputerService-{LocalComputer.Address}:{openPort}", evidence, new AppDomainSetup
                {
                    ApplicationBase = Environment.CurrentDirectory
                });

                domain.FirstChanceException += Domain_FirstChanceException;

                domain.SetData("computerManager", ComputerManager.Instance);
                domain.SetData("dns", Dns.Get());
                domain.SetData("localComputer", (ILocalComputer)LocalComputer);

                domain.DoCallBack(() =>
                {
                    Dns.Set((Dns)AppDomain.CurrentDomain.GetData("dns"));
                });

                service = (IService)domain.CreateInstanceAndUnwrap(serviceType.Assembly.FullName, serviceType.FullName,
                    true, BindingFlags.CreateInstance, null, new object[] { }, null, null);

                service.Manager = this;

                if (services.ContainsKey(service.Port))
                {
                    AppDomain.Unload(domain);
                    return false;
                }

                services.Add(service.Port, new ServiceInfo { Service = service, Loopback = !openPort, Domain = domain });
            }

            service.OnStart();
            return true;
        }

        public bool Start<T>(bool openPort) where T : IService
        {
            return Start(typeof(T), openPort);
        }

        private void Domain_FirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            Log.Exception(e.Exception);
        }

        public bool Open(int port)
        {
            if (services.ContainsKey(port))
            {
                services[port].Loopback = false;
                return true;
            }

            return false;
        }

        public bool Close(int port)
        {
            if (services.ContainsKey(port))
            {
                services[port].Loopback = true;
                return true;
            }

            return false;
        }

        public bool Stop(IService service) => Stop(service.Port);

        public bool Stop(int port)
        {
            foreach(var keyValue in ConnectedUsers)
            {
                if (keyValue.Value == port)
                    ComputerManager.Instance.Disconnect(keyValue.Key);
            }

            if (services.TryGetValue(port, out ServiceInfo info))
            {
                if (info.Domain != null)
                {
                    info.Domain.FirstChanceException -= Domain_FirstChanceException;

                    Task.Run(() => AppDomain.Unload(info.Domain));   
                }

                return services.Remove(port);
            }

            return false;
        }

        public bool IsOpenService(int port)
        {
            if (services.TryGetValue(port, out ServiceInfo serviceInfo))
                return !serviceInfo.Loopback;

            return false;
        }

        public bool IsRunningService(int port)
        {
            return services.ContainsKey(port);
        }

        public T FindService<T>() where T : IService
        {
            foreach(var serviceInfo in services.Values)
                if (serviceInfo.Service is T)
                    return (T)serviceInfo.Service;

            return default;
        }

        public IList<IService> GetServices() => services.Values.Select((serviceInfo) => serviceInfo.Service).ToList();

        public double GetTotalProcessorTime()
        {
            double totalTime = 0;

            foreach(var info in services)
                if (info.Value.Domain != null)
                    totalTime += info.Value.Domain.MonitoringTotalProcessorTime.TotalMilliseconds;

            return totalTime;
        }

        public long GetTotalMemoryUsage()
        {
            long totalMemory = 0;

            foreach (var info in services)
                if (info.Value.Domain != null)
                    totalMemory += info.Value.Domain.MonitoringSurvivedMemorySize;

            return totalMemory;
        }

        public void Shutdown()
        {
            foreach (int port in services.Keys)
            {
                foreach (var keyValue in ConnectedUsers)
                {
                    if (keyValue.Value == port)
                        ComputerManager.Instance.Disconnect(keyValue.Key);
                }
            }

            foreach (var service in services)
            {
                if (service.Value.Domain != null)
                {
                    service.Value.Domain.FirstChanceException -= Domain_FirstChanceException;
                    Task.Run(() => AppDomain.Unload(service.Value.Domain));
                }
            }


            services.Clear();
            ConnectedUsers.Clear();
        }

        #endregion
    }
}
