﻿using GameAPI;
using System;
using System.Reflection;

namespace GameServer.Game
{
    public interface INode
    {
        IComputer LocalComputer { get; }

        void RunCode(string path, string code, Action<Scripting.ProgramCompiler.Message?, Assembly> finished, params string[] args);
    }
}
