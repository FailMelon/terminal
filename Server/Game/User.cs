﻿using GameAPI;
using GameAPI.Packets;
using GameServer.Game.Definitions;
using GameServer.Game.IO;
using GameServer.Game.Missions;
using GameServer.Networking;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Tarul;
using Tarul.Threading;
using GameAPI.Missions;
using GameServer.Game.Services.SSH;
using GameServer.Game.Services.SSH.Commands;
using GameAPI.IO;
using GameServer.Scripting;
using System.Text;
using System.Reflection;

namespace GameServer.Game
{
    public class User : MarshalByRefObject, IUser
    {
        /// <summary>
        /// Gets the username of this user
        /// </summary>
        public string Username { get; private set; }

        public string DatabaseName => Username;

        /// <summary>
        /// Network connection of the user
        /// </summary>
        public INetConnection NetConnection { get; protected set; }

        public IComputer LocalComputer { get; private set; }

        public IComputer TargetComputer { get; private set; }

        public IMissionManager LocalMissionManager { get; private set; }

        public ITerminal Console { get; private set; }

        public bool isWaitingForInput;

        private TimedActionDispatcher timedActionDispatcher;

        private readonly object startupLock = new object();

        private INode node;

        public User(INetConnection connection)
        {
            NetConnection = connection;
            Username = connection.Auth.Username;

            connection.Disconnected += Connection_OnDisconnect;

            connection.SubscribeToPacket<LineInputPacket>(OnPacket);
            connection.SubscribeToPacket<KeyInputPacket>(OnPacket);
            connection.SubscribeToPacket<EnteredAndInfoPacket>(OnPacket);

            LocalMissionManager = new MissionManager(this);

            Log.Debug($"User '{Username}' connected");

            timedActionDispatcher = new TimedActionDispatcher();
            timedActionDispatcher.EnqueueAction(1, PerformanceChecker, true);

            EnterWorldPacket pkt;
            SendPacket(pkt);
        }

        private void PerformanceChecker()
        {
            lock (startupLock)
            {
                if (LocalComputer != null)
                {
                    var percentageOfProcess = LocalComputer.Services.GetTotalProcessorTime() / Process.GetCurrentProcess().TotalProcessorTime.TotalMilliseconds;

                    GC.Collect();
                    var memoryUsage = LocalComputer.Services.GetTotalMemoryUsage();

                    SendPacket(new ComputerPerformancePacket
                    {
                        activeMemory = memoryUsage,
                        currentProcessingTime = (int)(WindowsHardwareInfo.CurrentProcessMhzUsage * percentageOfProcess)
                    });

                    var services = LocalComputer.Services?.GetServices();

                    if (services != null)
                    {
                        var strServices = new List<string>();

                        foreach (var service in services)
                            strServices.Add(service.Name);

                        SendPacket(new ComputerServicesPacket
                        {
                            services = strServices
                        });
                    }
                }
            }
        }

        public void SetTargetComputer(IComputer target)
        {
            TargetComputer = target;

            PerformanceChecker();
        }

        private void Connection_OnDisconnect(INetConnection sender, string message)
        {
            Log.Debug($"User '{Username}' disconnected");

            ComputerManager.Instance.Shutdown(this);

            timedActionDispatcher?.Stop();

            if (LocalComputer != null)
                ComputerManager.Instance.UnRegisterComputer(LocalComputer);

            LocalMissionManager?.ShutdownMissions();
        }

        private void OnPacket(INetConnection sender, LineInputPacket pkt)
        {
            if (isWaitingForInput)
            {
                isWaitingForInput = false;

                TargetComputer?.HandleInput(this, new UserLineInput(pkt.input));

            }
        }

        private void OnPacket(INetConnection sender, KeyInputPacket pkt)
        {
            if (isWaitingForInput)
            {
                isWaitingForInput = false;

                TargetComputer?.HandleInput(this, new UserKeyInput(pkt.control, pkt.shift, pkt.key));
            }
        }

        private void OnPacket(INetConnection sender, EnteredAndInfoPacket pkt)
        {
            Console = new Terminal(this, pkt.terminalWindowWidth, pkt.terminalWindowHeight, pkt.terminalBufferWidth, pkt.terminalBufferHeight);

            Database.LoadComputer(this, (computers) =>
            {
                if (computers.Count > 0)
                {
                    lock (startupLock)
                    {
                        LocalComputer = computers.First();

                        LocalComputer.Services.Start<SSHService>(false);

                        if (!LocalComputer.HasAddress)
                            LocalComputer.SetAddress(ComputerManager.Instance.GenerateAddress());

                        if (!LocalComputer.HasBootDrive)
                        {
                            LocalComputer.SetBootDrive(HardDriveDefinition.FactoryCreate("hd_test", true));

                            LocalComputer.GetBootDriver<HardDrive>().AddFile("wallet.wal", true).SetContent(new WalletContent(100.00m));
                            LocalComputer.GetBootDriver<HardDrive>().FindOrCreateDirectory("bin", true).AddFile("probe.script", true).SetContent(new ScriptContent().SetType<ProbeScript>());
                        }

                        LocalMissionManager.LoadMissions();

                        ComputerManager.Instance.ConnectWithByPass(this, LocalComputer.Address, 22);
                    }
                }
                else
                { 
                    var cpuDef = Definition.FindDefinition<CPUDefinition>("cpu_test");
                    var memDef = Definition.FindDefinition<MemoryDefinition>("mem_test");

                    lock (startupLock)
                    {
                        LocalComputer = new Computer(new Hardware(cpuDef, memDef));

                        LocalComputer.SetAddress(ComputerManager.Instance.GenerateAddress());

                        LocalComputer.SetBootDrive(HardDriveDefinition.FactoryCreate("hd_test", true));

                        LocalComputer.GetBootDriver<HardDrive>().AddFile("wallet.wal", true).SetContent(new WalletContent(100.00m));
                        LocalComputer.GetBootDriver<HardDrive>().FindOrCreateDirectory("bin", true).AddFile("probe.script", true).SetContent(new ScriptContent().SetType<ProbeScript>());

                        LocalMissionManager.StartMission<TutorialMission>();

                        Database.CreateUserComputer(this, (Computer)LocalComputer);

                        ComputerManager.Instance.RegisterComputer(LocalComputer);

                        LocalComputer.Services.Start<SSHService>(false);

                    }


                    Task.Run(async () =>
                    {
                        Console.SetSlowMode(true);
                        await Task.Delay(100);
                        Console.WriteLine("Hello");
                        await Task.Delay(2000);
                        Console.WriteLine("If you are receiving this then you have just booted up terminal for the first time.");
                        await Task.Delay(5000);
                        Console.WriteLine("Well... Welcome, i hope you are ready.");
                        await Task.Delay(5000);
                        Console.WriteLine("This is not an easy world you are about to enter, script kiddies, programmers, elite hackers.");
                        await Task.Delay(5000);
                        Console.WriteLine("You name it and they are here, dont know much? you'll learn, or rather you'll have to learn.");
                        await Task.Delay(5000);
                        Console.WriteLine("Know a lot? you'll probably go far.     probably.");
                        await Task.Delay(5000);
                        Console.WriteLine("The biggest flaw in your security is you.");
                        await Task.Delay(5000);
                        Console.WriteLine("Or, if you're trusting perhaps one of your team members.");
                        await Task.Delay(5000);
                        Console.WriteLine("What are you here for? Money? Fame?");
                        await Task.Delay(5000);
                        Console.WriteLine("It matters not, just remember one thing.");
                        await Task.Delay(5000);
                        Console.WriteLine("Security is a matter of trust.");
                        await Task.Delay(5000);

                        Console.SetSlowMode(false);

                        Console.Clear();

                        var ipEndPoint = Dns.Get().Resolve("chat.hack");

                        if (!ComputerManager.Instance.AttemptConnect(this, ipEndPoint.Address, ipEndPoint.Port))
                            ComputerManager.Instance.ConnectWithByPass(this, LocalComputer.Address, 22);
                    });
                }

                var code = new StringBuilder();

                code.AppendLine("protected override bool OnStart()");
                code.AppendLine("{");
                    code.AppendLine("WriteLine(\"Starting!\");");
                    code.AppendLine("return true;");
                code.AppendLine("}");

                code.AppendLine("protected override void HandleInputUpdate(IUserInput input)");
                code.AppendLine("{");
                    code.AppendLine("WriteLine(\"HandleUserInput!\");");
                code.AppendLine("}");

                code.AppendLine("protected override void OnEnd()");
                code.AppendLine("{");
                    code.AppendLine("WriteLine(\"Stopping!\");");
                code.AppendLine("}");

                node = new UserNode(LocalComputer);
                node.RunCode("UserProgram", code.ToString(), (m, a) => {

                    var type = a.GetType("MyUserScript");

                    LocalComputer.GetBootDriver<HardDrive>().FindOrCreateDirectory("bin", true).FindOrAddFile("TestScript.script", true).
                        SetContent(new ScriptContent().SetType(type));

                });

            });
        }

        public void SendPerformanceDetails(long memory, long processingTime)
        {

        }

        #region Networking Helpers

        /// <summary>
        /// Sends a packet to this user
        /// </summary>
        /// <param name="packet"></param>
        public virtual void SendPacket(IBitSerialisable packet)
        {
            if (NetConnection != null)
                NetConnection.SendPacket(packet);
        }

        public bool Equals(IUser a, IUser b)
        {
            if (a is User userA && b is User userB)
                return userA.Username == userB.Username;

            return false;
        }

        public int GetHashCode(IUser obj)
        {
            return base.GetHashCode() ^ (obj as User).Username.GetHashCode();
        }

        #endregion
    }
}
