﻿using GameAPI;
using GameAPI.IO;
using GameServer.ChatBot;
using GameServer.ChatBot.Rules;
using GameServer.Game.AI;
using GameServer.Game.Definitions;
using GameServer.Game.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.Game
{
    public class GameWorld : IGameWorld
    {
        public void Start()
        {
            new CPUDefinition("cpu_test", "cpu", ByteMath.Kilobyte * 500, 1);
            new MemoryDefinition("mem_test", ByteMath.Kilobyte * 500);
            new HardDriveDefinition("hd_test", ByteMath.Kilobyte * 500);


            Log.Info("[GameWorld] Loading Dns");
            Dns.New();

            Log.Info("[GameWorld] Starting Chat AI");
            var globalAI = new ChatServerAI("global_chat_server_ai");

            var file = globalAI.LocalComputer.GetBootDriver<HardDrive>().FindOrCreateFile("ultimateWallet.wal", true);
            file?.SetContent(new WalletContent(1000000m));

            ComputerManager.Instance.AttemptConnect(new BobChatAI("global_bob"), globalAI.LocalComputer.Address, 6697);
        }

        public void Shutdown()
        {

        }
    }
}
