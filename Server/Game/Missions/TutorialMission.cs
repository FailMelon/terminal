﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using GameServer.Game.Definitions;
using GameServer.Game.IO;
using Services.BasicEmail;
using GameAPI.Missions;
using GameServer.Game.Services.SSH;
using GameServer.Game.Services.SSH.Scripts;
using GameAPI.IO;

namespace GameServer.Game.Missions
{
    public class TutorialMission : MissionBase
    {
        private class TutorialData
        {
            public ObjectId computerId { get; set; }
            public bool SSHOpened { get; set; }
        }

        private TutorialData tutorialData;
        private AIUser tutorialAI;

        public override void OnStart()
        {
            using (tutorialAI = new AIUser($"{User.DatabaseName}_tutorialAI1"))
            {
                var hardDef = Definition.FindDefinition<HardDriveDefinition>("hd_test");

                var bootDrive = tutorialAI.LoadBootDrive(hardDef, true);
                tutorialAI.LocalComputer.Services.Start<SSHService>(false);
                tutorialAI.LocalComputer.Services.Start<BasicEmailService>(true);

                bootDrive.AddFile("ezpz.wal", true).SetContent(new WalletContent(1250));

                bootDrive.AddDirectory("bin", true).AddFile("serviceopener.script", true).SetContent(new ScriptContent().SetType<InsaneServiceOpenerScript>());
            }

            var file = User.LocalComputer.GetBootDriver<HardDrive>().GetRootDirectory().FindOrAddFile("notes.txt", true);

            using (var txt = System.IO.File.OpenText("Resources/TutorialMissionPart1.txt"))
            {
                file.SetContent(new TextContent(string.Format(txt.ReadToEnd(), tutorialAI.LocalComputer.Address)));
            }

            tutorialData = new TutorialData
            {
                computerId = (tutorialAI.LocalComputer as Computer).Id
            };
        }

        public override bool OnUserCompletedTask(MissionTask task)
        {
            if (task.TargetComputer == tutorialAI.LocalComputer)
            {
                if (task.ServiceType == typeof(SSHService))
                {
                    if (task.TaskType == MissionTaskType.Connect)
                    {
                        tutorialData.SSHOpened = true;
                        return false;
                    }

                    if (task.TaskType == MissionTaskType.Disconnect && tutorialData.SSHOpened)
                    {
                        if (tutorialData.SSHOpened)
                            return true;
                    }
                }
            }

            return false;
        }

        public override void OnRestore(BsonDocument content)
        {
            tutorialData = BsonSerializer.Deserialize<TutorialData>(content);

            tutorialAI = new AIUser($"{User.DatabaseName}_tutorialAI1");

            tutorialAI.LocalComputer.Services.Start<SSHService>(tutorialData.SSHOpened);
            tutorialAI.LocalComputer.Services.Start<BasicEmailService>(true);
        }

        public override BsonDocument OnStore() => tutorialData.ToBsonDocument();

        public override void Shutdown()
        {
            tutorialAI?.Shutdown();

            if (IsComplete)
            {
                User.LocalComputer.GetBootDriver<HardDrive>().GetRootDirectory().RemoveFile("notes.txt");

                if (tutorialAI != null)
                    tutorialAI.LocalComputer.GetBootDriver().Remove();
            }
        }

        public override void OnComplete()
        {
            EmailPacket packet;
            packet.Author = "Anonymous@Unknown";
            packet.To = $"admin";
            packet.Subject = "Tutorial Mission Complete";
            packet.Message = "Mission complete!";

            ComputerManager.Instance.SendAPICallToService<BasicEmailService>(null, User.LocalComputer.Address, packet);
        }
    }
}
