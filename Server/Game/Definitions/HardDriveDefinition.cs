﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game.Definitions
{
    [BsonSerializer(typeof(DefinitionSerializer))]
    public class HardDriveDefinition : Definition
    {
        /// <summary>
        /// Code name
        /// </summary>
        public override string Name { get; protected set; }

        public virtual int Space { get; protected set; }

        public HardDriveDefinition(string name, int space)
        {
            Name = name;
            Space = space;

            Register(this);
        }
        
        public static HardDrive FactoryCreate(string definition, bool isWritable)
        {
            return new HardDrive(isWritable, FindDefinition<HardDriveDefinition>(definition));
        }

        public HardDrive FactoryCreate(bool isWritable)
        {
            return new HardDrive(isWritable, this);
        }
    }
}
