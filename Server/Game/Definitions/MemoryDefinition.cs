﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game.Definitions
{
    [BsonSerializer(typeof(DefinitionSerializer))]
    public class MemoryDefinition : Definition
    {
        /// <summary>
        /// Code name
        /// </summary>
        public override string Name { get; protected set; }

        public virtual int Memory { get; protected set; }

        public MemoryDefinition(string name, int memory)
        {
            Name = name;
            Memory = memory;

            Register(this);
        }
    }
}
