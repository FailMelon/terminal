﻿using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game.Definitions
{
    [BsonSerializer(typeof(DefinitionSerializer))]
    public class CPUDefinition : Definition
    {
        public override string Name { get; protected set; }

        public virtual string Brand { get; protected set; }

        public virtual int Cores { get; protected set; }

        public virtual int Frequency { get; protected set; }

        public CPUDefinition(string name, string brand, int frequency, int cores)
        {
            Name = name;
            Brand = brand;
            Frequency = frequency;
            Cores = cores;

            Register(this);
        }


    }
}
