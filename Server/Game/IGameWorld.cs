﻿namespace GameServer.Game
{
    public interface IGameWorld
    {
        void Shutdown();
        void Start();
    }
}