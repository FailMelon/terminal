﻿using GameAPI;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;

namespace GameServer.Game
{


    public class Computer : MarshalByRefObject, IEqualityComparer<Computer>, IComputer, ILocalComputer
    {
        public ObjectId Id { get; private set; }

        public IPAddress Address { get; private set; }

        public IUser LocalUser { get; private set; }

        public Hardware Hardware { get; private set; }

        public IServiceManager Services { get; private set; }

        public bool HasAddress { get => Address != null; }

        public bool HasBootDrive { get => bootDrive != null; }

        private IDrive bootDrive;

        public Computer(Hardware hardware)
            : this(ObjectId.GenerateNewId(), hardware) { }

        public Computer(ObjectId id, Hardware hardware)
        {
            Id = id;
            Hardware = hardware;
        }


        public void AssignUser(IUser user)
        {
            LocalUser = user;
        }

        public void Startup()
        {
            Services = new ServiceManager(this);
        }

        public void Connect(IUser user, int port)
        {
            if (user.TargetComputer == this)
            {
                if (!ComputerManager.Instance.AttemptConnect(user, Address, port))
                    ComputerManager.Instance.ConnectWithByPass(user, user.LocalComputer.Address, 22);
            }
        }

        public void Disconnect(IUser user)
        {
            if (user.TargetComputer == this)
            {
                ComputerManager.Instance.ConnectWithByPass(user, user.LocalComputer.Address, 22);
            }
        }

        public void Shutdown()
        {
            Services?.Shutdown();
        }

        public void HandleInput(IUser user, IUserInput input)
        {
            Services.HandleUserInput(user, input);
        }


        public void SetAddress(IPAddress address)
        {
            Address = address;
        }

        public void SetBootDrive(IDrive drive)
        {
            bootDrive = drive;
            bootDrive.Mount("/");
        }

        public IDrive GetBootDriver()
        {
            return bootDrive;
        }

        public T GetBootDriver<T>() where T : IDrive
        {
            return (T)bootDrive;
        }

        public bool Equals(Computer x, Computer y)
        {
            if (x.Address == y.Address)
                return true;

            return false;
        }

        public int GetHashCode(Computer obj)
        {
            return obj.Address.GetHashCode();
        }

        public bool SendAPICallToService<T>(IService sender, IPAddress targetIP, IApiPacket packet) where T : IService
        {
            return ComputerManager.Instance.SendAPICallToService<T>(sender, targetIP, packet);
        }
    }
}
