﻿using GameAPI;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game
{
    public class ComputerManager : MarshalByRefObject
    {
        public static ComputerManager Instance { get; private set; }

        public static void Initialise()
        {
            Instance = new ComputerManager();
        }

        public static void Initialise(ComputerManager instance)
        {
            Instance = instance;
        }

        private Dictionary<IPAddress, IComputer> currentActiveComputers;

        public Random Random { get; private set; }

        public ComputerManager()
        {
            currentActiveComputers = new Dictionary<IPAddress, IComputer>();
            Random = new Random();
        }

        public IPAddress GenerateAddress()
        {
            byte[] data = new byte[4];

            Random.NextBytes(data);
            IPAddress address = new IPAddress(data);

            while (!Database.IsIPAddressFree(address))
            {
                Random.NextBytes(data);
                address = new IPAddress(data);
            }

            return address;
        }

        public bool ConnectWithByPass(IUser user, IPAddress targetIP, int port = 22)
        {
            if (currentActiveComputers.TryGetValue(targetIP, out IComputer target))
            {
                Internal_Disconnect(user.TargetComputer, user);
                if (!Internal_Connect(target, user, true, port))
                {
                    Internal_Connect(user.LocalComputer, user, true, port);
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool Internal_Connect(IComputer computer, IUser user, bool isLoopBack, int port = 22)
        {
            if (computer != null && computer.Services.Connect(port, user, isLoopBack))
            {
                user.SetTargetComputer(computer);
                return true;
            }

            return false;
        }

        private void Internal_Disconnect(IComputer computer, IUser user)
        {
            if (computer != null && computer.Services.Disconnect(user))
            {
                user.SetTargetComputer(null);
            }
        }

        public bool AttemptConnect(IUser user, IPAddress targetIP, int port = 22)
        {
            if (IPAddress.IsLoopback(targetIP))
            {
                if (user.LocalComputer.Services.CanConnect(port, user, true))
                {
                    Internal_Disconnect(user.TargetComputer, user);
                    Internal_Connect(user.LocalComputer, user, true, port);

                    return true;
                }
            }

            if (currentActiveComputers.TryGetValue(targetIP, out IComputer target))
            {
                if (target.Services.CanConnect(port, user, false))
                {
                    Internal_Disconnect(user.TargetComputer, user);
                    Internal_Connect(target, user, false, port);

                    return true;
                }
            }

            return false;
        }

        public bool SendAPICallToService<T>(IService sender, IPAddress targetIP, IApiPacket packet) where T : IService
        {
            if (IPAddress.IsLoopback(targetIP))
                targetIP = sender.Manager.LocalComputer.Address;

            if (currentActiveComputers.TryGetValue(targetIP, out IComputer target))
            {
                var service = target.Services.FindService<T>();
                service?.OnReceiveAPICall(sender, packet);
                return true;
            }

            return false;
        }

        public bool TryProbeComputer(IPAddress targetIP, out IList<IService> services)
        {
            if (currentActiveComputers.TryGetValue(targetIP, out IComputer computer))
            {
                services = computer.Services.GetServices();
                return true;
            }

            services = new List<IService>();
            return false;
        }

        public bool TryOpenService(IPEndPoint endPoint)
        {
            if (currentActiveComputers.TryGetValue(endPoint.Address, out IComputer computer))
            {
                if (computer.Services.Open(endPoint.Port))
                {
                    return true;
                }

            }
            return false;
        }

        public void Disconnect(IUser user)
        {
            Internal_Disconnect(user.TargetComputer, user);
            Internal_Connect(user.LocalComputer, user, true, 22);
        }

        public void Shutdown(IUser user)
        {
            Internal_Disconnect(user.TargetComputer, user);
        }

        public void RegisterComputer(IComputer computer)
        {
            currentActiveComputers.Add(computer.Address, computer);
            (computer as Computer).Startup();
        }


        public void UnRegisterComputer(IComputer computer)
        {
            currentActiveComputers.Remove(computer.Address);
        }
    }
}
