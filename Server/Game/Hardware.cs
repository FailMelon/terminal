﻿using GameServer.Game.Definitions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameServer.Game
{
    public class Hardware
    {
        public CPUDefinition CPU { get; private set; }

        public MemoryDefinition RAM { get; private set; }

        [BsonConstructor("CPU", "RAM")]
        public Hardware(CPUDefinition cpu, MemoryDefinition ram)
        {
            CPU = cpu;
            RAM = ram;
        }
    }
}
