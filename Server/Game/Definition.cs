﻿using GameServer.Game.Definitions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Game
{
    public class DefinitionSerializer : SerializerBase<Definition>
    {
        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Definition value)
        {
            context.Writer.WriteStartDocument();
            context.Writer.WriteName("Name");
            context.Writer.WriteString(value.Name);
            context.Writer.WriteEndDocument();
        }

        public override Definition Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            context.Reader.ReadStartDocument();
            string name = context.Reader.ReadString();
            context.Reader.ReadEndDocument();

            return Definition.FindDefinition(name);
        }
    }

    public abstract class Definition : IEqualityComparer<Definition>
    {
        private static Dictionary<string, Definition> definitions;

        static Definition()
        {
            definitions = new Dictionary<string, Definition>();
        }

        public static Definition FindDefinition(string name)
        {
            definitions.TryGetValue(name, out Definition def);
            return def;
        }

        public static T FindDefinition<T>(string name) where T : Definition
        {
            definitions.TryGetValue(name, out Definition def);
            return (T)def;
        }

        public static void Register(Definition def)
        {
            definitions.Add(def.Name, def);
        }

        public abstract string Name { get; protected set; }

        public bool Equals(Definition x, Definition y)
        {
            return x?.Name == y?.Name;
        }

        public int GetHashCode(Definition obj)
        {
            return base.GetHashCode() ^ obj.Name.GetHashCode();
        }
    }

    public abstract class Definition<FactoryType> : Definition
    {
        private static Dictionary<string, Definition<FactoryType>> definitions;

        static Definition()
        {
            definitions = new Dictionary<string, Definition<FactoryType>>();
        }

        public static FactoryType FactoryCreate(string name)
        {
            var t = FindDefinition(name);
            return t != null ? t.FactoryCreate() : default;
        }

        public new static Definition<FactoryType> FindDefinition(string name)
        {
            definitions.TryGetValue(name, out Definition<FactoryType> def);
            return def;
        }

        public static void Register(Definition<FactoryType> def)
        {
            definitions.Add(def.Name, def);
        }

        protected virtual FactoryType FactoryCreate()
        {
            return default;
        }
    }
}
