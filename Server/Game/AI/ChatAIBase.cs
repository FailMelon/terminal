﻿using GameAPI;
using MongoDB.Bson;
using GameServer.ChatBot;
using GameServer.ChatBot.Rules;
using GameServer.Game.IO;
using Services.SSH;
using GameServer.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GameServer.Game.AI
{
    public abstract class ChatAIBase : AIUser, IAIChatBehaviour, IChatSession
    {
        private struct ChatMessage
        {
            public string Username;
            public string Message;
        }

        public event Action<IChatSession, string> OnMessageReceived;
        public event Action<IChatSession, string> OnMessageSent;

        public bool IsInteractive { get; private set; }
        public SessionStorage SessionStorage { get; private set; }

        private ManualResetEventResponse<string> readResponse = new ManualResetEventResponse<string>();

        private Bot chatBot;

        private Queue<ChatMessage> messages;

        public ChatAIBase(string databaseName)
            : base(databaseName)
        {
            Username = "chatAi";

            SessionStorage = new SessionStorage();
            IsInteractive = true;

            chatBot = new Bot();

            messages = new Queue<ChatMessage>();

            Task.Run(() => chatBot.TalkWith(this));
        }

        public void AddRule(IBotRule rule)
        {
            chatBot.AddRule(rule);
        }

        public void AddGenericRule(string name, int weight, Regex messagePattern, Func<Match, IChatSession, string> process)
        {
            chatBot.AddRule(new GenericBotRule(name, weight, messagePattern, process));
        }

        public virtual void OnUserConnect(IUser user)
        {
            SessionStorage.Values[user.Username] = new Dictionary<string, object>();
        }

        public void OnIncomingChat(IUser user, string message)
        {
            if (readResponse.IsWaiting)
            {
                SessionStorage.Values["currentUser"] = user.Username;
                readResponse.Set(message);
            }
            else
            {
                messages.Enqueue(new ChatMessage() { Username = user.Username, Message = message });
            }
        }

        public virtual void OnUserDisconnect(IUser user)
        {
            SessionStorage.Values.Remove(user.Username);
        }

        public string ReadMessage()
        {
            string response = null;
            if (messages.Count > 0)
            {
                var chatMessage = messages.Dequeue();
                SessionStorage.Values["currentUser"] = chatMessage.Username;
                response = chatMessage.Message;
            }
            else
                response = readResponse.Wait();

            Thread.Sleep(response.Length * 333);

            if (response != null)
                OnMessageReceived?.Invoke(this, response);

            return response;
        }

        public void SendMessage(string message)
        {
            SendLineInput(message);
            OnMessageSent?.Invoke(this, message);
        }

        public string AskQuestion(string message)
        {
            SendMessage(message);
            return ReadMessage();
        }

        public void SetResponseHistorySize(int Size)
        {
            throw new NotImplementedException();
        }

        protected LinkedList<BotResponse> _ResponseHistory = new LinkedList<BotResponse>();
        public void AddResponseToHistory(BotResponse Response)
        {
            _ResponseHistory.AddFirst(Response);
        }

        public Stack<BotResponse> GetResponseHistory()
        {
            return new Stack<BotResponse>(_ResponseHistory.Reverse());
        }
    }
}
