﻿using MongoDB.Bson;
using GameServer.Game.IO;
using Services.SSH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameServer.Game.Definitions;
using GameServer.Game.Services.SSH;

namespace GameServer.Game.AI
{
    public class ChatServerAI : AIUser
    {
        public ChatServerAI(string databaseName)
            : base(databaseName)
        {
            LocalComputer.Services.Start<SSHService>(false);
            LocalComputer.Services.Start<IRCService>(true);


            Dns.Get().RegisterDnsEntry("chat.hack", new System.Net.IPEndPoint(LocalComputer.Address, 6697));

            Startup();
        }
    }
}
