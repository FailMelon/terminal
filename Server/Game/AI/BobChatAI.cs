﻿using GameServer.ChatBot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.Game.AI
{
    public class BobChatAI : ChatAIBase
    {
        private Random rng;
        public BobChatAI(string databaseName)
            : base(databaseName)
        {
            rng = new Random();

            Username = "bob";

            SessionStorage.Values["name"] = Username;

            AddGenericRule(
                name: "greet",
                weight: 2,
                messagePattern: new Regex("(hi|hello|hey|sup|ey)", RegexOptions.IgnoreCase),
                process: (Match match, IChatSession session) =>
                {
                    string answer = "hi";

                    answer += " " + SessionStorage.Values["currentUser"];

                    if (rng.Next(0, 10) == 0)
                        answer += "!";

                    return answer;
                }
            );


            /*
            AddGenericRule(
                name: "getbotname",
                weight: 10,
                messagePattern: new Regex("(who are you|(what is|say) your name)", RegexOptions.IgnoreCase),
                process: (Match match, IChatSession session) =>
                {
                    if (!session.SessionStorage.Values.ContainsKey("name"))
                        return "I do not have a name";

                    if (match.Value.ToLower() == "who are you")
                        return "i am " + session.SessionStorage.Values["name"];

                    return "My name is " + session.SessionStorage.Values["name"];
                }
            );*/

            AddGenericRule(
                name: "default",
                weight: 1,
                messagePattern: new Regex(".*", RegexOptions.IgnoreCase),
                process: (Match match, IChatSession session) => null
            );

            Startup();
        }
    }
}
