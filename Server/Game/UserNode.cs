﻿using GameAPI;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GameServer.Game
{
    public class UserNode : INode
    {
        public static Dictionary<string, Assembly> Assemblies = new Dictionary<string, Assembly>();

        public static ObjectId TestComputer = new ObjectId("580481536bc3149cb4ca68ea");

        public static Random random = new Random();
        public IComputer LocalComputer { get; }

        private AppDomain domain;

        public UserNode(IComputer computer)
        {
            LocalComputer = computer;

            domain = AppDomain.CreateDomain("blahblahfixlater");
            domain.Load("System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
            domain.Load(typeof(IComputer).Assembly.GetName());
        }

        public void RunCode(string path, string code, Action<Scripting.ProgramCompiler.Message?, Assembly> finished, params string[] args)
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                var messages = new List<Scripting.ProgramCompiler.Message>();

                var task = Scripting.ProgramManager.Instance.CompileEntryPoint(AppDomain.CurrentDomain, "blahblahfixlater", code, path, messages);
                task.Wait();

                if (task.IsCompleted && !task.IsFaulted)
                {
                    Assemblies.Add(task.Result.FullName, task.Result);
                    finished?.Invoke(messages.Where((m) => m.Severity == Scripting.TErrorSeverity.Error).FirstOrDefault(), task.Result);
                }
            });
        }
    }
}
