﻿using GameServer.ChatBot.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.ChatBot
{
    internal class DescComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return Comparer<T>.Default.Compare(y, x);
        }
    }

    public class Bot
    {
        public event Action<IChatSession> OnConverationStarted;

        public event Action<IChatSession> OnConverationEnded;

        public event Action<IChatSession, string> OnMessageReceived;

        public event Action<IChatSession, string> OnMessageSent;

        public Func<string, bool> ExitCondition;

        protected Stack<string> commandHistory;
        protected SortedList<int, List<IBotRule>> botRules;

        public Bot()
        {
            commandHistory = new Stack<string>();
            botRules = new SortedList<int, List<IBotRule>>(new DescComparer<int>());

            ExitCondition = IsGoodBye;
        }

        public void AddRule(IBotRule rule)
        {
            if (rule.Process == null)
                throw new ArgumentException($"Rule '{rule.Name}' needs a process", "Rules");
            
           if (rule.MessagePattern == null)
                throw new ArgumentException($"Rule '{rule.Name}' needs a message pattern", "Rules");

            if (!botRules.ContainsKey(rule.Weight))
                botRules[rule.Weight] = new List<IBotRule>();

            botRules[rule.Weight].Add(rule);
        }

        protected string FindAnswer(IChatSession session, string messageIn)
        {
            foreach (List<IBotRule> rules in botRules.Values)
            {
                foreach (IBotRule rule in rules)
                {
                    Match match = rule.MessagePattern.Match(messageIn);
                    if (match.Success)
                    {
                        string msg = rule.Process(match, session);
                        if (msg != null)
                        {
                            session.AddResponseToHistory(new BotResponse(rule.Name, messageIn, msg));
                            return msg;
                        }
                    }
                }
            }

            return null;
        }

        protected void SendResponse(IChatSession session, string messageOut)
        {
            session.SendMessage(messageOut);
            OnMessageSent?.Invoke(session, messageOut);
        }

        public void TalkWith(IChatSession session)
        {
            if (session == null) return;

            OnConverationStarted?.Invoke(session);

            string messageIn = "";

            for (messageIn = session.ReadMessage(); !ExitCondition(messageIn); messageIn = session.ReadMessage())
            {
                OnMessageReceived?.Invoke(session, messageIn);

                string answer = FindAnswer(session, messageIn);

                if (answer != null)
                    SendResponse(session, answer);

                if (!session.IsInteractive)
                    break;
            }

            OnConverationEnded?.Invoke(session);
        }


        public bool IsGoodBye(string message)
        {
            switch (message.ToLower())
            {
                case "quit": return true;
                case "exit": return true;
                case "goodbye": return true;
                case "good bye": return true;
                case "bye": return true;
            }
            return false;
        }
    }
}
