﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GameServer.ChatBot
{
    public interface IBotRule : IEqualityComparer<IBotRule>
    {
        string Name { get; }
        int Weight { get; }
        Regex MessagePattern { get; }
        Func<Match, IChatSession, string> Process { get; }
    }
}
