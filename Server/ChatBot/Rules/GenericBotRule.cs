﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.ChatBot.Rules
{
    public class GenericBotRule : IBotRule
    {
        public string Name { get; private set; }

        public int Weight { get; private set; }

        public Regex MessagePattern { get; private set; }

        public Func<Match, IChatSession, string> Process { get; private set; }

        protected GenericBotRule(string name, int weight)
        {
            Name = name;
            Weight = weight;
        }

        protected GenericBotRule(string name, int weight, Regex messagePattern)
            : this(name, weight)
        {
            MessagePattern = messagePattern;
        }

        public GenericBotRule(string name, int weight, Regex messagePattern, Func<Match, IChatSession, string> process)
            : this(name, weight, messagePattern)
        {
            Process = process;
        }

        public bool Equals(IBotRule x, IBotRule y)
        {
            if (x.Name == y.Name)
                return true;

            return false;
        }

        public int GetHashCode(IBotRule obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
