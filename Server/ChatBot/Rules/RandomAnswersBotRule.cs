﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.ChatBot.Rules
{
    public class RandomAnswersBotRule : IBotRule
    {
        protected Random rnd;

        protected string[] Messages;

        public string Name { get; private set; }

        public int Weight { get; private set; }

        public Regex MessagePattern { get; private set; }

        public Func<Match, IChatSession, string> Process { get; private set; }

        protected RandomAnswersBotRule(string name, int weight)
        {
            rnd = new Random();
            Name = name ?? throw new ArgumentException("Name is null.", "Name");
            Weight = weight;
        }

        protected RandomAnswersBotRule(string name, int weight, Regex messagePattern)
            : this(name, weight)
        {
            MessagePattern = messagePattern ?? throw new ArgumentException("MessagePattern is null.", "MessagePattern");
        }

        public RandomAnswersBotRule(string name, int weight, Regex MessagePattern, string[] messages)
            : this(name, weight, MessagePattern)
        {
            Messages = messages;
            Process = SendRandomMessage;
        }

        public string SendRandomMessage(Match match, IChatSession session)
        {
            return Messages[rnd.Next(Messages.Length)];
        }

        public bool Equals(IBotRule x, IBotRule y)
        {
            if (x.Name == y.Name)
                return true;

            return false;
        }

        public int GetHashCode(IBotRule obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
