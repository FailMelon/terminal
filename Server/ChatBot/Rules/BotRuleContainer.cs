﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace GameServer.ChatBot.Rules
{
    public class BotRuleContainer : IBotRule
    {
        public string Name { get; protected set; }
        public int Weight { get; protected set; }
        public Regex MessagePattern { get; protected set; }
        public Func<Match, IChatSession, string> Process { get; protected set; }
        public SortedList<int, List<IBotRule>> NestedBotRules { get; protected set; }

        protected BotRuleContainer(string name, int weight)
        {
            NestedBotRules = new SortedList<int, List<IBotRule>>(new DescComparer<int>());
            Name = name ?? throw new ArgumentException("Name is null.", "Name");
            Weight = weight;
        }

        protected BotRuleContainer(string name, int weight, Regex messagePattern)
            : this(name, weight)
        {
            MessagePattern = messagePattern ?? throw new ArgumentException("MessagePattern is null.", "MessagePattern");
        }

        protected BotRuleContainer(string name, int weight, Regex messagePattern, Func<Match, IChatSession, string> process)
            : this(name, weight, messagePattern)
        {
            Process = process ?? throw new ArgumentException("Process is null.", "Process");
        }

        public bool Equals(IBotRule x, IBotRule y)
        {
            if (x.Name == y.Name)
                return true;

            return false;
        }

        public int GetHashCode(IBotRule obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
