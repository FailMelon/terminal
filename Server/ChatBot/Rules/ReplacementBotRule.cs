﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameServer.ChatBot.Rules
{
    public class ReplacementBotRule : IBotRule
    {
        protected Random rnd = new Random();
        protected string[] Replacements;

        protected Regex regex = new Regex("\\$(r|s)\\$([a-z0-9]+)\\$", RegexOptions.IgnoreCase);

        protected Dictionary<string, string> Setters;

        public string Name { get; private set; }
        public int Weight { get; private set; }
        public Regex MessagePattern { get; private set; }
        public Func<Match, IChatSession, string> Process { get; private set; }

        protected ReplacementBotRule(string name, int weight)
        {
            Name = name ?? throw new ArgumentException("Name is null.", "Name");
            Weight = weight;
        }

        protected ReplacementBotRule(string name, int weight, Regex messagePattern)
            : this(name, weight)
        {
            MessagePattern = messagePattern ?? throw new ArgumentException("MessagePattern is null.", "MessagePattern");
        }

        protected ReplacementBotRule(string name, int weight, Regex messagePattern, Func<Match, IChatSession, string> process)
            : this(name, weight, messagePattern)
        {
            Process = process ?? throw new ArgumentException("Process is null.", "Process");
        }

        public ReplacementBotRule(string name, int weight, Regex messagePattern, string replacement, Dictionary<string, string> setters)
            : this(name, weight, messagePattern, replacement)
        {
            Setters = setters;
        }

        public ReplacementBotRule(string name, int weight, Regex messagePattern, string[] replacements, Dictionary<string, string> setters)
            : this(name, weight, messagePattern, replacements)
        {
            Setters = setters;
        }

        public ReplacementBotRule(string name, int Weight, Regex messagePattern, string replacement)
            : this(name, Weight, messagePattern)
        {
            Setters = new Dictionary<string, string>();
            Replacements = new string[] { replacement };
            Process = ReplaceMessage;
        }

        public ReplacementBotRule(string name, int Weight, Regex messagePattern, string[] replacements)
            : this(name, Weight, messagePattern)
        {
            Setters = new Dictionary<string, string>();
            Replacements = replacements;
            Process = ReplaceMessage;
        }

        public string ReplaceMessage(Match match, IChatSession session)
        {
            foreach (string key in Setters.Keys)
            {
                session.SessionStorage.Values[key] = regex.Replace(
                    Setters[key],
                    (Match m) =>
                    {
                        switch (m.Groups[1].Value.ToLower())
                        {
                            case "s":
                                if (session.SessionStorage.Values.ContainsKey(m.Groups[2].Value))
                                {
                                    return (string)session.SessionStorage.Values[m.Groups[2].Value];
                                }
                                break;
                            case "r":
                                return match.Groups[m.Groups[2].Value].Value;
                        }
                        return "";
                    }
                );

            }

            if (Replacements.Length == 0)
                 return "";

            string msg;
            if (Replacements.Length > 1)
                msg = Replacements[rnd.Next(Replacements.Length)];
            else
                msg = Replacements[0];

            return regex.Replace(
                msg,
                (Match m) =>
                {
                    switch (m.Groups[1].Value.ToLower())
                    {
                        case "s":
                            if (session.SessionStorage.Values.ContainsKey(m.Groups[2].Value))
                            {
                                return (string)session.SessionStorage.Values[m.Groups[2].Value];
                            }
                            break;
                        case "r":
                            return match.Groups[m.Groups[2].Value].Value;
                    }
                    return "";
                }
            );
        }
        public bool Equals(IBotRule x, IBotRule y)
        {
            if (x.Name == y.Name)
                return true;

            return false;
        }

        public int GetHashCode(IBotRule obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}
