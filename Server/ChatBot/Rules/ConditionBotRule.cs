﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace GameServer.ChatBot.Rules
{
    public class ConditionBotRule : BotRuleContainer
    {
        public enum Operator
        {
            Equal,
            EqualIgnoreCase,
            NotEqual,
            NotEqualIgnoreCase,
            ContainsKey,
            ContainsValue,
            ContainsValueIgnoreCase,
        }

        protected IEnumerable<Tuple<string, Operator, object>> Conditions;

        public ConditionBotRule(string Name, int Weight, IEnumerable<Tuple<string, Operator, object>> conditions, IEnumerable<IBotRule> Rules)
            : base(Name, Weight)
        {
            MessagePattern = new Regex("^.*$");
            Conditions = conditions;

            foreach (IBotRule rule in Rules)
            {
                if (rule.Process == null)
                    throw new ArgumentException("Process is null.", "Rules");

                if (rule.MessagePattern == null)
                    throw new ArgumentException("MessagePattern is null.", "Rules");

                if (!NestedBotRules.ContainsKey(rule.Weight))
                    NestedBotRules[rule.Weight] = new List<IBotRule>();

                NestedBotRules[rule.Weight].Add(rule);
            }

            Process = ProcessSubrules;
        }

        public string ProcessSubrules(Match match, IChatSession session)
        {
            foreach (Tuple<string, Operator, object> condition in Conditions)
            {
                if (!session.SessionStorage.Values.ContainsKey(condition.Item1))
                {
                    return null;
                }
                switch (condition.Item2)
                {
                    case Operator.Equal:
                        if (session.SessionStorage.Values[condition.Item1] != condition.Item3)
                        {
                            return null;
                        }
                        break;
                    case Operator.NotEqual:
                        if (session.SessionStorage.Values[condition.Item1] == condition.Item3)
                        {
                            return null;
                        }
                        break;
                    case Operator.EqualIgnoreCase:
                        if (session.SessionStorage.Values[condition.Item1] != condition.Item3)
                        {
                            return null;
                        }
                        break;
                    case Operator.NotEqualIgnoreCase:
                        if (session.SessionStorage.Values[condition.Item1] == condition.Item3)
                        {
                            return null;
                        }
                        break;
                    case Operator.ContainsKey:
                        if (!session.SessionStorage.Values.ContainsKey(condition.Item1))
                        {
                            return null;
                        }
                        break;
                    case Operator.ContainsValue:
                        if (!((string)session.SessionStorage.Values[condition.Item1]).Contains((string)condition.Item3))
                        {
                            return null;
                        }
                        break;
                    case Operator.ContainsValueIgnoreCase:
                        if (!((string)session.SessionStorage.Values[condition.Item1]).Contains((string)condition.Item3))
                        {
                            return null;
                        }
                        break;
                }
            }

            foreach (List<IBotRule> rules in NestedBotRules.Values)
            {
                foreach (IBotRule rule in rules)
                {
                    Match submatch = rule.MessagePattern.Match(match.Value);
                    if (submatch.Success)
                    {

                        string msg = rule.Process(submatch, session);
                        if (msg != null)
                        {
                            return msg;
                        }
                    }
                }
            }
            // no hit found
            return null;
        }
    }
}
