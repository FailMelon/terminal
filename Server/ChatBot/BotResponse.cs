﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.ChatBot
{
    public class BotResponse
    {
        public const int MaxAnswerSize = 4096;

        public string RuleName { get; private set; }
        public string Question { get; private set; }
        public string Answer { get; private set; }

        public BotResponse(string ruleName, string question, string answer)
        {
            RuleName = ruleName;
            Question = question;
            Answer = (answer.Length <= MaxAnswerSize ? answer : answer.Substring(0, MaxAnswerSize - 3));
        }
    }
}
