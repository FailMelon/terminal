﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.ChatBot
{
    public interface IChatSession
    {
        event Action<IChatSession, string> OnMessageReceived;

        event Action<IChatSession, string> OnMessageSent;

        bool IsInteractive { get; }

        SessionStorage SessionStorage { get;  }

        string ReadMessage();
        void SendMessage(string message);
        string AskQuestion(string message);
        void SetResponseHistorySize(int Size);
        void AddResponseToHistory(BotResponse Response);
        Stack<BotResponse> GetResponseHistory();
    }
}
