﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.ChatBot
{
    public class SessionStorage
    {
        public Dictionary<string, object> Values;

        public SessionStorage()
        {
            Values = new Dictionary<string, object>();
        }
    }
}
