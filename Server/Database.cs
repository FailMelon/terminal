﻿using GameAPI;
using GameAPI.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System.Security;
using GameServer.Configs;
using GameServer.Game;
using GameServer.Game.IO;
using GameServer.Game.Missions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameServer.Game.Definitions;
using System.Net;
using GameAPI.Missions;

namespace GameServer
{

    public class Database
    {
        protected static IMongoClient client;
        protected static IMongoDatabase DB;

        private static UpdateOptions upsertOpt = new UpdateOptions { IsUpsert = true };

        private static IMongoCollection<BsonDocument> usersCollection;
        private static IMongoCollection<BsonDocument> computerCollection;
        private static IMongoCollection<BsonDocument> reservedIpCollection;
        private static IMongoCollection<BsonDocument> fileSystemCollection;
        private static IMongoCollection<BsonDocument> missionCollection;

        public static bool Initialise()
        {
            BsonClassMap.RegisterClassMap<WalletContent>();
            BsonClassMap.RegisterClassMap<DatabaseContent>();
            BsonClassMap.RegisterClassMap<TextContent>();
            BsonClassMap.RegisterClassMap<EmailContent>();
            BsonClassMap.RegisterClassMap<ScriptContent>();
            BsonClassMap.RegisterClassMap<HardDrive>();

            var databaseConfig = Config<DatabaseConfig>.Instance;

            var settings = new MongoClientSettings
            {
                Servers = new[] { new MongoServerAddress(databaseConfig.Address, databaseConfig.Port) },
                Credential = MongoCredential.CreateCredential(databaseConfig.AuthenticationDatabase,
                    databaseConfig.AdminAccount.Username,
                    databaseConfig.AdminAccount.Password)
            };

            client = new MongoClient(settings);

            DB = client.GetDatabase("terminal-dev");

            usersCollection = DB.GetCollection<BsonDocument>("users");
            computerCollection = DB.GetCollection<BsonDocument>("computers");
            reservedIpCollection = DB.GetCollection<BsonDocument>("reserved_ips");
            fileSystemCollection = DB.GetCollection<BsonDocument>("file_systems");
            missionCollection = DB.GetCollection<BsonDocument>("missions");

            Log.Info($"Checking connection to the Database at address {databaseConfig.Address} on port {databaseConfig.Port}...");
            var canConnect = DB.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(10000);

            if (canConnect)
            {
                Log.Info("Connection to database established");
            }
            else
            {
                Log.Error("Failed to connect to the database");
                return false;
            }

            return true;
        }

        public static void Shutdown()
        {

        }

        public static bool UserLogin(string username, string sessionID)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("username", username);

            var projection = Builders<BsonDocument>.Projection.Include("sessionid");

            var bson = usersCollection.Find(filter).Project(projection).SingleOrDefault();

            if (bson != null && bson.TryGetValue("sessionid", out BsonValue sessionid) && sessionid.IsString)
                return sessionid.AsString == sessionID;

            return false;
        }

        #region File Manipulation

        public static void CreateFile(IFileSystem ifileSystem, IFile file)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var path = file.AbsolutePath.GetPath().Replace(".", ",");

                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var projection = Builders<BsonDocument>.Projection.Include("paths");

                var bson = fileSystemCollection.Find(filter).Project(projection).SingleOrDefault();

                List<string> paths = new List<string>();

                if (bson != null && bson.TryGetValue("paths", out BsonValue pathsBson) && pathsBson.IsBsonArray)
                {
                    var bsonPaths = pathsBson.AsBsonArray;

                    foreach (var bsonPath in bsonPaths)
                        paths.Add(bsonPath.AsString);
                }

                if (!paths.Contains(path))
                {
                    var updatePaths = Builders<BsonDocument>.Update.AddToSet("paths", path);
                    var updateFiles = Builders<BsonDocument>.Update.Set("files." + path, BsonNull.Value);

                    var update = Builders<BsonDocument>.Update.Combine(updatePaths, updateFiles);

                    fileSystemCollection.UpdateMany(filter, update, upsertOpt);
                }
            }
        }

        public static void MoveFile(IFileSystem ifileSystem, IFile file, Path newfilePath)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var updateFile = Builders<BsonDocument>.Update.Rename("files." + file.AbsolutePath.GetPath().Replace(".", ","), "files." + newfilePath.GetPath().Replace(".", ","));
                var pullFromPath = Builders<BsonDocument>.Update.Pull("paths", file.AbsolutePath.GetPath().Replace(".", ","));

                var update1 = Builders<BsonDocument>.Update.Combine(updateFile, pullFromPath);

                fileSystemCollection.UpdateMany(filter, update1);

                var update2 = Builders<BsonDocument>.Update.AddToSet("paths", newfilePath.GetPath().Replace(".", ","));

                fileSystemCollection.UpdateOne(filter, update2);
            }
        }

        public static void SetFileContent(IFile file, IFileContent content)
        {
            SetFileContent(file.FileSystem, file.AbsolutePath, content);
        }

        public static void SetFileContent(IFileSystem ifileSystem, Path path, IFileContent content)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var update = Builders<BsonDocument>.Update.Set("files." + path.GetPath().Replace(".", ","), content.ToBsonDocument());

                fileSystemCollection.UpdateOne(filter, update, upsertOpt);
            }
        }

        public static IFileContent GetFileContent(File file)
        {
            return GetFileContent(file.FileSystem, file.AbsolutePath);
        }

        public static IFileContent GetFileContent(IFileSystem ifileSystem, Path path)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var projection = Builders<BsonDocument>.Projection.Include("files." + path.GetPath().Replace(".", ","));

                var bson = fileSystemCollection.Find(filter).Project(projection).SingleOrDefault();

                if (bson != null && bson.TryGetValue("files", out BsonValue pathsContent))
                {
                    if (pathsContent != null && pathsContent.IsBsonDocument && pathsContent.AsBsonDocument.TryGetValue(path.GetPath().Replace(".", ","), out BsonValue content))
                    {
                        if (content.IsBsonNull)
                            return null;
                        else
                            return BsonSerializer.Deserialize<IFileContent>(content.AsBsonDocument);
                    }
                }
            }

            return null;
        }

        public static IFileContent GetFileContent(IFile file, params string[] strProjections)
        {
            return GetFileContent(file.FileSystem, file.AbsolutePath, strProjections);
        }

        public static IFileContent GetFileContent(IFileSystem ifileSystem, Path path, params string[] strProjections)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var projections = new List<ProjectionDefinition<BsonDocument>>
                {
                    Builders<BsonDocument>.Projection.Include("files." + path.GetPath().Replace(".", ",") + "." + "_t")
                };

                foreach (string strProjection in strProjections)
                    projections.Add(Builders<BsonDocument>.Projection.Include("files." + path.GetPath().Replace(".", ",") + "." + strProjection));

                var projection = Builders<BsonDocument>.Projection.Combine(projections);

                var bson = fileSystemCollection.Find(filter).Project(projection).SingleOrDefault();

                if (bson != null && bson.TryGetValue("files", out BsonValue pathsContent))
                {
                    if (pathsContent != null && pathsContent.IsBsonDocument && pathsContent.AsBsonDocument.TryGetValue(path.GetPath().Replace(".", ","), out BsonValue content))
                    {
                        if (content.IsBsonNull)
                            return null;
                        else
                            return BsonSerializer.Deserialize<IFileContent>(content.AsBsonDocument);
                    }
                }

                return null;
            }

            return GetFileContent(ifileSystem, path);
        }

        public static void RemoveFile(IFile file)
        {
            if (file.FileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var updateFile = Builders<BsonDocument>.Update.Unset("files." + file.AbsolutePath.GetPath().Replace(".", ","));
                var pullFromPath = Builders<BsonDocument>.Update.Pull("paths", file.AbsolutePath.GetPath().Replace(".", ","));

                var update = Builders<BsonDocument>.Update.Combine(updateFile, pullFromPath);

                fileSystemCollection.UpdateMany(filter, update);
            }
        }

        #endregion

        #region Directory Manipulation

        public static void CreateDirectory(IFileSystem ifileSystem, IDirectory directory)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var path = directory.GetAbsolutePath().GetPath();

                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

                var projection = Builders<BsonDocument>.Projection.Include("paths");
            
                var bson = fileSystemCollection.Find(filter).Project(projection).SingleOrDefault();

                List<string> paths = new List<string>();

                if (bson != null && bson.TryGetValue("paths", out BsonValue pathsBson) && pathsBson.IsBsonArray)
                {
                    var bsonPaths = pathsBson.AsBsonArray;

                    foreach (var bsonPath in bsonPaths)
                        paths.Add(bsonPath.AsString);
                }
                
                if (!paths.Contains(path))
                {
                    var update = Builders<BsonDocument>.Update.AddToSet("paths", path);

                    fileSystemCollection.UpdateOne(filter, update, upsertOpt);
                }
            }
        }

        #endregion

        #region FileSystem Manipulation

        public static FileSystem LoadFileSystem(ObjectId fileSystemID)
        {
            var fileSystem = new FileSystem(fileSystemID);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);

            var projection = Builders<BsonDocument>.Projection.Include("paths");

            var bson = fileSystemCollection.Find(filter).Project(projection).SingleOrDefault();

            var paths = new List<string>();

            if (bson != null && bson.TryGetValue("paths", out BsonValue pathsBson) && pathsBson.IsBsonArray)
            {
                var bsonPaths = pathsBson.AsBsonArray;

                foreach (var bsonPath in bsonPaths)
                {
                    paths.Add(bsonPath.AsString.Replace(",", "."));
                }
            }

            fileSystem.LoadFileSystem(paths);

            return fileSystem;
        }

        public static void RemoveFileSystem(IFileSystem ifileSystem)
        {
            if (ifileSystem is FileSystem fileSystem)
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", fileSystem.ID);
                fileSystemCollection.DeleteOne(filter);
            }
        }

        #endregion

        public static void CreateUserComputer(IUser user, Computer computer)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", computer.Id);

            if (!computerCollection.Find(filter).Any())
            {
                var compBson = new BsonDocument
                {
                    { "_id", computer.Id },
                    { "owner", user.DatabaseName },
                    { "hardware", computer.Hardware.ToBsonDocument() },
                    { "address", computer.Address.ToString() },
                    { "bootDrive", computer.GetBootDriver().ToBsonDocument() }
                };

                computerCollection.InsertOne(compBson);
            }
        }

        public static void SaveComputerHardware(ObjectId id, Hardware hardware)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", id);

            var update = Builders<BsonDocument>.Update.Set("hardware", hardware);

            computerCollection.UpdateOne(filter, update, upsertOpt);
        }

        public static void AssignComputerIP(ObjectId id, IPAddress ip)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", id);

            var update = Builders<BsonDocument>.Update.Set("address", ip.ToString());

            computerCollection.UpdateOne(filter, update, upsertOpt);
        }

        public static Computer LoadComputerInstant(IUser user)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("owner", user.DatabaseName);

            var bson = computerCollection.Find(filter).SingleOrDefault();

            if (bson != null && bson.TryGetValue("_id", out BsonValue bsonObjectId) && bson.TryGetValue("hardware", out BsonValue bsonHardware) && bsonHardware.IsBsonDocument)
            {
                var hardware = BsonSerializer.Deserialize<Hardware>(bsonHardware.AsBsonDocument);

                var computer = new Computer(bsonObjectId.AsObjectId, hardware);

                if (bson.TryGetValue("address", out BsonValue bsonAddress) && bsonAddress.IsString)
                    computer.SetAddress(IPAddress.Parse(bsonAddress.AsString));

                computer.AssignUser(user);

                if (bson.TryGetValue("bootDrive", out BsonValue bsonBootDrive) && bsonBootDrive.IsBsonDocument)
                    computer.SetBootDrive(BsonSerializer.Deserialize<IDrive>(bsonBootDrive.AsBsonDocument));

                ComputerManager.Instance.RegisterComputer(computer);

                return computer;
            }

            return null;

        }

        public static void LoadComputer(IUser user, Action<List<Computer>> action)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("owner", user.DatabaseName);

            computerCollection.Find(filter).SingleOrDefaultAsync().ContinueWith((task) =>
            {
                var computers = new List<Computer>();
                var bson = task.Result;

                if (bson != null && bson.TryGetValue("_id", out BsonValue bsonObjectId) && bson.TryGetValue("hardware", out BsonValue bsonHardware) && bsonHardware.IsBsonDocument)
                {
                    var hardware = BsonSerializer.Deserialize<Hardware>(bsonHardware.AsBsonDocument);

                    var computer = new Computer(bsonObjectId.AsObjectId, hardware);

                    if (bson.TryGetValue("address", out BsonValue bsonAddress) && bsonAddress.IsString)
                        computer.SetAddress(IPAddress.Parse(bsonAddress.AsString));

                    computer.AssignUser(user);

                    if (bson.TryGetValue("bootDrive", out BsonValue bsonBootDrive) && bsonBootDrive.IsBsonDocument)
                        computer.SetBootDrive(BsonSerializer.Deserialize<IDrive>(bsonBootDrive.AsBsonDocument));

                    ComputerManager.Instance.RegisterComputer(computer);
                    computers.Add(computer);
                }

                action?.Invoke(computers);
            });

        }

        public static void ReserveIPAddress(IPAddress address)
        {
            var bson = new BsonDocument
            {
                { "address", address.ToString() }
            };

            reservedIpCollection.InsertOne(bson);
        }

        public static void UnReserveIPAddress(IPAddress address)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("address", address.ToString());
            reservedIpCollection.DeleteOne(filter);
        }

        public static bool IsIPAddressFree(IPAddress address)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("address", address.ToString());

            if (computerCollection.Find(filter).Any()) return false;

            if (reservedIpCollection.Find(filter).Any()) return false;

            return true;
        }

        public static void SaveMission(IUser user, IMission mission)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("owner", user.DatabaseName) & Builders<BsonDocument>.Filter.Eq("type", mission.GetType().ToString()) & Builders<BsonDocument>.Filter.Eq("_id", mission.ID);

            var update = Builders<BsonDocument>.Update.Set("data", mission.OnStore());

            missionCollection.UpdateOne(filter, update, upsertOpt);
        }

        public static IMission LoadMission(IUser user)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("owner", user.DatabaseName);

            var bson = missionCollection.Find(filter).SingleOrDefault();

            if (bson != null && bson.TryGetValue("type", out BsonValue bsonTypeValue) && bson.TryGetValue("data", out BsonValue dataValue))
            {
                var mission = (IMission)Activator.CreateInstance(Type.GetType(bsonTypeValue.AsString));
                mission.ID = bson.GetValue("_id").AsObjectId;
                mission.User = user;
                mission.OnRestore(dataValue.AsBsonDocument);

                return mission;
            }

            return null;
        }

        public static void RemoveMission(IMission mission)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", mission.ID);
            missionCollection.DeleteOne(filter);
        }
    }
}
