﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public class WindowsHardwareInfo
    {
        private static WindowsHardwareInfo instance;

        private int m_maxClockSpeed;
        private int m_coreCount;
        private DateTime m_startupTime = DateTime.Now;

        public static int MaxClockSpeed => instance.m_maxClockSpeed;

        public static int CoreCount => instance.m_coreCount;

        public static DateTime StartupTime => instance.m_startupTime;

        public static float CurrentProcessMhzUsage { get; set; }

        public WindowsHardwareInfo(int maxClockSpeed, int coreCount)
        {
            m_maxClockSpeed = maxClockSpeed;
            m_coreCount = coreCount;

            instance = this;
        }

    }
}
