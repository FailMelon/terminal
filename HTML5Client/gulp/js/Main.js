"use strict";
var path = require("path");
var gulp = require("gulp");
var ts = require("gulp-typescript");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var cleanCSS = require("gulp-clean-css");
var autoprefixer = require("gulp-autoprefixer");
var htmlmin = require("gulp-htmlmin");
var uglify = require("gulp-uglify");
var Terminal;
(function (Terminal) {
    var GulpFile;
    (function (GulpFile) {
        var uglifyOpts = {};
        var thirdPartyLibs = [
            {
                js: "node_modules/pixi.js/dist/pixi.js",
                map: "node_modules/pixi.js/dist/pixi.js.map"
            },
            {
                js: "node_modules/howler/dist/howler.js"
            },
            {
                js: "node_modules/pixi-filters/dist/pixi-filters.js",
                map: "node_modules/pixi-filters/dist/pixi-filters.js.map"
            }
        ];
        /** ASSETS */
        gulp.task("copy-video-assets", function () {
            return gulp.src("src/assets/videos/*.mp4")
                .pipe(gulp.dest("dist/videos"));
        });
        gulp.task("copy-audio-assets", function () {
            return gulp.src("src/assets/audio/*.mp3")
                .pipe(gulp.dest("dist/audio"));
        });
        gulp.task("copy-audio-assets", function () {
            return gulp.src("src/assets/audio/*.mp3")
                .pipe(gulp.dest("dist/audio"));
        });
        gulp.task("copy-font-assets", function () {
            return gulp.src("src/assets/fonts/*.ttf")
                .pipe(gulp.dest("dist/fonts"));
        });
        gulp.task("build-assets", gulp.parallel("copy-video-assets", "copy-audio-assets", "copy-font-assets"));
        /** JAVASCRIPT */
        gulp.task("build-app", function () {
            var tsProject = ts.createProject("src/tsconfig.json");
            var tsResult = tsProject.src()
                .pipe(sourcemaps.init())
                .pipe(tsProject());
            return tsResult.js
                .pipe(uglify(uglifyOpts))
                .pipe(sourcemaps.write({
                sourceRoot: function (file) { return path.relative(path.dirname(file.path), file.base); }
            }))
                .pipe(gulp.dest("dist"));
        });
        gulp.task("build-lib", function () {
            return gulp.src(thirdPartyLibs.map(function (tpl) { return tpl.js; }))
                .pipe(sourcemaps.init())
                .pipe(uglify(uglifyOpts))
                .pipe(concat("lib.js"))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest("dist"));
        });
        /** STYLESHEETS */
        gulp.task("build-css", function () {
            return gulp.src("src/css/*.css")
                .pipe(sourcemaps.init())
                .pipe(cleanCSS({ compatibility: "ie8" }))
                .pipe(autoprefixer())
                .pipe(concat("app.min.css"))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest("dist"));
        });
        /** HTML */
        gulp.task("build-html", function () {
            return gulp.src("src/html/*.html")
                .pipe(sourcemaps.init())
                .pipe(htmlmin({ collapseWhitespace: true }))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest("dist"));
        });
        /**   */
        gulp.task("default", gulp.parallel("build-app", "build-lib", "build-css", "build-html", "build-assets"));
        gulp.task("fast", gulp.parallel("build-app", "build-css", "build-html"));
    })(GulpFile = Terminal.GulpFile || (Terminal.GulpFile = {}));
})(Terminal = exports.Terminal || (exports.Terminal = {}));
