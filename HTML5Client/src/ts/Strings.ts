namespace Terminal.Strings
{
    export const ui =
    {
        loginTitle: "Login",
        username: "Username: ",
        password: "Password: ",
        loginButton: "Login"
    };
}