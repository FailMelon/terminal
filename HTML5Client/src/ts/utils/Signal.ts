namespace Terminal.Util
{
    export interface ISignal<TArg> extends PromiseLike<TArg>
    {
        add(callback: ISignal.Callback<TArg>, context?: object): void;
        remove(callback: ISignal.Callback<TArg>, context?: object): void;

        dispatch(arg: TArg): void;
    }

    export namespace ISignal
    {
        export type Callback<TArg> = (arg: TArg) => void;
    }

    class Signal<TArg> implements ISignal<TArg>
    {
        private readonly _callbacks: { callback: ISignal.Callback<TArg>; context?: object; }[] = [];
        private readonly _callbacksAdd: { callback: ISignal.Callback<TArg>; context?: object; }[] = [];
        private readonly _callbacksRemove: { callback: ISignal.Callback<TArg>; context?: object; }[] = [];
        private _dispatching: number = 0;

        public add(callback: ISignal.Callback<TArg>, context?: object): void
        {
            if (this._dispatching > 0)
            {
                this._callbacksAdd.push({ callback, context });
                return;
            }
            this._callbacks.push({ callback, context });
        }
        public remove(callback: ISignal.Callback<TArg>, context?: object): void
        {
            if (this._dispatching > 0)
            {
                this._callbacksRemove.push({ callback, context });
                return;
            }
            for (let i = this._callbacks.length - 1; i >= 0; --i)
            {
                if (this._callbacks[i].callback === callback && this._callbacks[i].context === context)
                {
                    this._callbacks.splice(i, 1);
                    break;
                }
            }
        }
        public dispatch(arg: TArg): void
        {
            ++this._dispatching;
            for (let i = this._callbacks.length - 1; i >= 0; --i)
            {
                this._callbacks[i].callback.call(this._callbacks[i].context || this, arg);
            }
            --this._dispatching;
            if (this._dispatching === 0)
            {
                for (const cb of this._callbacksRemove)
                {
                    this.remove(cb.callback, cb.context);
                }
                this._callbacksRemove.length = 0;
                for (const cb of this._callbacksAdd)
                {
                    this.add(cb.callback, cb.context);
                }
                this._callbacksAdd.length = 0;
            }
        }

        public then<TResult1 = TArg, TResult2 = never>(onfulfilled?: (value: TArg) => TResult1 | PromiseLike<TResult1>, onrejected?: (reason: any) => TResult2 | PromiseLike<TResult2>): PromiseLike<TResult1 | TResult2>
        {
            return new Promise((resolve, reject) =>
            {
                const func = (arg: TArg) =>
                {
                    this.remove(func);
                    resolve(onfulfilled(arg));
                };
                this.add(func);
            })
            
        }
    }

    export function createSignal<TArg>(): ISignal<TArg>
    {
        return new Signal<TArg>();
    }
}