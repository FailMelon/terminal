namespace Terminal.Utils
{
    export interface IInputReceiver
    {
        handleGainFocus(): void;
        handleLoseFocus(): void;
        handleKeyPress(key: string): void;
    }

    export class InputManager
    {
        private _curFocus: IInputReceiver|null = null;

        public get focus() { return this._curFocus; }
        public set focus(value)
        {
            if (this._curFocus)
            {
                this._curFocus.handleLoseFocus();
                this._curFocus = null;
            }
            this._curFocus = value;
            if (this._curFocus)
            {
                this._curFocus.handleGainFocus();
            }
        }

        public constructor()
        {
            window.addEventListener("keydown", ev =>
            {
                if (this._curFocus)
                {
                    this._curFocus.handleKeyPress(ev.key);
                }
            });
            window.addEventListener("keyup", ev =>
            {
                
            });
        }
    }
}