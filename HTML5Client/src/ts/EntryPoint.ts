/// <reference path="Main.ts" />

namespace Terminal
{
    function isDocumentReady()
    {
        return document.readyState === "complete" || document.readyState === "interactive";
    }

    let didHitEntryPoint = false;
    export let p: Program | null = null;
    function entryPoint()
    {
        p = new Program();
        p.run();
    }

    if (isDocumentReady)
    {
        entryPoint();
        didHitEntryPoint = true;
    }
    else
    {
        document.addEventListener("readystatechange", ev =>
        {
            if (isDocumentReady() && !didHitEntryPoint)
            {
                entryPoint();
                didHitEntryPoint = true;
            }
        });
    }
}