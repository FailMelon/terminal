/// <reference path="Base.ts" />
/// <reference path="../Strings.ts" />
/// <reference path="../utils/InputManager.ts" />
/// <reference path="../components/TextBox.ts" />
/// <reference path="../components/Button.ts" />

/// <reference path="../definitions/pixi-filters.d.ts" />

namespace Terminal.Views
{
    const frameSettings =
    {
        w: 300, h: 150,
        borderSize: 2,
        borderColor: 0x20C20E,
        titleHeight: 25,
        labelWidth: 100,
        fieldHeight: 26
    };

    export class Login extends Base
    {
        private _logoVideoBaseTex: PIXI.VideoBaseTexture;
        private _logoVideo: PIXI.Sprite;
        private _logoVideoTimer: number;

        private _music: Howl;

        private _frame: PIXI.Graphics;
        private _titleText: PIXI.Text;

        private _usernameText: PIXI.Text;
        private _usernameField: Components.TextBox;

        private _passwordText: PIXI.Text;
        private _passwordField: Components.TextBox;

        private _loginButton: Components.Button;

        private _inputManager: Utils.InputManager;

        public onOpen(): void
        {
            this._logoVideoBaseTex = PIXI.VideoBaseTexture.fromUrl("videos/LogoCenter.mp4");

            this._logoVideoTimer = window.setInterval(() => this._logoVideoBaseTex.source.play(), 10000);

            this._logoVideo = new PIXI.Sprite(new PIXI.Texture(this._logoVideoBaseTex));
            this._logoVideo.anchor.set(0.5, 0.5);
            this._logoVideo.position.set(this._width * 0.5, this._height * 0.25);
            this._logoVideo.scale.set(0.5, 0.5);
            this.addChild(this._logoVideo);

            this._music = new Howl({
                src: "audio/music_ambient_01.mp3",
                loop: true
            });
            this._music.play();

            this._frame = new PIXI.Graphics();
            this._frame
                .beginFill(frameSettings.borderColor, 1.0)
                .drawRect(0, 0, frameSettings.w, frameSettings.titleHeight)
                .endFill()

                .lineStyle(frameSettings.borderSize, frameSettings.borderColor, 1.0)
                .drawRect(0, 0, frameSettings.w, frameSettings.h);
            this._frame.pivot.set(frameSettings.w * 0.5, frameSettings.h * 0.5);
            this._frame.position.set(this._width * 0.5, this._height * 0.5);
            this.addChild(this._frame);

            this._titleText = new PIXI.Text(Strings.ui.loginTitle,
            {
                fontFamily: "VCR OSD Mono",
                fontWeight: 'bold',
                fontSize: 18,
                fill: 0x000000
            });
            this._titleText.anchor.set(0.0, 0.5);
            this._titleText.position.set(5, frameSettings.titleHeight * 0.5);
            this._frame.addChild(this._titleText);

            this._inputManager = new Utils.InputManager();

            const usernameText = this._usernameText = new PIXI.Text(Strings.ui.username,
            {
                fontFamily: "VCR OSD Mono",
                fontStyle: "italic",
                fontSize: 18,
                fill: 0xFFFFFF
            });
            usernameText.anchor.set(0.0, 0.0);
            usernameText.position.set(10, 35);
            this._frame.addChild(usernameText);

            this._usernameField = new Components.TextBox({
                size: { w: frameSettings.w - frameSettings.labelWidth - 10, h: frameSettings.fieldHeight },
                isPassword: false
            }, this._inputManager);
            this._usernameField.position.set(frameSettings.labelWidth, 35);
            this._frame.addChild(this._usernameField);
            
            const passwordText = this._passwordText = new PIXI.Text(Strings.ui.password,
            {
                fontFamily: "VCR OSD Mono",
                fontStyle: "italic",
                fontSize: 18,
                fill: 0xFFFFFF
            });
            passwordText.anchor.set(0.0, 0.0);
            passwordText.position.set(10, 70);
            this._frame.addChild(passwordText);

            this._passwordField = new Components.TextBox({
                size: { w: frameSettings.w - frameSettings.labelWidth - 10, h: frameSettings.fieldHeight },
                isPassword: true
            }, this._inputManager);
            this._passwordField.position.set(frameSettings.labelWidth, 70);
            this._frame.addChild(this._passwordField);

            this._loginButton = new Components.Button({
                color: 0x00397c,
                hoverColor: 0x00479b,
                pressColor: 0x002a5d,
                disabledColor: 0x3636AA,
                text: Strings.ui.loginButton,
                textStyle:
                {
                    fontFamily: "Hack",
                    fontSize: 18,
                    fill: 0xFFFFFF
                },
                size: { w: 200, h: 25 }
            });
            this._loginButton.position.set(frameSettings.w * 0.5 - 200 * 0.5, frameSettings.h - 40);
            this._loginButton.onClicked.add(this.handleLoginButtonClicked, this);
            this._frame.addChild(this._loginButton);

            const rgbSplitFilter = new PIXI.filters.RGBSplitFilter(new PIXI.Point(0, 0), new PIXI.Point(1.5, 1.5), new PIXI.Point(0, 0));
            const bloomFilter = new PIXI.filters.AdvancedBloomFilter({
                threshold: 0.1,
                bloomScale: 2,
            });
            const crtFilter = new PIXI.filters.CRTFilter({
                vignetting: 0.1
            });

            this._frame.filters = [crtFilter];
        }

        private handleLoginButtonClicked(): void
        {
            
        }

        public onResize(newWidth: number, newHeight: number): void
        {
            super.onResize(newWidth, newHeight);
            this._logoVideo.position.set(newWidth * 0.5, newHeight * 0.25);
            this._frame.position.set(newWidth * 0.5, newHeight * 0.5);
        }

        public onClose(): void
        {
            this._music.stop();
            this.removeChildren();
            this._logoVideoBaseTex.destroy();
            window.clearInterval(this._logoVideoTimer);
        }
    }
}