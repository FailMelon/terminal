namespace Terminal.Views
{
    /**
     * Base view.
     * There can only be one view active at any given time.
     * A view encapsulates a number of components to be displayed to the screen.
     */
    export abstract class Base extends PIXI.Container
    {
        protected _width: number;
        protected _height: number;

        public constructor(width: number, height: number)
        {
            super();
            this._width = width;
            this._height = height;
        }

        /** Called when the view has been opened. */
        public abstract onOpen(): void;

        /** Called when the view has been closed. */
        public abstract onClose(): void;

        /** Called when the view has been resized. */
        public onResize(newWidth: number, newHeight: number): void
        {
            this._width = newWidth;
            this._height = newHeight;
        }
        
    }
}