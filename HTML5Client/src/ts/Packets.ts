namespace Terminal.Packets
{
    export interface Base<TName extends string, TData>
    {
        name: TName;
        data: TData;
    }

    export type Login = Base<"LoginPacket",
    {
        username: string;
        password: string;
    }>;

    export type EnterWorld = Base<"EnterWorldPacket", {}>;

    export type InputResponse = Base<"InputResponsePacket",
    {
        response: string;
    }>;

    export type Clear = Base<"ClearPacket", {}>;

    export type LineInput = Base<"LineInputPacket",
    {
        input: string;
    }>;

    export type KeyInput = Base<"KeyInputPacket",
    {
        key: number;
        control: boolean;
        shift: boolean;
        alt: boolean;
    }>;

    export type CaretPosition = Base<"CaretPositionPacket",
    {
        chPos: number;
        colPos: number;
    }>;

    export type WaitForInput = Base<"WaitForInputPacket",
    {
        keyInput: boolean;
        lineInput: boolean;
    }>;

    export type Packet = Login | EnterWorld | InputResponse | Clear | LineInput | KeyInput | CaretPosition | WaitForInput;

    export function is(obj: any): obj is Packet
    {
        return obj != null && typeof obj === "object" && (typeof obj.name === "string" || obj.name instanceof String);
    }
}