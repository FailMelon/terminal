/// <reference path="NetClient.ts" />

/// <reference path="views/Base.ts" />
/// <reference path="Views/Login.ts" />

namespace Terminal
{
    /**
     * Core program logic.
     */
    export class Program
    {
        private _canvas: HTMLCanvasElement;
        private _renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
        private _rootContainer: PIXI.Container;
        private _curView: Views.Base|null = null;
        private _ticker: PIXI.ticker.Ticker;
        private _client: NetClient;

        public constructor()
        {
            // Setup rendering
            this._canvas = document.getElementById("main-stage") as HTMLCanvasElement;
            this.updateCanvasSize();

            this._renderer = new PIXI.WebGLRenderer({
                width: this._canvas.width,
                height: this._canvas.height,
                view: this._canvas
            });

            this._rootContainer = new PIXI.Container();


            // Setup ticker
            this._ticker = new PIXI.ticker.Ticker();
            this._ticker.add(this.render.bind(this));

            window.addEventListener("resize", this.handleWindowResize.bind(this));
        }

        private switchToView(newView: Views.Base|null): void
        {
            if (this._curView)
            {
                this._rootContainer.removeChild(this._curView);
                this._curView.destroy();
                this._curView = null;
            }
            if (!newView) { return; }
            this._curView = newView;
            this._curView.onOpen();
            this._rootContainer.addChild(this._curView);
        }

        private render(): void
        {
            this._renderer.render(this._rootContainer);
        }

        private updateCanvasSize(): void
        {
            this._canvas.width = window.innerWidth;
            this._canvas.height = window.innerHeight;
        }

        private handleWindowResize(ev: UIEvent)
        {
            this.updateCanvasSize();
            this._renderer.resize(this._canvas.width, this._canvas.height);
            if (this._curView) { this._curView.onResize(this._canvas.width, this._canvas.height); }
        }

        public run(): void
        {
            // Start ticker
            this._ticker.start();

            // Open login view
            this.switchToView(new Views.Login(this._canvas.width, this._canvas.height));

            // Setup client
            this._client = new NetClient();

            // Login
            this.connectAndLogin();
            
        }

        private async connectAndLogin()
        {
            try
            {
                await this._client.connect("ws://localhost:22781/terminal");
            }
            catch
            {
                return false;
            }
            if (this._client.status !== NetClient.Status.Connected) { return; }

            this._client.sendPacket({ name: "LoginPacket", data: { username: "TestUser", password: "TestPass" } });
            const reply = await Promise.race([ this._client.onPacketReceived, this._client.onDisconnected ]);

            if (Packets.is(reply))
            {
                if (reply.name === "EnterWorldPacket")
                {
                    return true;
                }
            }
            else
            {
                console.warn(`${reply.message}`);
                return false;
            }

            
            
        }
    }
}