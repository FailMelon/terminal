/// <reference path="../utils/Signal.ts" />

namespace Terminal.Components
{
    export class Button extends PIXI.Container
    {
        private _bg: PIXI.Graphics;
        private _text: PIXI.Text;
        private _hovered: boolean;
        private _pressed: boolean;
        private _enabled: boolean;

        public readonly onClicked = Util.createSignal<void>();

        public get enabled() { return this._enabled; }
        public set enabled(value)
        {
            this._enabled = value;
            this.updateBackground();
        }

        public constructor(public readonly settings: Button.Settings)
        {
            super();

            this._enabled = true;

            this._bg = new PIXI.Graphics();
            this.updateBackground();
            this.addChild(this._bg);

            this._text = new PIXI.Text(settings.text, settings.textStyle);
            this._text.anchor.set(0.5, 0.5);
            this._text.position.set(settings.size.w * 0.5, settings.size.h * 0.5);
            this.addChild(this._text);

            this.interactive = true;
            this.cursor = "pointer";

            this.on("mouseover", ev => { this._hovered = true; this.updateBackground(); });
            this.on("mouseout", ev => { this._hovered = false; this.updateBackground(); });
            this.on("mousedown", ev => { this._pressed = true; this.updateBackground(); });
            this.on("mouseup", ev => { this._pressed = false; this.updateBackground(); this._enabled && this.onClicked.dispatch(undefined); });
            this.on("mouseupoutside", ev => { this._pressed = false; this.updateBackground(); });
        }

        private updateBackground()
        {
            this._bg
                .clear()
                .beginFill(!this._enabled ? this.settings.disabledColor : this._pressed ? this.settings.pressColor : this._hovered ? this.settings.hoverColor : this.settings.color, 1.0)
                .drawRect(0, 0, this.settings.size.w, this.settings.size.h)
                .endFill();
        }
    }

    export namespace Button
    {
        export interface Settings
        {
            size: { w: number; h: number; };
            color: number;
            hoverColor: number;
            pressColor: number;
            disabledColor: number;
            text: string;
            textStyle: PIXI.TextStyleOptions;
        }
    }
}