/// <reference path="../utils/InputManager.ts" />
/// <reference path="../utils/Signal.ts" />

namespace Terminal.Components
{
    export class TextBox extends PIXI.Container implements Utils.IInputReceiver
    {
        private _bg: PIXI.Graphics;
        private _text: PIXI.Text;
        private _focused: boolean;
        private _caretPos: number;
        private _string: string = "";

        public get text() { return this._string; }
        public set text(value)
        {
            this._string = value;
            this.updateText();
        }

        public constructor(public readonly settings: TextBox.Settings, public readonly inputManager: Utils.InputManager)
        {
            super();
            this.interactive = true;
            
            this.on("click", ev =>
            {
                inputManager.focus = this;
            });

            this._focused = false;

            this._bg = new PIXI.Graphics();
            this.updateBackground();
            this.addChild(this._bg);

            this._text = new PIXI.Text("",
            {
                fontFamily: "Hack",
                fontSize: 16,
                fill: 0xFFFFFF
            });
            this._text.anchor.set(0.0, 0.5);
            this._text.position.set(0, settings.size.h * 0.5);
            this.addChild(this._text);
        }

        private updateBackground()
        {
            this._bg
                .clear()
                .beginFill(this._focused ? 0x303030 : 0x181818, 1.0)
                .drawRect(0, 0, this.settings.size.w, this.settings.size.h)
                .endFill();
        }

        private updateText()
        {
            if (this.settings.isPassword)
            {
                this._text.text = ("*").repeat(this._string.length);
            }
            else
            {
                this._text.text = this._string;
            }
        }

        public handleGainFocus(): void
        {
            this._focused = true;
            this.updateBackground();
        }
        public handleLoseFocus(): void
        {
            this._focused = false;
            this.updateBackground();
        }
        public handleKeyPress(key: string): void
        {
            if (key.length === 1)
            {
                this.text += key;
            }
            else
            {

            }
        }
    }

    export namespace TextBox
    {
        export interface Settings
        {
            size: { w: number; h: number; };
            isPassword: boolean;
        }
    }
}