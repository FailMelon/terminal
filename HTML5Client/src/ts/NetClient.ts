/// <reference path="utils/Signal.ts" />
/// <reference path="Packets.ts" />

namespace Terminal
{
    export class NetClient
    {
        private _status = NetClient.Status.None;
        private _ws: WebSocket | null = null;

        /** Gets the current status of the client. */
        public get status() { return this._status; }

        /** Published when a packet has been received. */
        public readonly onPacketReceived = Util.createSignal<Packets.Packet>();

        /** Published when connection was closed. */
        public readonly onDisconnected = Util.createSignal<NetClient.DisconnectData>();

        private log(message: string): void
        {
            console.log(`NetClient: ${message}`);
        }

        /**
         * Attempts to connect to the specified url.
         * @param url 
         */
        public connect(url: string): PromiseLike<void>
        {
            if (this._status !== NetClient.Status.None) { throw new Error("Attempt to connect when status was not none"); }
            return new Promise((resolve, reject) =>
            {
                this.log("Connecting...");
                this._status = NetClient.Status.Connecting;
                const ws = this._ws = new WebSocket(url);
                const self2 = this;
                function onClose(ev: CloseEvent)
                {
                    self2.log("connect:onClose");
                    self2._status = NetClient.Status.None;
                    self2._ws = null;
                    ws.removeEventListener("close", onClose);
                    ws.removeEventListener("open", onOpen);
                    reject(ev.reason);
                }
                function onOpen(ev: Event)
                {
                    self2.log("connect:onOpen");
                    ws.removeEventListener("close", onClose);
                    ws.removeEventListener("open", onOpen);
                    self2.handleConnected();
                    resolve();
                }
                ws.addEventListener("close", onClose);
                ws.addEventListener("open", onOpen);
            });
        }

        private handleConnected(): void
        {
            this.log("handleConnected");
            this._status = NetClient.Status.Connected;
            this._ws.addEventListener("message", ev => this.handleMessage(ev.data));
            this._ws.addEventListener("close", ev => this.handleDisconnected(ev.code, ev.reason));
        }

        private handleMessage(data: any): void
        {
            this.log(`handleMessage (${data})`);
            this.onPacketReceived.dispatch(JSON.parse(data));
        }

        private handleDisconnected(code: number, reason: string): void
        {
            this.log("handleDisconnected");
            this._status = NetClient.Status.None;
            this._ws = null;
            this.onDisconnected.dispatch({
                message: reason,
                code
            });
        }

        /**
         * Sends a packet to the remote host.
         * @param packet 
         */
        public sendPacket(packet: Packets.Packet): void
        {
            if (this._status !== NetClient.Status.Connected) { throw new Error("Attempt to send packet when status was not connected"); }
            this._ws.send(JSON.stringify(packet));
        }

        /**
         * Disconnects from the remote host.
         */
        public disconnect(code?: number, reason?: string): void
        {
            if (this._status !== NetClient.Status.Connected) { throw new Error("Attempt to disconnect when status was not connected"); }
            this._ws.close(code, reason);
            this._ws = null;
        }
    }

    export namespace NetClient
    {
        export interface DisconnectData
        {
            message: string;
            code: number;
        }

        export enum Status
        {
            None,
            Connecting,
            Connected
        }
    }
}