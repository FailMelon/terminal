﻿using GameAPI.IO;

namespace GameAPI
{
    public interface IDrive
    {
        string MountPath { get; }
        bool IsMounted { get; }
        bool IsWritable { get; }
        IFileSystem FileSystem { get; }

        IDirectory GetRootDirectory();
        void Mount(Path path);
        void UnMount();
        void Remove();
    }
}