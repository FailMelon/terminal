﻿using GameAPI;
using GameAPI.IO;

namespace GameAPI.Services.SSH
{
    public interface ISSHService
    {
        IServiceManager Manager { get; set; }
        string Name { get; }
        ushort Port { get; }

        void ChangeDirectory(IUser user, IDirectory directory);
        IDirectory FindDirectory(IUser user, Path pathToNavTo);
        IDirectory FindDirectory(IUser user, string pathToNavTo);
        IDirectory GetCurrentDirectory(IUser user);
        int GetHashCode(IService service);
        void Log(string message);
    }
}