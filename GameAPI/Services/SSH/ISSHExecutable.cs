﻿using GameAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI.Services.SSH
{
    public interface ISSHExecutable
    {
        IUser User { get; set; }
        ISSHService Service { get; set; }
        IList<string> Arguments { get; set; }
        bool IsCompleted { get; }
        bool IsRunning { get; }
        bool AskForInput { get; }

        void HandleInput(IUserInput input);
    }
}
