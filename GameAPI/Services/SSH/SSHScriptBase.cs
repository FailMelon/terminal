﻿using GameAPI.IO;
using System;
using System.Collections.Generic;

namespace GameAPI.Services.SSH
{
    public abstract class SSHScriptBase : MarshalByRefObject, ISSHExecutable
    {
        public IUser User { get; set; }

        public ISSHService Service { get; set; }

        public IList<string> Arguments { get; set; }

        public bool IsCompleted { get; private set; }

        public bool IsRunning { get; private set; }

        public virtual bool AskForInput => true;

        public void HandleInput(IUserInput input)
        {
            if (!IsRunning)
            {
                IsRunning = true;

                if (OnStart())
                    Complete();
            }
            else if (!IsCompleted)
            {
                HandleInputUpdate(input);
            }
        }

        protected abstract bool OnStart();

        protected abstract void HandleInputUpdate(IUserInput input);

        protected abstract void OnEnd();

        protected void Write(string message)
        {
            User.Console.Write(message);
        }

        protected void WriteLine(string message)
        {
            User.Console.WriteLine(message);
        }

        protected IDirectory FindDirectory(string pathToNavTo)
        {
            return Service.FindDirectory(User, pathToNavTo);
        }

        protected void Complete()
        {
            IsCompleted = true;

            if (IsRunning)
            {
                OnEnd();
                IsRunning = false;
            }
        }
    }
}
