﻿using GameAPI.IO;
using MongoDB.Bson;
using System;

namespace GameAPI.IO
{
    public class WalletContent : MarshalByRefObject, IFileContent
    {
        public decimal Coins { get; set; }

        public WalletContent(decimal coins)
        {
            Coins = coins;
        }

        public bool TryTakeCoins(decimal coins)
        {
            if (Coins >= coins)
            {
                Coins -= coins;
                return true;
            }

            return false;
        }

        public void AddCoins(decimal coins) => Coins += coins;

        public void TakeCoins(decimal coins) => Coins -= coins;

        public bool HasCoins(decimal coins) => Coins >= coins;
    }
}
