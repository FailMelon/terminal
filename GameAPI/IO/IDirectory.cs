﻿using GameAPI.IO;
using System;
using System.Collections.Generic;

namespace GameAPI.IO
{
    public interface IDirectory
    {
        IReadOnlyList<IDirectory> Directories { get; }
        IReadOnlyList<IFile> Files { get; }
        IFileSystem FileSystem { get; }
        string Name { get; }
        IDirectory PreviousDirectory { get; }

        IDirectory AddDirectory(string directoryName, bool saveToDB);
        IFile AddFile(Func<string> func, bool saveToDB);
        IFile AddFile(string fileName, bool saveToDB);
        IDirectory FindDirectory(string directoryName);
        IFile FindFile(string fileName);
        IFile FindOrAddFile(string fileName, bool saveToDB);
        IDirectory FindOrCreateDirectory(string directoryName, bool saveToDB);
        Path GetAbsolutePath();
        void MoveFile(IFile oldFile, string newFileName, IDirectory newDirectory);
        void RemoveDirectory(string directory);
        void RemoveFile(IFile file);
        void RemoveFile(string fileName);
    }
}