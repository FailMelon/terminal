﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameAPI.IO
{
    public class DatabaseContent : MarshalByRefObject, IFileContent
    {
        [BsonRequired]
        private Dictionary<string, object> collection;

        [BsonIgnore]
        private IFile referenceFile;

        public DatabaseContent()
        {
            collection = new Dictionary<string, object>();
        }

        public void SetReference(IFile referenceFile)
        {
            this.referenceFile = referenceFile;
        }

        public bool Add<T>(string name, T value)
        {
            if (!collection.ContainsKey(name))
            {
                collection.Add(name, value);
                return true;
            }

            return false;
        }

        public bool Remove(string name) => collection.Remove(name);

        public bool Contains(string name) => collection.ContainsKey(name);

        public T GetValue<T>(string name)
        {
            collection.TryGetValue(name, out object rawValue);
            return (T)rawValue;
        }

        public bool TryGetValue<T>(string name, out T value)
        {
            if (collection.TryGetValue(name, out object rawValue))
            {
                value = (T)rawValue;
                return true;
            }

            value = default;
            return false;
        }

        public IEnumerable<string> Keys()
        {
            return collection.Keys;
        }

        public IEnumerable<T> Values<T>()
        {
            return collection.Values.Cast<T>();
        }

        public void Save()
        {
            referenceFile.SetContent(this);
        }
    }
}
