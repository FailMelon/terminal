﻿using System.Collections.Generic;

namespace GameAPI.IO
{
    public interface IFileSystem
    {
        IDirectory Root { get; }
        IDrive Drive { get; }

        void AssignDrive(IDrive drive);
        IDirectory AddDirectory(Path path, bool saveToDB, bool recursive = false);
        IFile AddFile(Path path, bool saveToDB);
        IDirectory FindDirectory(Path path);
        IFile FindFile(string path);
        IEnumerable<IDirectory> ListDirectories(Path path);
        IEnumerable<IFile> ListFiles(Path path, bool recursive = false);
        void LoadFileSystem(List<string> paths);
        void RemoveDirectory(Path path);
    }
}