﻿using System;

namespace GameAPI.IO
{
    public class EmailContent : MarshalByRefObject, IFileContent, IConvertible
    {
        public string Author { get; set; }

        public string Subject { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Content { get; set; }

        public EmailContent() { }

        public TypeCode GetTypeCode() => TypeCode.Object;

        public bool ToBoolean(IFormatProvider provider) => throw new InvalidCastException();
        public char ToChar(IFormatProvider provider) => throw new InvalidCastException();
        public sbyte ToSByte(IFormatProvider provider) => throw new InvalidCastException();
        public byte ToByte(IFormatProvider provider) => throw new InvalidCastException();
        public short ToInt16(IFormatProvider provider) => throw new InvalidCastException();
        public ushort ToUInt16(IFormatProvider provider) => throw new InvalidCastException();
        public int ToInt32(IFormatProvider provider) => throw new InvalidCastException();
        public uint ToUInt32(IFormatProvider provider) => throw new InvalidCastException();
        public long ToInt64(IFormatProvider provider) => throw new InvalidCastException();
        public ulong ToUInt64(IFormatProvider provider) => throw new InvalidCastException();
        public float ToSingle(IFormatProvider provider) => throw new InvalidCastException();
        public double ToDouble(IFormatProvider provider) => throw new InvalidCastException();
        public decimal ToDecimal(IFormatProvider provider) => throw new InvalidCastException();
        public DateTime ToDateTime(IFormatProvider provider) => throw new InvalidCastException();
        public string ToString(IFormatProvider provider) => throw new InvalidCastException();

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            if (conversionType ==  typeof(TextContent))
                return new TextContent($"{Author}\n{Subject}\n{TimeStamp}\n{Content}");

            return null;
        }
    }
}
