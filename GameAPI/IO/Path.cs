﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameAPI.IO
{
    public class Path : MarshalByRefObject
    {
        private IList<string> segments;

        private string endingName;
        private string extensionName;

        public bool IsSingle { get; private set; }
        public bool IsRoot { get; private set; }

        private Path(bool isRoot, IList<string> strSegments)
        {
            segments = new List<string>();

            IsRoot = isRoot;

            for (int i = 0; i < strSegments.Count; i++)
            {
                var strSegment = strSegments[i].Replace("/", "").Replace("\\", "");

                if (i == strSegments.Count - 1)
                {
                    if (HasExtension(strSegment))
                    {
                        endingName = System.IO.Path.GetFileNameWithoutExtension(strSegment);
                        extensionName = System.IO.Path.GetExtension(strSegment);
                    }
                    else
                        endingName = strSegment;
                }
                else
                {
                    (segments as List<string>).Add(strSegment);
                }
            }

            IsSingle = segments.Count == 0;
        }

        public string GetEndName()
        {
            return endingName;
        }

        public string GetEndName(string extension)
        {
            return endingName + "." + extension;
        }

        public string GetEndNameWithExtensionOrDefault(string defaultExtension)
        {
            return string.IsNullOrEmpty(extensionName) ? endingName + "." + defaultExtension : endingName + extensionName;
        }

        public string GetEndNameWithExtension()
        {
            return endingName + (extensionName ?? string.Empty);
        }

        public string GetExtension()
        {
            return extensionName;
        }

        public string GetPath()
        {
            string pathStr = GetPathWithoutEnd();

            pathStr += pathStr == string.Empty || pathStr.EndsWith("/") ? endingName : "/" + endingName;

            if (!string.IsNullOrEmpty(extensionName))
                pathStr += extensionName;

            return pathStr;
        }

        public string GetPathWithoutEnd()
        {
            string pathStr = string.Empty;

            if (IsRoot)
                pathStr += "/";

            pathStr += string.Join("/", segments);

            return pathStr;
        }

        public IList<string> GetAllSegments()
        {
            return new List<String>(segments)
            {
                endingName
            };
        }

        public IList<string> GetSegments()
        {
            return segments;
        }

        #region Static Methods

        public static Path Parse(string strPath)
        {
            var strSegments = StringUtil.Segements(strPath);
            return new Path(strPath.StartsWith("/"), strSegments);
        }

        public static bool TryParse(string strPath, out Path path)
        {
            var strSegments = StringUtil.Segements(strPath);

            path = null;

            for (int i = 0; i < strSegments.Count; i++)
            {
                var strSegment = strSegments[i];

                if (strSegment.Contains(","))
                {
                    return false;
                }
                else if (HasExtension(strSegment) && HasInvalidFileNameChar(strSegment))
                {
                    return false;
                }
                else if (HasInvalidPathChar(strSegment))
                {
                    return false;
                }
            }

            path = new Path(strPath.StartsWith("/"), strSegments);
            return true;
        }

        public static string GetFileName(string strPath)
        {
            return System.IO.Path.GetFileName(strPath);
        }

        public static bool HasExtension(string strPath)
        {
            return System.IO.Path.HasExtension(strPath);
        }

        public static bool HasInvalidFileNameChar(string strFileName)
        {
            var invalidChars = System.IO.Path.GetInvalidFileNameChars();
            return strFileName.Any((c) => invalidChars.Contains(c));
        }

        public static bool HasInvalidPathChar(string strPath)
        {
            var invalidChars = System.IO.Path.GetInvalidPathChars();
            return strPath.Any((c) => invalidChars.Contains(c));
        }

        public static Path Combine(string strPathA, string strPathB)
        {
            if (strPathA.EndsWith("/"))
                return Parse(strPathA + strPathB);
            else
                return Parse(strPathA + "/" + strPathB);
        }

        public static implicit operator string(Path path)
        {
            return path.ToString();
        }

        public static implicit operator Path(string path)
        {
            return Parse(path);
        }

        #endregion

        public override string ToString()
        {
            return GetPath();
        }
    }
}
