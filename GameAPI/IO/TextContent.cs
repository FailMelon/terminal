﻿using GameAPI.IO;
using System;

namespace GameAPI.IO
{
    public class TextContent : MarshalByRefObject, IFileContent, IDisposable
    {
        public string Data { get; set; }

        public TextContent(string data)
        {
            Data = data;
        }

        public void Dispose()
        {
            Data = null;
        }

    }
}
