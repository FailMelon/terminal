﻿namespace GameAPI.IO
{
    public interface IFile
    {
        Path AbsolutePath { get; }
        IFileSystem FileSystem { get; }
        bool IsDeleted { get; }
        string Name { get; }
        IDirectory Owner { get; }

        void SetContent(IFileContent content);
        IFileContent GetContent(params string[] projection);
        void Remove();
    }
}