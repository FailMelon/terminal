﻿using System;
using Tarul.Structures;

namespace GameAPI
{
    public class FuckMarshalColor : MarshalByRefObject
    {
        public byte R, G, B;
        public FuckMarshalColor(byte r, byte g, byte b )
        {
            R = r;
            G = g;
            B = b;
        }
    }

    public interface ITerminal
    {
        FuckMarshalColor BackgroundColor { get; set; }
        FuckMarshalColor ForegroundColor { get; set; }
        int BufferHeight { get; }
        int BufferWidth { get; }
        int WindowHeight { get; }
        int WindowWidth { get; }

        void SaveCursorPosition();
        void RestoreCursorPosition();
        void Clear();
        void ClearLine();
        void MoveBufferArea(int sourceX, int sourceY, int sourceWidth, int sourceHeight, int targetX, int targetY);
        void ReadKey();
        void ReadLine();
        void ResetColor();
        void SetCursorPosition(int chPos, int lnPos);
        void SetSlowMode(bool slowMode);
        void Write(string value);
        void WriteLine(string value);
    }
}