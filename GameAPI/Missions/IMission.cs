﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI.Missions
{
    public interface IMission
    {
        ObjectId ID { get; set; }
        IUser User { get; set; }
        bool IsComplete { get; }

        void OnStart();
        bool OnUserCompletedTask(MissionTask task);
        void OnRestore(BsonDocument content);
        BsonDocument OnStore();
        void Complete();
        void Shutdown();
    }
}
