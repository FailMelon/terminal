﻿using GameAPI.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI.Missions
{
    public class MissionTask : MarshalByRefObject
    {
        public IComputer TargetComputer { get; }
        public IFile TargetFile { get; }
        public MissionTaskType TaskType { get; }
        public Type ServiceType { get; }

        public MissionTask(IComputer targetComputer, IFile targetFile, MissionTaskType taskType, Type serviceType)
        {
            TargetComputer = targetComputer;
            TargetFile = targetFile;
            TaskType = taskType;
            ServiceType = serviceType;
        }
    }
}
