﻿namespace GameAPI.Missions
{
    public interface IMissionManager
    {
        void LoadMissions();
        void RegisterUserInteraction(MissionTask task);
        void ShutdownMissions();
        void StartMission<T>() where T : IMission, new();
    }
}