﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI.Missions
{
    public abstract class MissionBase : IMission
    {
        public ObjectId ID { get; set; }

        public IUser User { get; set; }

        public bool IsComplete { get; private set; }

        public abstract void OnStart();

        public abstract bool OnUserCompletedTask(MissionTask task);

        public abstract void OnRestore(BsonDocument content);

        public abstract BsonDocument OnStore();

        public abstract void Shutdown();

        public virtual void Complete()
        {
            IsComplete = true;
            OnComplete();
        }

        public abstract void OnComplete();
    }

}
