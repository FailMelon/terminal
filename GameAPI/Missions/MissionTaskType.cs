﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI.Missions
{
    public enum MissionTaskType
    {
        Unknown,
        Connect,
        Disconnect,
        DownloadedFile
    }
}
