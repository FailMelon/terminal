﻿using System;
using System.Linq;
using System.Reflection;
using Tarul.Collections;

namespace GameAPI.Packets
{
    [AttributeUsage(AttributeTargets.Struct)]
    public class Packet : Attribute
    {
    }

    public static class PacketSystem
    {
        private static Mapping<Type, byte> packetDict;
        private static Mapping<Type, string> webPacketDict;
        private static byte nextid;

        static PacketSystem()
        {
            packetDict = new Mapping<Type, byte>();
            webPacketDict = new Mapping<Type, string>();
            nextid = 0;
        }

        public static void Initialise()
        {
            Initialise(Assembly.GetCallingAssembly());
        }

        public static void Initialise(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes()
                .Where((t) => t.IsValueType && t.GetCustomAttributes(typeof(Packet), true).Count() > 0)
                .OrderBy((t) => t.Name))
            {
                packetDict.Add(type, nextid++);
                webPacketDict.Add(type, type.Name);
            }
        }

        public static byte GetPacketID(Type type)
        {
            return packetDict.GetValue(type);
        }

        public static Type GetPacketType(byte id)
        {
            if (packetDict.ContainsValue(id))
                return packetDict.GetKey(id);
            else
                return null;
        }
        
        public static Type GetPacketType(string name)
        {
            if (webPacketDict.ContainsValue(name))
                return webPacketDict.GetKey(name);
            else
                return null;
        }

        public static string GetPacketName(Type type)
        {
            return webPacketDict.GetValue(type);
        }
    }
}
