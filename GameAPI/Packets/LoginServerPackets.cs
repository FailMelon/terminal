﻿using Tarul;

namespace GameAPI.Packets
{
    [Packet]
    public struct LoginPacket : IBitSerialisable
    {
        public string username, password;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            username = rdr.ReadString();
            password = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(username);
            wtr.Write(password);
        }

        #endregion
    }

    [Packet]
    public struct RegisterPacket : IBitSerialisable
    {
        public string username, password;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            username = rdr.ReadString();
            password = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(username);
            wtr.Write(password);
        }

        #endregion
    }

    [Packet]
    public struct LoginSuccessPacket : IBitSerialisable
    {
        public string sessionID;
        public string gameServerAddress;
        public ushort gameServerport;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            sessionID = rdr.ReadString();
            gameServerAddress = rdr.ReadString();
            gameServerport = rdr.ReadBitsAsUInt16(16);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(sessionID);
            wtr.Write(gameServerAddress);
            wtr.WriteBits(gameServerport, 16);
        }

        #endregion
    }
}
