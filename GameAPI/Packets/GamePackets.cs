﻿using System.Collections.Generic;
using Tarul;
using Tarul.Structures;

namespace GameAPI.Packets
{
    
    [Packet]
    public struct EnterWorldPacket : IBitSerialisable
    {
        #region ISerialisable

        public void Deserialise(BitStreamReader rdr) { }

        public void Serialise(BitStreamWriter wtr) { }

        #endregion
    }

    [Packet]
    public struct EnteredAndInfoPacket : IBitSerialisable
    {
        public int terminalBufferWidth, terminalBufferHeight;
        public int terminalWindowWidth, terminalWindowHeight;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            terminalWindowWidth = rdr.ReadBitsAsInt32(32);
            terminalWindowHeight = rdr.ReadBitsAsInt32(32);
            terminalBufferWidth = rdr.ReadBitsAsInt32(32);
            terminalBufferHeight = rdr.ReadBitsAsInt32(32);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(terminalWindowWidth);
            wtr.Write(terminalWindowHeight);
            wtr.Write(terminalBufferWidth);
            wtr.Write(terminalBufferHeight);
        }

        #endregion
    }


    [Packet]
    public struct SlowModePacket : IBitSerialisable
    {
        public bool slowMode;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            slowMode = rdr.ReadBitAsBool();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(slowMode);
        }

        #endregion
    }

    [Packet]
    public struct ComputerPerformancePacket : IBitSerialisable
    {
        public long activeMemory;
        public int currentProcessingTime;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            activeMemory = rdr.ReadBitsAsInt64(64);
            currentProcessingTime = rdr.ReadBitsAsInt32(32);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(activeMemory);
            wtr.Write(currentProcessingTime);
        }

        #endregion
    }


    [Packet]
    public struct ComputerServicesPacket : IBitSerialisable
    {
        public List<string> services;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            int serviceCount = rdr.ReadBitsAsInt32(32);

            services = new List<string>();
            for (int i = 0; i < serviceCount; i++)
                services.Add(rdr.ReadString());
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(services.Count);

            for (int i = 0; i < services.Count; i++)
                wtr.Write(services[i]);
        }

        #endregion
    }


    [Packet]
    public struct ColorUpdatePacket : IBitSerialisable
    {
        public Color foreground, background;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            foreground = Color.CreateFromStream(rdr);
            background = Color.CreateFromStream(rdr);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(foreground);
            wtr.Write(background);
        }

        #endregion
    }


    [Packet]
    public struct WriteLinePacket : IBitSerialisable
    {
        public string data;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            data = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(data);
        }

        #endregion
    }


    [Packet]
    public struct WritePacket : IBitSerialisable
    {
        public string data;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            data = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(data);
        }

        #endregion
    }

    [Packet]
    public struct MoveBufferAreaPacket : IBitSerialisable
    {
        public int sourceX, sourceY, sourceWidth, sourceHeight, targetX, targetY;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            sourceX = rdr.ReadBitsAsInt32(32);
            sourceY = rdr.ReadBitsAsInt32(32);
            sourceWidth = rdr.ReadBitsAsInt32(32);
            sourceHeight = rdr.ReadBitsAsInt32(32);
            targetX = rdr.ReadBitsAsInt32(32);
            targetY = rdr.ReadBitsAsInt32(32);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.WriteBits(sourceX, 32);
            wtr.WriteBits(sourceY, 32);
            wtr.WriteBits(sourceWidth, 32);
            wtr.WriteBits(sourceHeight, 32);
            wtr.WriteBits(targetX, 32);
            wtr.WriteBits(targetY, 32);
        }

        #endregion
    }

    [Packet]
    public struct SaveCursorAndRestorePositionPacket : IBitSerialisable
    {
        public bool save;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            save = rdr.ReadBitAsBool();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(save);
        }

        #endregion
    }

    public enum TerminalClearType : byte
    {
        Line,
        Buffer
    }

    [Packet]
    public struct ClearPacket : IBitSerialisable
    {
        public TerminalClearType type;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            type = (TerminalClearType)rdr.ReadBitsAsByte(8);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.WriteBits((int)type, 8);
        }

        #endregion
    }

    [Packet]
    public struct LineInputPacket : IBitSerialisable
    {
        public string input;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            input = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(input);
        }

        #endregion
    }

    [Packet]
    public struct KeyInputPacket : IBitSerialisable
    {
        public Keyboard.KeyCode key;
        public bool control;
        public bool shift;
        public bool alt;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            key = (Keyboard.KeyCode)rdr.ReadBitsAsInt32(13);
            control = rdr.ReadBitAsBool();
            shift = rdr.ReadBitAsBool();
            alt = rdr.ReadBitAsBool();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.WriteBits((short)key, 13);
            wtr.Write(control);
            wtr.Write(shift);
            wtr.Write(alt);
        }

        #endregion
    }

    [Packet]
    public struct CaretPositionPacket : IBitSerialisable
    {
        public int chPos;
        public int colPos;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            chPos = rdr.ReadBitsAsInt32(32);
            colPos = rdr.ReadBitsAsInt32(32);
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.WriteBits(chPos, 32);
            wtr.WriteBits(colPos, 32);
        }

        #endregion
    }

    [Packet]
    public struct WaitForInputPacket : IBitSerialisable
    {
        public bool keyInput, lineInput;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            keyInput = rdr.ReadBitAsBool();
            lineInput = rdr.ReadBitAsBool();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(keyInput);
            wtr.Write(lineInput);
        }

        #endregion
    }

    [Packet]
    public struct ComputerBsodPacket : IBitSerialisable
    {
        public string errorCode;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            errorCode = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write(errorCode);
        }

        #endregion
    }
}
