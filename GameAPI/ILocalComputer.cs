﻿using System;
using System.Net;

namespace GameAPI
{
    public interface ILocalComputer
    {
        IPAddress Address { get; }
        IServiceManager Services { get; }
        bool HasAddress { get; }
        bool HasBootDrive { get; }

        void Connect(IUser user, int port);
        bool SendAPICallToService<T>(IService sender, IPAddress targetIP, IApiPacket packet) where T : IService;
        void Disconnect(IUser user);
        IDrive GetBootDriver();
        T GetBootDriver<T>() where T : IDrive;
        void HandleInput(IUser user, IUserInput input);
        void SetAddress(IPAddress address);
        void SetBootDrive(IDrive drive);
        void Shutdown();
    }
}