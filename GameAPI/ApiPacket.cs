﻿namespace GameAPI
{
    public interface IApiPacket
    {
        bool IsValid();
    }
}
