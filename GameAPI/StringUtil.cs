﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI
{
    public static class StringUtil
    {
        public static bool TryGetIPEndPoint(string parse, out IPEndPoint ipEndPoint)
        {
            var splitParse = parse.Split(':');

            if (splitParse.Length > 1)
            {
                if (IPAddress.TryParse(splitParse[0], out IPAddress ipAddress))
                {
                    if (ushort.TryParse(splitParse[1], out ushort port))
                    {
                        ipEndPoint = new IPEndPoint(ipAddress, port);
                        return true;
                    }
                }
            }
            else
            {
                if (IPAddress.TryParse(parse, out IPAddress ipAddress))
                {
                    ipEndPoint = new IPEndPoint(ipAddress, 22);
                    return true;
                }
            }

            ipEndPoint = null;
            return false;
        }

        public static IList<string> Segements(this string path)
        {
            path = path.Replace("\\", "/");
            var segs = new List<string>();

            var paths = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            return paths;
        }

        public static IEnumerable<string> SplitCommandLine(this string commandLine)
        {
            bool inQuotes = false;

            return commandLine.Split(c =>
            {
                if (c == '\"') inQuotes = !inQuotes;
                return !inQuotes && c == ' ';
            })
            .Select(arg => arg.Trim().TrimMatchingQuotes('\"'))
            .Where(arg => !string.IsNullOrEmpty(arg));
        }

        public static IEnumerable<string> Split(this string str, Func<char, bool> controller)
        {
            int nextPiece = 0;

            for (int c = 0; c < str.Length; c++)
            {
                if (controller(str[c]))
                {
                    yield return str.Substring(nextPiece, c - nextPiece);
                    nextPiece = c + 1;
                }
            }

            yield return str.Substring(nextPiece);
        }

        public static string TrimMatchingQuotes(this string input, char quote)
        {
            if (input.Length >= 2 && input[0] == quote && input[input.Length - 1] == quote)
                return input.Substring(1, input.Length - 2);

            return input;
        }
    }
}
