﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI
{
    public static class ComputerAPI
    {
        public static ILocalComputer LocalComputer => (ILocalComputer)AppDomain.CurrentDomain.GetData("localComputer");
    }
}
