﻿using System;
using System.Net;

namespace GameAPI
{
    public interface IComputer
    {
        IPAddress Address { get; }
        IServiceManager Services { get; }
        bool HasAddress { get; }
        bool HasBootDrive { get; }

        IDrive GetBootDriver();
        T GetBootDriver<T>() where T : IDrive;
        void HandleInput(IUser user, IUserInput input);
        void SetAddress(IPAddress address);
        void SetBootDrive(IDrive drive);
        void Shutdown();
    }
}