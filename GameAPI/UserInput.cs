﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarul;

namespace GameAPI
{
    public interface IUserInput : IStateData
    {

    }

    public class UserLineInput : MarshalByRefObject, IUserInput
    {
        public string Line { get; }

        public UserLineInput(string line)
        {
            Line = line;
        }
    }

    public class UserKeyInput : MarshalByRefObject, IUserInput
    {
        public bool Ctrl { get; }
        public bool Shift { get; }
        public Keyboard.KeyCode KeyCode { get; }

        public UserKeyInput(bool ctrl, bool shift, Keyboard.KeyCode keyCode)
        {
            Ctrl = ctrl;
            Shift = shift;
            KeyCode = keyCode;
        }
    }
}
