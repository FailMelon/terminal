﻿using GameAPI.Missions;
using System;
using System.Collections.Generic;
using System.Net;

namespace GameAPI
{
    public interface IUser : IEqualityComparer<IUser>
    { 
        string DatabaseName { get; }
        string Username { get; }
        IComputer LocalComputer { get; }
        IComputer TargetComputer { get; }
        ITerminal Console { get; }
        IMissionManager LocalMissionManager { get; }

        void SetTargetComputer(IComputer target);
    }
}