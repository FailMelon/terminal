﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace GameAPI
{
    public interface IServiceManager
    {
        int Count { get; }
        IComputer LocalComputer { get; }

        bool CanConnect(int port, IUser user, bool isLoopback);
        bool Connect(int port, IUser user, bool isLoopback);
        bool Disconnect(IUser user);
        T FindService<T>() where T : IService;
        void HandleUserInput(IUser user, IUserInput input);
        bool IsConnected(IUser user, IService service);
        bool IsConnected<T>(IUser user) where T : IService;
        bool Start(Type serviceType, bool openPort);
        bool Start<T>(bool openPort) where T : IService;
        bool Stop(int port);
        bool Stop(IService service);
        bool Open(int port);
        bool Close(int port);
        bool IsOpenService(int port);
        bool IsRunningService(int port);
        void Shutdown();
        IList<IService> GetServices();
        double GetTotalProcessorTime();
        long GetTotalMemoryUsage();
    }
}