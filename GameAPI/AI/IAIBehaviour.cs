﻿using GameAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameAPI
{
    public interface IAIChatBehaviour
    {
        void OnUserConnect(IUser user);

        void OnIncomingChat(IUser user, string message);

        void OnUserDisconnect(IUser user);
    }
}
