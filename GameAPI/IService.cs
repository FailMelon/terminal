﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GameAPI
{
    public interface IService : IEqualityComparer<IService>
    {
        string Name { get; }
        ushort Port { get; }
        IServiceManager Manager { get; set; }

        void OnStart();
        void OnConnect(IUser user, bool isOwner);
        void HandleInput(IUser user, IUserInput input);
        void OnDisconnect(IUser user);
        void OnReceiveAPICall(IService caller, IApiPacket packet);
    }

    public class ServiceAttribute : Attribute
    {
        public string Name { get; private set; }

        public ServiceAttribute(string name)
        {
            Name = name;
        }
    }
}
