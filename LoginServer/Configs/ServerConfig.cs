﻿using System;

namespace LoginServer.Configs
{
    public struct GameServer
    {
        public string Address;
        public ushort Port;
    }


    public class ServerConfig : IConfig
    {
        public string Address = "127.0.0.1";
        public ushort Port = 27018;
        public string ServicePath = "/terminal_login";
        public bool Secure = false;

        public GameServer[] GameServer = new GameServer[]
        {
            new GameServer
            {
                Address = "127.0.0.1",
                Port = 27019
            }
        };


        public GameServer GetRandomGameServer()
        {
            Random rng = new Random();

            if (GameServer.Length >= 1)
                return GameServer[rng.Next(0, GameServer.Length)];
            else
            {
                throw new Exception("No game servers provided in the list!");
            }
        }
    }
}
