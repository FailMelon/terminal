﻿using Newtonsoft.Json;
using System;

namespace LoginServer.Configs
{
    public interface IConfig
    {

    }

    public class Config<T> where T : IConfig
    {
        public static T Instance { get; private set; }

        public static void Save(string path)
        {
            var config = JsonConvert.SerializeObject(Instance, Formatting.Indented);
            System.IO.File.WriteAllText(path, config);
        }

        public static void Load(string path)
        {
            T config = default;

            if (System.IO.File.Exists(path))
                config = JsonConvert.DeserializeObject<T>(System.IO.File.ReadAllText(path));

            Instance = Equals(config, default(T)) ? Activator.CreateInstance<T>() : config;

            Save(path);
        }
    }
}