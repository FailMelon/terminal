﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace LoginServer
{
    public class Crypto
    {
        private static byte[] salt = Encoding.ASCII.GetBytes("FXFd82fhSJFK");

        public static string GeneratePasswordHash(string Pass)
        {
            byte[] tmpSource, tmpHash, SourceAndSalt;
            tmpSource = Encoding.ASCII.GetBytes(Pass);

            SourceAndSalt = new byte[tmpSource.Length + salt.Length];
            Array.Copy(tmpSource, SourceAndSalt, tmpSource.Length);
            Array.Copy(salt, 0, SourceAndSalt, tmpSource.Length, salt.Length);

            SHA256 shaM = new SHA256Managed();
            tmpHash = shaM.ComputeHash(tmpSource);

            return Convert.ToBase64String(tmpHash);
        }

        public static Match ValidUsername(string Username)
        {
            Regex regX = new Regex("^[_a-zA-Z0-9-]{3,32}$");
            return regX.Match(Username);
        }

        public static Match ValidPassword(string Password)
        {
            Regex regX = new Regex("^[_a-zA-Z0-9-]{3,32}$");
            return regX.Match(Password);
        }

        public static Match ValidEmail(string Email)
        {
            Regex regX = new Regex("^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+).(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$");
            return regX.Match(Email);
        }

        public static Match ValidKey(string Key)
        {
            Regex regX = new Regex("^([a-zA-Z0-9]{4})-([a-zA-Z0-9]{4})-([a-zA-Z0-9]{4})-([a-zA-Z0-9]{4})$");
            return regX.Match(Key);
        }

        public static string SanitizeInput(string thisInput)
        {
            Regex regX = new Regex(@"([<>""'%;()&])");
            return regX.Replace(thisInput, "");
        }
    }
}
