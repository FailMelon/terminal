﻿using LoginServer.Configs;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoginServer
{

    public class Database
    {
        protected static IMongoClient client;
        protected static IMongoDatabase DB;

        private static UpdateOptions upsertOpt = new UpdateOptions { IsUpsert = true };

        private static IMongoCollection<BsonDocument> usersCollection;

        public static bool Initialise()
        {
            var databaseConfig = Config<DatabaseConfig>.Instance;

            var settings = new MongoClientSettings
            {
                Servers = new[] { new MongoServerAddress(databaseConfig.Address, databaseConfig.Port) },
                Credential = MongoCredential.CreateCredential(databaseConfig.AuthenticationDatabase,
                    databaseConfig.AdminAccount.Username,
                    databaseConfig.AdminAccount.Password)
            };

            client = new MongoClient(settings);

            DB = client.GetDatabase("terminal-dev");

            usersCollection = DB.GetCollection<BsonDocument>("users");

            Log.Info($"Checking connection to the Database at address {databaseConfig.Address} on port {databaseConfig.Port}...");
            var canConnect = DB.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(10000);

            if (canConnect)
            {
                Log.Info("Connection to database established");
            }
            else
            {
                Log.Error("Failed to connect to the database");
                return false;
            }


            return true;
        }

        public static Task CreateAccount(string username, string password)
        {
            var userdoc = new BsonDocument
            {
                { "username", username },
                { "password", Crypto.GeneratePasswordHash(password) }
            };

            var task = usersCollection.InsertOneAsync(userdoc);
            return task;
        }

        public static string UserLogin(string username, string password) 
        {
            var filter = Builders<BsonDocument>.Filter.Eq("username", username) & Builders<BsonDocument>.Filter.Eq("password", Crypto.GeneratePasswordHash(password));

            var sessionID = Guid.NewGuid().ToString();

            var update = Builders<BsonDocument>.Update.Set("sessionid", sessionID);

            var bson = usersCollection.FindOneAndUpdate(filter, update);

            if (bson != null)
            {
                if (bson.TryGetValue("username", out BsonValue bsonUsername) && bsonUsername.IsString && bsonUsername.AsString == username)
                {
                    if (bson.TryGetValue("password", out BsonValue bsonPassword) && bsonPassword.IsString && bsonPassword.AsString == Crypto.GeneratePasswordHash(password))
                    {
                        // Get rid of password asap
                        bson = null;

                        return sessionID;
                    }
                }
            }

            return null;
        }       
    }
}
