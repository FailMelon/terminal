﻿using GameAPI.Packets;
using LoginServer.Configs;
using LoginServer.Networking;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Tarul;
using WebSocketSharp.Server;

namespace LoginServer
{

    public class Server
    {
        public static Server Singleton { get; private set; } = new Server();

        private WebSocketServer webSocket;

        private bool exit;

        /// <summary>
        /// Initialises the server
        /// </summary>
        private void Initialise(params string[] args)
        {
            Log.OnLogMessage += Log_OnLogMessage;

            AppDomain.CurrentDomain.FirstChanceException += (sender, eventArgs) =>
            {
                Log.Exception(eventArgs.Exception);
            };

            Directory.CreateDirectory("logs");
            Directory.CreateDirectory("configs");

            Config<ServerConfig>.Load("configs/ServerConfig.json");
            Config<DatabaseConfig>.Load("configs/DatabaseConfig.json");

            Log.Info("Registering packets");
            PacketSystem.Initialise(typeof(LoginPacket).Assembly);

            Log.Info("Loading database");
            if (!Database.Initialise())
            {
                Stop();
                return;
            }

            var serverConfig = Config<ServerConfig>.Instance;

            Log.Info($"Starting web socket server at address {serverConfig.Address} on port '{serverConfig.Port}' at service path '{serverConfig.ServicePath}'");
            webSocket = new WebSocketServer(IPAddress.Parse(serverConfig.Address), serverConfig.Port, serverConfig.Secure);
            webSocket.AddWebSocketService<TerminalWebSocketConnection>(serverConfig.ServicePath);
            webSocket.Start();

            /*
            Task.Run(async () =>
            {
                while (true)
                {
                    if (webSocket.WebSocketServices.TryGetServiceHost(serverConfig.ServicePath, out var host))
                    {
                        int open = 0;
                        int connecting = 0;
                        int closed = 0;
                        int closing = 0;

                        foreach(var session in host.Sessions.Sessions)
                        {
                            if (session.State == WebSocketSharp.WebSocketState.Connecting)
                                connecting++;
                            if (session.State == WebSocketSharp.WebSocketState.Open)
                                open++;

                            if (session.State == WebSocketSharp.WebSocketState.Closing)
                                closing++;
                            if (session.State == WebSocketSharp.WebSocketState.Closed)
                                closed++;
                        }

                        Console.Title = $"Terminal - Login Server # Sessions({host.Sessions.Count}) Connecting({connecting}) Open({open}) Closing({closing}) Closed({closed})";
                        await Task.Delay(1000);
                    }
                }
            });*/

            Log.Info("Server started!");
        }

        private void Log_OnLogMessage(Log.Level level, ConsoleColor tagColor, ConsoleColor textColor, string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(DateTime.Now);

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" - ");

            Console.ForegroundColor = tagColor;
            Console.Write($"[{level}] ");

            Console.ForegroundColor = textColor;
            Console.WriteLine(message);

            Console.ForegroundColor = ConsoleColor.White;

            if (!Directory.Exists($"logs/{DateTime.Now.ToShortDateString().Replace('/', '-')}"))
                Directory.CreateDirectory($"logs/{DateTime.Now.ToShortDateString().Replace('/', '-')}");

            File.AppendAllText($"logs/{DateTime.Now.ToShortDateString().Replace('/', '-')}/{level.ToString()}.log", $"{DateTime.Now.ToLongTimeString()} - [{level}] {message}\n");
        }

        /// <summary>
        /// Shuts down this server
        /// </summary>
        private void Shutdown()
        {
            Log.Info("Stopping server...");

            webSocket?.Stop();

            Log.Info("Server successfully stopped!");
        }

        /// <summary>
        /// Runs the server
        /// </summary>
        public void Run(params string[] args)
        {
            // Initialise
            Initialise(args);

            while (exit || Console.ReadKey(true).Key != ConsoleKey.Q) { }

            // Shut down
            Shutdown();
        }

        public void Stop()
        {
            exit = true;
        }
    }
}

