﻿using GameAPI.Packets;
using LoginServer.Configs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Tarul;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace LoginServer.Networking
{
    public interface INetConnection
    {
        void SendPacket(IBitSerialisable packet);

        event Action<INetConnection, string> Disconnected;

        AuthData Auth { get; }
    }

    public struct AuthData
    {
        public string Username;
    }

    public delegate void PacketReceived<T>(INetConnection sender, T packet) where T : struct;

    public class TerminalWebSocketConnection : WebSocketBehavior, INetConnection
    {
        public class JsonPacket
        {
            public string name;
            public JObject data;
        }

        public event Action<INetConnection, string> Disconnected;

        public AuthData Auth { get; private set; }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (!e.IsText) return;

            var packetRaw = JsonConvert.DeserializeObject<JsonPacket>(e.Data);

            var packetType = PacketSystem.GetPacketType(packetRaw.name);

            if (packetType == null) return;

            switch (packetRaw.data.ToObject(packetType))
            {
                case RegisterPacket pkt:
                    {
                        var validUsername = Crypto.ValidUsername(pkt.username.ToLower()).Success;
                        var validPassword = Crypto.ValidPassword(pkt.password).Success;

                        if (!validUsername)
                        {
                            GenericResponsePacket genericPkt;
                            genericPkt.responseType = GenericResponseType.Register;
                            genericPkt.failed = true;
                            genericPkt.reason = "Invalid username";
                            SendPacket(genericPkt);

                            return;
                        }

                        if (!validPassword)
                        {
                            GenericResponsePacket genericPkt;
                            genericPkt.responseType = GenericResponseType.Register;
                            genericPkt.failed = true;
                            genericPkt.reason = "Invalid password";
                            SendPacket(genericPkt);

                            return;
                        }

                        Database.CreateAccount(pkt.username.ToLower(), pkt.password).ContinueWith((task) =>
                        {

                            GenericResponsePacket genericPkt;
                            genericPkt.responseType = GenericResponseType.Register;

                            if (!task.IsFaulted)
                            {
                                genericPkt.failed = false;
                                genericPkt.reason = "Successfully Registered!";

                                LoginServer.Log.Debug($"User registered with the username ({pkt.username.ToLower()})");
                            }
                            else
                            {
                                genericPkt.failed = true;
                                genericPkt.reason = "Duplicate users";
                            }

                            SendPacket(genericPkt);
                        });
                    }
                    break;
                case LoginPacket pkt:
                    {
                        var sessionID = Database.UserLogin(pkt.username.ToLower(), pkt.password);

                        if (!string.IsNullOrEmpty(sessionID))
                        {
                            var gameServer = Config<ServerConfig>.Instance.GetRandomGameServer();

                            LoginSuccessPacket loginSuccessPkt;
                            loginSuccessPkt.sessionID = sessionID;
                            loginSuccessPkt.gameServerAddress = gameServer.Address;
                            loginSuccessPkt.gameServerport = gameServer.Port;
                            SendPacket(loginSuccessPkt);

                            LoginServer.Log.Debug($"Valid user ({pkt.username.ToLower()}) logged in, assigning game server {gameServer.Address}:{gameServer.Port} to them");
                        }
                        else
                        {
                            GenericResponsePacket genericPkt;
                            genericPkt.responseType = GenericResponseType.Login;
                            genericPkt.failed = true;
                            genericPkt.reason = "Invalid login details.";
                            SendPacket(genericPkt);
                        }

                    }
                    break;
            }
        }

        public void SendPacket(IBitSerialisable packet)
        {
            if (State == WebSocketState.Open)
            {
                var name = PacketSystem.GetPacketName(packet.GetType());

                var packetRaw = new JsonPacket
                {
                    name = name,
                    data = JObject.FromObject(packet)
                };

                Send(JsonConvert.SerializeObject(packetRaw));
            }
        }

        protected override void OnClose(CloseEventArgs e)
        {
            base.OnClose(e);

            Disconnected?.Invoke(this, e.Reason);


            Console.Title = $"Terminal - Login Server #  Sessions({Sessions.Count})";
        }

        public void Drop(string reason, CloseStatusCode code = CloseStatusCode.Normal)
        {
            Sessions.CloseSession(ID, code, reason);
        }
    }
}
