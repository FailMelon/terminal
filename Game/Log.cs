﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public static class Log
    {
        public enum Level
        {
            Debug = 0,
            Info = 1,
            Warning = 2,
            Error = 3,
            Exception = 4
        }

        public delegate void HandleLogMessage(Level level, ConsoleColor tagColor, ConsoleColor textColor, string message);
        public static event HandleLogMessage OnLogMessage;

        public static void Debug(object obj)
        {
            OnLogMessage?.Invoke(Level.Debug, ConsoleColor.Cyan, ConsoleColor.White, obj != null ? obj.ToString() : "null");
        }

        public static void Info(string message)
        {
            OnLogMessage?.Invoke(Level.Info, ConsoleColor.White, ConsoleColor.White, message);
        }

        public static void Info(string message, ConsoleColor tagColor, ConsoleColor textColor)
        {
            OnLogMessage?.Invoke(Level.Error, tagColor, textColor, message);
        }

        public static void Warning(string message)
        {
            OnLogMessage?.Invoke(Level.Warning, ConsoleColor.Yellow, ConsoleColor.White, message);
        }

        public static void Error(string message)
        {
            OnLogMessage?.Invoke(Level.Error, ConsoleColor.Red, ConsoleColor.White, message);
        }

        public static void Exception(Exception exception)
        {
            OnLogMessage?.Invoke(Level.Exception, ConsoleColor.DarkRed, ConsoleColor.Red, exception.ToString());
        }
    }
}
