﻿Shader "Unlit/GlowyShader"
{
	Properties
	{
		_Color("Main Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100
		Cull Off

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			float rand(float3 myVector) {
				return frac(sin(dot(myVector, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
			}


			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color.xyz = _Color.rgb;
				o.color.w = min(max(cos(((_Time.y / 5) + 1000) * cos(rand(float3(v.texcoord, v.color.r)))) - 0.8, 0), .8);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target{ return i.color; }
			ENDCG
		}
	}
}
