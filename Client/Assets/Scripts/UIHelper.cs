﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHelper : MonoBehaviour {

    public InputField Username, Password;

    public void Login()
    {
        LoginController.Instance.LoginUser(Username.text, Password.text);
    }

    public void Register()
    {
        LoginController.Instance.RegisterNewUser(Username.text, Password.text);
    }
}
