﻿using Client.Packets;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Terminal;
using UnityEngine;

public class Startup : MonoBehaviour {

	void Start ()
    {
        UnityMainThreadQueuer.Instance.Startup();
        PacketSystem.Initialise();
        LoginController.Instance.Startup();
    }
}
