﻿using Client.Packets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class TriangleBackground : MaskableGraphic
    {
        protected override void Start()
        {

        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

   
            var list = new List<UIVertex>();

            int scale = 20;

            var bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(transform.parent, transform);

            var maxX = Mathf.CeilToInt(Mathf.FloorToInt(bounds.size.x / (scale * 2f)) / 2f);
            var maxY = Mathf.CeilToInt(Mathf.FloorToInt(bounds.size.y / (scale * 2f)) / 2f);

            for (int i = -maxX; i < maxX; i++)
            {
                for (int j = -maxY; j < maxY; j++)
                {
                    Vector3 pos = new Vector3((i * 2f) * scale, (j * 2) * scale);

                    int sides = 6;
                    for (int side = 0; side < sides; side++)
                    {
                        float triangleCurve = (Mathf.PI / (sides / 2f));

                        float halfIncreament = triangleCurve / 2f;

                        float leftX = Mathf.Cos(triangleCurve * side - halfIncreament);
                        float leftY = Mathf.Sin(triangleCurve * side - halfIncreament);

                        float rightX = Mathf.Cos(triangleCurve * side + halfIncreament);
                        float rightY = Mathf.Sin(triangleCurve * side + halfIncreament);

                        list.Add(new UIVertex
                        {
                            position = pos + new Vector3(leftX * scale, leftY * scale),
                            uv0 = new Vector2(i, j),
                            color = new Color32((byte)side, 0, 0, 0)
                        });

                        list.Add(new UIVertex
                        {
                            position = pos + new Vector3(rightX * scale, rightY * scale),
                            uv0 = new Vector2(i, j),
                            color = new Color32((byte)side, 0, 0, 0)
                        });
                        
                        list.Add(new UIVertex
                        {
                            position = pos,
                            uv0 = new Vector2(i, j),
                            color = new Color32((byte)side, 0, 0, 0)
                        });
                    }
                }
            }

            vh.AddUIVertexTriangleStream(list);
        }

        private void Update()
        {
            material.color = color;
        }
    }
}