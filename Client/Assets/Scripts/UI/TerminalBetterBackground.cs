﻿using Client.Packets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class TerminalBetterBackground : MaskableGraphic
    {
        public TerminalBetter terminal;

        private bool isBlinkerSolid;
        private float cursorBlinkTime;

        public override Texture mainTexture => s_WhiteTexture;

        private void Update()
        {
            if (terminal.CursorVisible)
            {
                if (Time.time >= cursorBlinkTime)
                {
                    isBlinkerSolid = !isBlinkerSolid;

                    cursorBlinkTime = Time.time + 0.4f;
                    terminal.Dirty();
                }
            }
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            if (terminal.area != null)
            {
                var pos = Vector3.zero;

                for (int j = 0; j < terminal.WindowHeight; j++)
                {
                    for (int i = 0; i < terminal.WindowWidth; i++)
                    {
                        var glyph = terminal.area[i, j];

                        if (glyph == null)
                            continue;

                        pos = new Vector3(i * terminal.glyphWidth, j * -terminal.glyphHeight, 0);

                        var backgroundColor = glyph.backgroundColor;

                        if (terminal.CursorVisible && terminal.IsSelected && isBlinkerSolid && terminal.CaretPosition == new Vector2Int(i, j))
                        {
                            backgroundColor = Color.white;
                        }

                        var list = new UIVertex[]
                        {
                            new UIVertex
                            {
                                position = pos + new Vector3(0, 0, 0),
                                color = backgroundColor,
                                uv0 = new Vector2(0, 0)
                            },
                            new UIVertex
                            {
                                position = pos + new Vector3(0, -terminal.glyphHeight, 0),
                                color = backgroundColor,
                                uv0 = new Vector2(0, 1)
                            },
                            new UIVertex
                            {
                                position = pos + new Vector3(terminal.glyphWidth, -terminal.glyphHeight, 0),
                                color = backgroundColor,
                                uv0 = new Vector2(1, 1)
                            },
                            new UIVertex
                            {
                                position = pos + new Vector3(terminal.glyphWidth, 0, 0),
                                color = backgroundColor,
                                uv0 = new Vector2(1, 0)
                            }

                        };

                        vh.AddUIVertexQuad(list);
                    }
                }
            }
        }
    }

}