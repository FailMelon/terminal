﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UI
{
    public class AreaGlyph
    {
        public Color foregroundColor, backgroundColor;
        public char character;
    }

    public class Area
    {
        public int BufferWidth { get; private set; }
        public int BufferHeight { get; private set; }

        private AreaGlyph[,] area;

        public AreaGlyph this[int x, int y]
        {
            get
            {
                return area[x, y];
            }
            set
            {
                area[x, y] = value;
            }
        }


        public Area(int bufferWidth, int bufferHeight)
        {
            BufferWidth = bufferWidth;
            BufferHeight = bufferHeight;

            area = new AreaGlyph[bufferWidth, bufferHeight];

            Clear(' ', Color.white, Color.black);
        }

        public void Clear(char clearCharacter, Color clearForegroundColor, Color clearBackgroundColor)
        {
            for (int j = 0; j < BufferHeight; j++)
            {
                for (int i = 0; i < BufferWidth; i++)
                {
                    area[i, j] = new AreaGlyph
                    {
                        character = clearCharacter,
                        foregroundColor = clearForegroundColor,
                        backgroundColor = clearBackgroundColor
                    };
                }
            }
        }

        public void CopyTo(Area dst, RectInt srcRect, RectInt dstRect)
        {
            if (!Contains(srcRect)) return;
            int dstWidth = dst.BufferWidth, dstHeight = dst.BufferHeight;

            int copyWidth = dstRect.x + srcRect.width > dstWidth ? dstWidth - dstRect.x : srcRect.width; // Scanline size in glyphs
            int copyHeight = dstRect.y + srcRect.height > dstHeight ? dstHeight - dstRect.y : srcRect.height; // Number of scanlines to copy

            if (copyWidth > dstRect.width) copyWidth = dstRect.width;
            if (copyHeight > dstRect.height) copyHeight = dstRect.height;

            // Copy by scanline
            for (int y = 0; y < copyHeight; y++)
            {
                int srcIndex = (srcRect.y + y) * BufferWidth + srcRect.x;
                int dstIndex = (dstRect.y + y) * dstWidth + dstRect.x;

                Array.Copy(area, srcIndex, dst.area, dstIndex, copyWidth);
            }
        }

        public bool Contains(RectInt rect) => rect.x >= 0 && rect.y >= 0 && rect.width + rect.x <= BufferWidth && rect.height + rect.y <= BufferHeight;

        public override string ToString() => $"Area ({BufferWidth}, {BufferHeight})";
    }
}
