﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TerminalBetterText : MaskableGraphic
    {
        public TerminalBetter terminal;

        public Font font;

        public override Texture mainTexture
        {
            get
            {
                if (font != null)
                    return font.material.mainTexture;
                else
                    return s_WhiteTexture;
            }
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            if (terminal.area != null && font != null)
            {
                font.RequestCharactersInTexture("abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ !?.',></\\:;\"£$%^&*()_+-=#~`¬|│", terminal.fontSize);

                var pos = Vector3.zero;

                for (int j = 0; j < terminal.WindowHeight; j++)
                {
                    for (int i = 0; i < terminal.WindowWidth; i++)
                    {
                        var glyph = terminal.area[i, j];

                        if (glyph == null)
                            continue;

                        if (glyph.character != ' ')
                        {
                            // Get character rendering information from the font
                            CharacterInfo ch;
                            font.GetCharacterInfo(glyph.character, out ch, terminal.fontSize);

                            pos = new Vector3(i * terminal.glyphWidth, j * -terminal.glyphHeight, 0);

                            int height = terminal.glyphHeight - ch.maxY;

                            vh.AddUIVertexQuad(new UIVertex[]
                            {
                                new UIVertex
                                {
                                    position = pos + new Vector3(0, -height, 0),
                                    color = glyph.foregroundColor,
                                    uv0 = ch.uvTopLeft
                                },
                                new UIVertex
                                {
                                    position = pos + new Vector3(0, -height + -ch.maxY, 0),
                                    color = glyph.foregroundColor,
                                    uv0 = ch.uvBottomLeft
                                },
                                new UIVertex
                                {
                                    position = pos + new Vector3(ch.maxX, -height + -ch.maxY, 0),
                                    color = glyph.foregroundColor,
                                    uv0 = ch.uvBottomRight
                                },
                                new UIVertex
                                {
                                    position = pos + new Vector3(ch.maxX, -height, 0),
                                    color = glyph.foregroundColor,
                                    uv0 = ch.uvTopRight
                                }

                            });
                        }
                    }
                }
            }
        }
    }

}