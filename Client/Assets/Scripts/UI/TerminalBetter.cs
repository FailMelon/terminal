﻿using Client.Packets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class TerminalBetter : Selectable, IPointerDownHandler
    {
        private struct TerminalData
        {
            public Color background, foreground;
            public string data;
        }

        public static TerminalBetter Instance;

        public TerminalBetterText text;
        public TerminalBetterBackground background;

        private Area m_area;

        public Area area
        {
            set
            {
                m_area = value;
                isDirty = true;
            }
            get
            {
                return m_area;
            }
        }


        private bool m_cursorVisible;

        public bool CursorVisible
        {
            set
            {
                m_cursorVisible = value;
                isDirty = true;
            }
            get
            {
                return m_cursorVisible;
            }
        }

        public int glyphWidth = 10;
        public int glyphHeight = 14;
        public int fontSize = 14;

        public int WindowWidth { get; private set; }
        public int WindowHeight { get; private set; }

        public bool IsSelected
        {
            get
            {
                return EventSystem.current.currentSelectedGameObject == gameObject;
            }
        }

        public Vector2Int CaretPosition;

        private Color backgroundColor;
        private Color foregroundColor;

        private bool readLine;
        private Vector2Int readPosition;
        private string readInput;
        private Action<string> readCallback;
        
        private bool isDirty;

        private Queue<Task> tasks;

        private bool slowMode;
        private bool speedUpAction;

        private Vector2Int tempCaretPosition;
        private Task currentTask;

        private List<TerminalData> output;

        protected TerminalBetter()
            : base() { }

        protected override void Start()
        {
            CaretPosition = Vector2Int.zero;
            slowMode = false;

            Select();

            tasks = new Queue<Task>();

            output = new List<TerminalData>();

            Instance = this;

            backgroundColor = Color.black;
            foregroundColor = Color.white;

            var bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(transform.parent, transform);

            var maxX = Mathf.FloorToInt(bounds.size.x / glyphWidth);
            var maxY = Mathf.CeilToInt(bounds.size.y / glyphHeight);

            WindowWidth = maxX;
            WindowHeight = maxY;

            area = new Area(Mathf.Min(WindowWidth, 512), Mathf.Max(WindowHeight, 128));

            EnteredAndInfoPacket enteredpkt;
            enteredpkt.terminalWindowWidth = maxX - 1;
            enteredpkt.terminalWindowHeight = maxY;
            enteredpkt.terminalBufferWidth = area.BufferWidth;
            enteredpkt.terminalBufferHeight = area.BufferHeight;

            GameController.Instance.LocalConnection.SendPacket(enteredpkt);
        }

        public void SlowMode(bool slow)
        {
            tasks.Enqueue(new Task(() =>
            {
                slowMode = slow;
            }));
        }

        public void SaveCaretPosition()
        {
            tasks.Enqueue(new Task(() =>
            {
                tempCaretPosition = CaretPosition;
            }));
        }

        public void RestoreCaretPosition()
        {
            tasks.Enqueue(new Task(() =>
            {
                CaretPosition = tempCaretPosition;
                isDirty = true;
            }));
        }

        public void SetColor(Color foreground, Color background)
        {
            tasks.Enqueue(new Task(() =>
            {
                foregroundColor = foreground;
                backgroundColor = background;
            }));
        }

        public void ClearLine()
        {
            tasks.Enqueue(new Task(() =>
            {
                for (int i = 0; i < area.BufferWidth; i++)
                {
                    area[i, CaretPosition.y] = new AreaGlyph
                    {
                        character = ' ',
                        foregroundColor = foregroundColor,
                        backgroundColor = backgroundColor
                    };
                }

                isDirty = true;
            }));
        }

        public void Clear()
        {
            tasks.Enqueue(new Task(() =>
            {
                area.Clear(' ', Color.white, Color.black);

                Internal_SetCursorPosition(0, 0);
            }));
        }

        public void MoveAreaPacket(int sourceX, int sourceY, int sourceWidth, int sourceHeight, int targetX, int targetY)
        {
            tasks.Enqueue(new Task(() =>
            {
                Area newArea = new Area(sourceWidth, sourceHeight);

                for (int i = 0; i < sourceWidth; i++)
                {
                    for (int j = 0; j < sourceHeight; j++)
                    {
                        var glyph = area[i + sourceX, j + sourceY];

                        newArea[i, j] = glyph;

                        area[i + sourceX, j + sourceY] = new AreaGlyph
                        {
                            character = ' ',
                            foregroundColor = foregroundColor,
                            backgroundColor = backgroundColor
                        };
                    }
                }

                for (int i = 0; i < sourceWidth; i++)
                {
                    for (int j = 0; j < sourceHeight; j++)
                    {
                        var glyph = newArea[i, j];

                        area[i + targetX, j + targetY] = glyph;
                    }
                }

                isDirty = true;
            }));
        }


        public void SetCursorPosition(int x, int y)
        {
            tasks.Enqueue(new Task(() =>
            {
                CaretPosition.x = x;
                CaretPosition.y = y;

                isDirty = true;
            }));
        }


        private void Internal_SetCursorPosition(int x, int y)
        {
            CaretPosition.x = x;
            CaretPosition.y = y;

            isDirty = true;
        }

        public void ReadLine(Action<string> action)
        {
            tasks.Enqueue(new Task(() =>
            {
                readLine = true;
                readCallback = action;
                readPosition = CaretPosition;
                readInput = string.Empty;
                CursorVisible = true;
                isDirty = true;
            }));            
        }

        public void Write(string str)
        {
            tasks.Enqueue(new Task(() =>
            {
                for (int i = 0; i < str.Length; i++)
                {
                    char cha = str[i];
                    Write(cha);

                    if (slowMode && !speedUpAction)
                        Thread.Sleep(10);
                }
            }));
        }

        public void Write(char cha)
        {
            if (cha == '\n')
            {
                Internal_SetCursorPosition(0, CaretPosition.y + 1);
            }
            else if (cha == '\r')
            {
                return;
            }
            else
            {
                area[CaretPosition.x, CaretPosition.y] = new AreaGlyph
                {
                    character = cha,
                    foregroundColor = foregroundColor,
                    backgroundColor = backgroundColor
                };

                Internal_SetCursorPosition(CaretPosition.x + 1, CaretPosition.y);

                if (CaretPosition.x >= WindowWidth - 1)
                    Internal_SetCursorPosition(0, CaretPosition.y + 1);
            }

            isDirty = true;

        }

        public void WriteLine(string str)
        {
            tasks.Enqueue(new Task(() =>
            {
                for (int i = 0; i < str.Length; i++)
                {
                    char cha = str[i];
                    Write(cha);

                    if (slowMode && !speedUpAction)
                        Thread.Sleep(10);
                }

                Internal_SetCursorPosition(0, CaretPosition.y + 1);
            }));
        }

        /*
        private void UpdateArea()
        {
            int currentX = 0, currentY = 0;
            foreach(var terminalData in output)
            {
                var data = terminalData.data;

                for(int i = 0; i < data.Length; i++)
                {
                    if (data[i] != '\n')
                    {
                        currentY++;
                    }
                    else
                    {
                        area[currentX, currentY] = new AreaGlyph
                        {
                            character = data[i],
                            foregroundColor = terminalData.foreground,
                            backgroundColor = terminalData.background
                        };

                        currentX++;
                    }


                }
            }
        }*/

        private void Update()
        {
            if (tasks.Count > 0)
            {
                if (slowMode)
                {
                    if (speedUpAction)
                    {
                        if (currentTask != null && !currentTask.IsCompleted)
                            currentTask.Wait();

                        while (tasks.Count > 0)
                        {
                            var task = tasks.Dequeue();
                            task.Start();
                            task.Wait();
                        }

                        speedUpAction = false;
                    }
                    else
                    {
                        if (currentTask == null || currentTask.IsCompleted)
                        {
                            currentTask = tasks.Dequeue();
                            currentTask.Start();
                        }
                    }
                }
                else
                {
                    while(tasks.Count > 0)
                    {
                        var task = tasks.Dequeue();
                        task.Start();
                        task.Wait();
                    }
                }
            }

            if (readLine && IsSelected)
            {
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V))
                {
                    Debug.Log(":D");
                }

                for (int i = 0; i < Input.inputString.Length; i++)
                {
                    char cha = Input.inputString[i];

                    switch (cha)
                    {
                        case '\b':
                            if (CaretPosition.x > readPosition.x)
                            {
                                readInput = readInput.Substring(0, readInput.Length - 1);
                                Internal_SetCursorPosition(CaretPosition.x - 1, CaretPosition.y);

                                area[CaretPosition.x, CaretPosition.y] = new AreaGlyph
                                {
                                    character = ' ',
                                    foregroundColor = foregroundColor,
                                    backgroundColor = backgroundColor
                                };
                            }
                            break;
                        case '\r':
                            {
                                readCallback(readInput);
                                readLine = false;
                                Internal_SetCursorPosition(0, CaretPosition.y + 1);
                            }
                            break;
                        default:
                            {
                                readInput += cha;
                                Write(cha);
                            }
                            break;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    speedUpAction = true;
                }
            }

            if (isDirty)
            {
                background.SetVerticesDirty();
                text.SetVerticesDirty();

                isDirty = false;
            }
        }

        public void Dirty()
        {
            isDirty = true;
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);

            EventSystem.current.SetSelectedGameObject(gameObject);
        }
    }

}