﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Notepad : MonoBehaviour {


    private void Start()
    {
        if (!File.Exists("notepad.txt"))
            File.Create("notepad.txt").Close();

        GetComponent<InputField>().onValueChanged.AddListener((value) =>
        {
            File.WriteAllText("notepad.txt", GetComponent<InputField>().text);
        });

        GetComponent<InputField>().text = File.ReadAllText("notepad.txt");
    }
}
