﻿using Tarul;

namespace Client.Packets
{
    public enum GenericResponseType : byte
    {
        Login,
        Register
    }

    [Packet]
    public struct GenericResponsePacket : IBitSerialisable
    {
        public GenericResponseType responseType;
        public bool failed;
        public string reason;

        #region ISerialisable

        public void Deserialise(BitStreamReader rdr)
        {
            responseType = (GenericResponseType)rdr.ReadBitsAsByte(8);
            failed = rdr.ReadBitAsBool();
            reason = rdr.ReadString();
        }

        public void Serialise(BitStreamWriter wtr)
        {
            wtr.Write((byte)responseType);
            wtr.Write(failed);
            wtr.Write(reason);
        }

        #endregion
    }
}
