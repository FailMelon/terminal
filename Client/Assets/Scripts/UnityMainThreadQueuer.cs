﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tarul.Threading;
using UnityEngine;

public class UnityMainThreadQueuer : MonoBehaviour {

    public static UnityMainThreadQueuer Instance;

    private ProducerConsumer<Action> methods;
    private object syncroot;

    void Awake()
    {
        Instance = this;
    }

    public void Startup()
    {
        methods = new ProducerConsumer<Action>();
        syncroot = new object();
    }

    public void QueueMethod(Action method)
    {
        lock(syncroot)
            methods.Produce(method);
    }

    void Update()
    {
        lock (syncroot)
        {
            Action method;
            while (methods.TryConsume(out method))
            {
                method.Invoke();
            }
        }
    }
}
