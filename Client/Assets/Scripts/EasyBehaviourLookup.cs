﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyBehaviourLookup : MonoBehaviour
{
    public MonoBehaviour Behaviour;

    public T GetBehaviour<T>() where T : MonoBehaviour
    {
        return (T)Behaviour;
    }
}
