﻿using Client.Networking;
using Client.Packets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WebSocketSharp;
using WebSocketSharp.Net;

public class GameController : MonoBehaviour {

    public static GameController Instance { get; private set; }

    public WebSocket WebSocket { get; private set; }

    public TerminalWebSocketConnection LocalConnection;

    public GameObject TerminalPrefab;
    public GameObject UICanvas;

    private GameObject terminalGO;

    private void Awake()
    {
        Instance = this;
    }

    public void Connect(string username, string sessionID, string address, ushort port)
    {
        Debug.Log($"Connecting to ws://{address}:{port}/terminal_game");

        WebSocket = new WebSocket($"ws://{address}:{port}/terminal_game");

        WebSocket.SetCookie(new Cookie("username", username));
        WebSocket.SetCookie(new Cookie("sessionid", sessionID));

        WebSocket.OnOpen += Ws_OnOpen;
        WebSocket.OnClose += Ws_OnClose;
        WebSocket.OnError += Ws_OnError;

        WebSocket.Connect();
    }

    public void Update()
    {
        LocalConnection?.Update();
    }

    private void Ws_OnOpen(object sender, EventArgs e)
    {
        LocalConnection = new TerminalWebSocketConnection(WebSocket);

        LocalConnection.SubscribeToPacket<GenericResponsePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<EnterWorldPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<WriteLinePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<WritePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<ColorUpdatePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<WaitForInputPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<CaretPositionPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<MoveBufferAreaPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<SlowModePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<SaveCursorAndRestorePositionPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<ComputerPerformancePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<ComputerServicesPacket>(OnPacket);
        LocalConnection.SubscribeToPacket<ClearPacket>(OnPacket);


        Debug.Log("[Game] Game Connection succcess");
    }

    private void OnPacket(TerminalWebSocketConnection sender, GenericResponsePacket packet)
    {
        Debug.Log("[Game] Generic Response Packet: (" + packet.responseType.ToString() + ") " + packet.reason);
    }

    private void OnPacket(TerminalWebSocketConnection sender, EnterWorldPacket pkt)
    {
        terminalGO = Instantiate(TerminalPrefab, UICanvas.transform);
    }

    private void OnPacket(TerminalWebSocketConnection sender, WriteLinePacket pkt)
    {
        UI.TerminalBetter.Instance.WriteLine(pkt.data);
    }

    private void OnPacket(TerminalWebSocketConnection sender, WritePacket pkt)
    {
        UI.TerminalBetter.Instance.Write(pkt.data);
    }

    private void OnPacket(TerminalWebSocketConnection sender, ColorUpdatePacket pkt)
    {
        var foreground = new Color32(pkt.foreground.R8, pkt.foreground.G8, pkt.foreground.B8, 255);
        var background = new Color32(pkt.background.R8, pkt.background.G8, pkt.background.B8, 255);

        UI.TerminalBetter.Instance.SetColor(foreground, background);
    }

    private void OnPacket(TerminalWebSocketConnection sender, WaitForInputPacket pkt)
    {
        UI.TerminalBetter.Instance.ReadLine((s) =>
        {
            LineInputPacket inputPkt;
            inputPkt.input = s;

            LocalConnection.SendPacket(inputPkt);
        });
    }

    private void OnPacket(TerminalWebSocketConnection sender, CaretPositionPacket packet)
    {
        UI.TerminalBetter.Instance.SetCursorPosition(packet.chPos, packet.colPos);
    }

    private void OnPacket(TerminalWebSocketConnection sender, MoveBufferAreaPacket packet)
    {
        UI.TerminalBetter.Instance.MoveAreaPacket(packet.sourceX, packet.sourceY, packet.sourceWidth, packet.sourceHeight, packet.targetX, packet.targetY);
    }

    private void OnPacket(TerminalWebSocketConnection sender, SlowModePacket packet)
    {
        UI.TerminalBetter.Instance.SlowMode(packet.slowMode);
    }

    private void OnPacket(TerminalWebSocketConnection sender, SaveCursorAndRestorePositionPacket packet)
    {
        if (packet.save)
            UI.TerminalBetter.Instance.SaveCaretPosition();
        else
            UI.TerminalBetter.Instance.RestoreCaretPosition();
    }

    private void OnPacket(TerminalWebSocketConnection sender, ComputerPerformancePacket packet)
    {
        GameObject.Find("MemoryText").GetComponent<Text>().text = "Mem: " + (packet.activeMemory / 1024) + "kB";
        GameObject.Find("CPUText").GetComponent<Text>().text = $"CPU: {packet.currentProcessingTime}%";
    }

    private void OnPacket(TerminalWebSocketConnection sender, ComputerServicesPacket packet)
    {
        StringBuilder services = new StringBuilder();

        foreach(var service in packet.services)
            services.AppendLine(service);

        GameObject.Find("ServicesText").GetComponent<Text>().text = services.ToString();

    }

    private void OnPacket(TerminalWebSocketConnection sender, ClearPacket packet)
    {
        if (packet.type == TerminalClearType.Buffer)
            UI.TerminalBetter.Instance.Clear();
        else if (packet.type == TerminalClearType.Line)
            UI.TerminalBetter.Instance.ClearLine();

    }

    private void Ws_OnError(object sender, WebSocketSharp.ErrorEventArgs e)
    {
        Debug.LogException(e.Exception);
    }

    private void Ws_OnClose(object sender, CloseEventArgs e)
    {
        if (LocalConnection != null)
            LocalConnection.ClearSubscriptions();

        UnityMainThreadQueuer.Instance.QueueMethod(() =>
        {
            Debug.Log("[Game] Connection closed (" + (CloseStatusCode)e.Code + ") (" + e.Reason + ")");

            Destroy(terminalGO);

            LoginController.Instance.Startup();
        });
    }

    private void OnApplicationQuit()
    {
        WebSocket?.Close();
    }
}
