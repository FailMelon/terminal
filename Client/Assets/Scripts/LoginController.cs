﻿using Client.Networking;
using Client.Packets;
using Lidgren.Network;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Tarul.Networking;
using Tarul.Networking.Client;
using Terminal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketSharp;

public class LoginController : MonoBehaviour {

    public static LoginController Instance { get; private set; }

    public GameObject TitlePrefab;
    public GameObject LoginWindowPrefab;
    public GameObject UICanvas;

    public TerminalWebSocketConnection LocalConnection;
    public WebSocket WebSocket { get; private set; }
    
    private GameObject titleGO;
    private GameObject loginWindowGO;

    private string username;

    private void Awake()
    {
        Instance = this;
    }

    public void Startup()
    {
        titleGO = Instantiate(TitlePrefab, UICanvas.transform);

        loginWindowGO = Instantiate(LoginWindowPrefab, UICanvas.transform);
        loginWindowGO.GetComponent<Animator>().SetTrigger("Open");
        loginWindowGO.GetComponent<AnimatorUIHelper>().DisableInteractionOnChildren();

        WebSocket = new WebSocket($"ws://melonmesa.com:27018/terminal_login");
        WebSocket.OnOpen += Ws_OnOpen;
        WebSocket.OnClose += Ws_OnClose;
        WebSocket.OnError += Ws_OnError;
        WebSocket.Connect();
    }

    public void Update()
    {
        LocalConnection?.Update();
    }

    private void Ws_OnError(object sender, WebSocketSharp.ErrorEventArgs e)
    {
        Debug.LogException(e.Exception);
    }

    private void Ws_OnOpen(object sender, EventArgs e)
    {
        LocalConnection = new TerminalWebSocketConnection(WebSocket);

        LocalConnection.SubscribeToPacket<GenericResponsePacket>(OnPacket);
        LocalConnection.SubscribeToPacket<LoginSuccessPacket>(OnPacket);

        UnityMainThreadQueuer.Instance.QueueMethod(() =>
        {
            Debug.Log("[Login] Connection opened");

            if (loginWindowGO != null)
                loginWindowGO.GetComponent<AnimatorUIHelper>().EnableInteractionOnChildren();
        });
    }

    private void Ws_OnClose(object sender, CloseEventArgs e)
    {
        UnityMainThreadQueuer.Instance.QueueMethod(() =>
        {
            Debug.Log("[Login] Connection closed (" + e.Reason + ")");

            if (loginWindowGO != null)
                loginWindowGO.GetComponent<AnimatorUIHelper>().DisableInteractionOnChildren();
        });

        if (LocalConnection != null)
        {
            LocalConnection.ClearSubscriptions();
            LocalConnection = null;
        }
    }

    public void RegisterNewUser(string username, string password)
    {
        if (LocalConnection != null && username != null && password != null)
        {
            LocalConnection.SendPacket(new RegisterPacket()
            {
                username = username,
                password = password
            });

            loginWindowGO.GetComponent<AnimatorUIHelper>().DisableInteractionOnChildren();
        }
    }

    public void LoginUser(string username, string password)
    {
        if (LocalConnection != null && username != null && password != null)
        {
            this.username = username;

            LocalConnection.SendPacket(new LoginPacket()
            {
                username = username,
                password = password
            });

            loginWindowGO.GetComponent<AnimatorUIHelper>().DisableInteractionOnChildren();
        }
    }

    private void OnPacket(TerminalWebSocketConnection sender, GenericResponsePacket packet)
    {
        loginWindowGO.GetComponent<AnimatorUIHelper>().EnableInteractionOnChildren();
        Debug.Log("[Login] Generic Response Packet: (" + packet.responseType.ToString() + ") " + packet.reason);
    }

    private void OnPacket(TerminalWebSocketConnection sender, LoginSuccessPacket packet)
    {
        Destroy(loginWindowGO);
        Destroy(titleGO);

        WebSocket.CloseAsync();
        WebSocket = null;

        GameController.Instance.Connect(username, packet.sessionID, packet.gameServerAddress, packet.gameServerport);
    }

    private void OnApplicationQuit()
    {
        WebSocket?.Close();
    }
}
