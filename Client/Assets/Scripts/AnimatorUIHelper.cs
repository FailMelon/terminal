﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatorUIHelper : MonoBehaviour {


    public void EnableInteractionOnChildren()
    {
        var selectables = gameObject.transform.GetComponentsInChildren<Selectable>();
        foreach (var selectable in selectables)
            selectable.interactable = true;
    }

    public void DisableInteractionOnChildren()
    {
        var selectables = gameObject.transform.GetComponentsInChildren<Selectable>();
        foreach (var selectable in selectables)
            selectable.interactable = false;


    }
}
