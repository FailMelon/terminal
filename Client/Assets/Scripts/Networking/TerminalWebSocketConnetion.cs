﻿using Client.Packets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarul;
using Tarul.Threading;
using UnityEngine;
using WebSocketSharp;

namespace Client.Networking
{
    public class TerminalWebSocketConnection
    {
        public class JsonPacket
        {
            public string name;
            public JObject data;
        }


        public delegate void PacketReceived<T>(TerminalWebSocketConnection sender, T packet) where T : struct;

        private ProducerConsumer<object> packets;

        private IDictionary<Type, HashSet<Delegate>> callbacks;
        private object syncroot;
        private WebSocket webSocket;

        public TerminalWebSocketConnection(WebSocket webSocket)
        {
            this.webSocket = webSocket;
            packets = new ProducerConsumer<object>();
            callbacks = new Dictionary<Type, HashSet<Delegate>>();
            syncroot = new object();

            webSocket.OnMessage += WebSocket_OnMessage;
        }

        private void WebSocket_OnMessage(object sender, MessageEventArgs e)
        {
            var packetRaw = JsonConvert.DeserializeObject<JsonPacket>(e.Data);

            var packetType = PacketSystem.GetPacketType(packetRaw.name);

            if (packetType != null)
            {
                packets.Produce(packetRaw.data.ToObject(packetType));
            }
        }

        public void SubscribeToPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                HashSet<Delegate> set;
                if (!callbacks.TryGetValue(t, out set))
                {
                    set = new HashSet<Delegate>();
                    callbacks.Add(t, set);
                }
                set.Add(callback);
            }
        }

        public void UnsubscribeFromPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                HashSet<Delegate> set;
                if (!callbacks.TryGetValue(t, out set)) return;
                set.Remove(callback);
            }
        }

        public void ClearSubscriptions()
        {
            lock (syncroot)
                callbacks.Clear();
        }

        public void SendPacket(IBitSerialisable packet)
        {
            var name = PacketSystem.GetPacketName(packet.GetType());

            var packetRaw = new JsonPacket
            {
                name = name,
                data = JObject.FromObject(packet)
            };

            webSocket.Send(JsonConvert.SerializeObject(packetRaw));
        }

        public void Update()
        {
            lock (syncroot)
            {
                object packet;
                while (packets.TryConsume(out packet))
                {
                    HashSet<Delegate> handlers;
                    if (callbacks.TryGetValue(packet.GetType(), out handlers))
                    {
                        foreach (Delegate handler in handlers)
                            handler.DynamicInvoke(this, packet);
                    }
                    else
                    {
                        Debug.LogFormat("No event handlers for event '{0}'", packet.GetType().Name);
                    }
                }
            }
        }
    }
}
