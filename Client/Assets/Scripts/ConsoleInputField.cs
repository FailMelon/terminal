﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConsoleInputField : InputField
{
    public struct ConsoleTextData
    {
        public string text;
        public float speed;
    }

    public static ConsoleInputField Instance;

    public Action<string> OnReturn;

    public int StartReadLength { get; private set; }
    public bool StartReadLine { get; private set; }

    public Queue<ConsoleTextData> ConsoleText;

    private ConsoleTextData currentConsoleTextWord;
    private float timeBetweenPrint;
    private bool isWriting;
    private bool isReading;

    protected override void Awake()
    { 
        base.Awake();
        Instance = this;
        ConsoleText = new Queue<ConsoleTextData>();
        Instance.DeactivateInputField();
    }

    public static void Clear()
    {
        Instance.Internal_Clear();
    }

    private void Internal_Clear()
    {
        text = "";
        ConsoleText.Clear();
        UpdateLayout();
    }

    public static void WriteLine(string value, float speed)
    {
        Write(value + "\n", speed);
    }

    public static void Write(string value, float speed)
    {
        var splitText = value.Split(' ').ToList();
        for (int i = 0; i < splitText.Count; i++)
        {
            var text = splitText[i];

            if (i != splitText.Count - 1)
                text += " ";

            Instance.ConsoleText.Enqueue(new ConsoleTextData() { text = text, speed = speed });
        }
    }

    public static void ReadLine(Action<string> complete)
    {
        Instance.ActivateInputField();
        Instance.StartReadLength = Instance.text.Length;
        Instance.StartReadLine = true;
        Instance.OnReturn += (s) =>
        {
            Instance.DeactivateInputField();
            complete(s);
            Instance.StartReadLine = false;
            Instance.OnReturn = null;
        };
    }


    public void Update()
    {
        if (!isReading)
        {
            if (currentConsoleTextWord.text != null || ConsoleText.Count > 0)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (currentConsoleTextWord.text != null)
                    {
                        text += currentConsoleTextWord.text;
                        currentConsoleTextWord.text = null;
                    }

                    while (ConsoleText.Count > 0)
                    {
                        text += ConsoleText.Dequeue().text;
                    }

                    caretPosition = text.Length;
                    UpdateLayout();
                }
            }

            if (ConsoleText.Count > 0)
            {
                isWriting = true;

                if (currentConsoleTextWord.text == null)
                    currentConsoleTextWord = ConsoleText.Dequeue();
            }
            else
            {
                if (currentConsoleTextWord.text == null)
                {
                    isWriting = false;
                    StartReadLength = text.Length;
                }
            }

            if (currentConsoleTextWord.text != null)
            {
                if (currentConsoleTextWord.text.Length > 0)
                {
                    if (Time.time >= timeBetweenPrint)
                    {
                        text += currentConsoleTextWord.text.Substring(0, 1);
                        currentConsoleTextWord.text = currentConsoleTextWord.text.Substring(1, currentConsoleTextWord.text.Length - 1);
                        timeBetweenPrint = Time.time + currentConsoleTextWord.speed;

                        caretPosition = text.Length;
                        UpdateLayout();
                    }
                }
                else
                {
                    currentConsoleTextWord.text = null;
                }
            }
        }


        if (!isWriting)
        {
            if (StartReadLine)
            {
                isReading = true;

                if (isFocused)
                {
                    for (int i = 0; i < Input.inputString.Length; i++)
                    {
                        char cha = Input.inputString[i];

                        switch (cha)
                        {
                            case '\b':
                                if (text.Length > StartReadLength)
                                {
                                    text = text.Substring(0, text.Length - 1);
                                    caretPosition = text.Length;
                                    UpdateLayout();
                                }
                                break;
                            case '\r':
                                {
                                    StartReadLine = false;
                                    OnReturn?.Invoke(text.Substring(StartReadLength, text.Length - StartReadLength));
                                    text += "\n";
                                    caretPosition = text.Length;
                                    UpdateLayout();
                                    isReading = false;
                                }
                                break;
                            default:
                                {
                                    text += cha;
                                    caretPosition = text.Length;
                                    UpdateLayout();

                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    public void UpdateLayout()
    {
        GetComponent<ContentSizeFitter>().SetLayoutVertical();
        LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();
        MoveTextEnd(true);
        m_CaretVisible = StartReadLine;
    }


}
