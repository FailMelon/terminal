﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class DelayVideoLoop : MonoBehaviour {

    public float Delay;

    private bool delayTimer;
    private float delayTime;

    private VideoPlayer videoPlayer;
    private RawImage rawImage;

    private void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        rawImage = GetComponent<RawImage>();
    }

    void Update()
    {
        if (delayTimer)
        {
            if (Time.time >= delayTime)
            {
                delayTimer = false;
                videoPlayer.frame = Random.Range(0, (int)videoPlayer.frameCount);
                videoPlayer.Play();
            }
        }
        else if (videoPlayer.frame > 0)
        {
            if (!videoPlayer.isPlaying)
            {
                if (!delayTimer)
                {
                    delayTimer = true;
                    delayTime = Time.time + Random.Range(5, 10);
                    videoPlayer.Pause();
                }
            }
        }

        if (videoPlayer.texture != null)
            rawImage.texture = videoPlayer.texture;

        rawImage.enabled = rawImage.texture != null;
    }


}
