﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
[ExecuteInEditMode]
public class AutoScroll : MonoBehaviour, IScrollHandler
{
    public bool StopAutoScroll;
    private float newVerticalPos;

    void Update ()
    {
        if (StopAutoScroll)
        {
            if (GetComponent<ScrollRect>().verticalNormalizedPosition == 0)
            {
                StopAutoScroll = false;
            }

            GetComponent<ScrollRect>().verticalNormalizedPosition = newVerticalPos;
        }
        else if (GetComponent<ScrollRect>().verticalNormalizedPosition > 0)
        {
            GetComponent<ScrollRect>().verticalNormalizedPosition = 0;
        }
    }

    public void OnScroll(PointerEventData eventData)
    {
        var scrollDelta = (eventData.scrollDelta.normalized.y * Time.deltaTime);
        var verticalNormalPos = GetComponent<ScrollRect>().verticalNormalizedPosition;
        newVerticalPos = Mathf.Min(Mathf.Max(verticalNormalPos - scrollDelta, 0), 1);
        StopAutoScroll = true;
    }
}
