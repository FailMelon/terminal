﻿using GameServer.Packets;
using Lidgren.Network;
using System;
using System.Collections.Generic;
using Tarul;
using Tarul.Networking;
using Tarul.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebSocketSharp;

namespace FastTestClient.Networking
{
    public class TerminalWebSocketConnection
    {
        public class JsonPacket
        {
            public string name;
            public JObject data;
        }


        public delegate void PacketReceived<T>(TerminalWebSocketConnection sender, T packet) where T : struct;

        private IDictionary<Type, HashSet<Delegate>> callbacks;
        private object syncroot;
        private WebSocket webSocket;

        public TerminalWebSocketConnection(WebSocket webSocket)
        {
            this.webSocket = webSocket;
            callbacks = new Dictionary<Type, HashSet<Delegate>>();
            syncroot = new object();

            webSocket.OnMessage += WebSocket_OnMessage;
        }

        private void WebSocket_OnMessage(object sender, MessageEventArgs e)
        {
            var packetRaw = JsonConvert.DeserializeObject<JsonPacket>(e.Data);

            var packetType = PacketSystem.GetPacketType(packetRaw.name);

            if (packetType != null)
            {
                var packet = packetRaw.data.ToObject(packetType) as IBitSerialisable;

                HashSet<Delegate> handlers;
                if (callbacks.TryGetValue(packetType, out handlers))
                {
                    foreach (Delegate handler in handlers)
                        handler.DynamicInvoke(this, packet);
                }
                else
                {
                    Console.WriteLine("No event handlers for event '{0}'", packetRaw.name);
                }
            }
        }

        public void SubscribeToPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                HashSet<Delegate> set;
                if (!callbacks.TryGetValue(t, out set))
                {
                    set = new HashSet<Delegate>();
                    callbacks.Add(t, set);
                }
                set.Add(callback);
            }
        }

        public void UnsubscribeFromPacket<T>(PacketReceived<T> callback) where T : struct
        {
            Type t = typeof(T);
            lock (syncroot)
            {
                HashSet<Delegate> set;
                if (!callbacks.TryGetValue(t, out set)) return;
                set.Remove(callback);
            }
        }

        public void ClearSubscriptions()
        {
            lock (syncroot)
                callbacks.Clear();
        }

        public void SendPacket(IBitSerialisable packet)
        {
            var name = PacketSystem.GetPacketName(packet.GetType());

            var packetRaw = new JsonPacket
            {
                name = name,
                data = JObject.FromObject(packet)
            };

            webSocket.Send(JsonConvert.SerializeObject(packetRaw));
        }
    }
}
