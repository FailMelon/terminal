﻿using System;
using FastTestClient.Networking;
using WebSocketSharp;

namespace FastTestClient
{
    public class Client
    {
        private TerminalWebSocketConnection _netConnection;

        private bool _exit;

        public WebSocket WebSocket { get; private set; }

        public void Start()
        {
            WebSocket = new WebSocket("ws://localhost:22781/terminal");
            WebSocket.OnOpen += Ws_OnOpen;
            WebSocket.OnClose += Ws_OnClose;
            WebSocket.OnError += Ws_OnError;
            WebSocket.Connect();

            _exit = false;
            while (!_exit)
            {
                System.Threading.Thread.Sleep(100);
            }

        }

        private void Ws_OnError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine("Connection Error: " + e.Message);
        }

        private void Ws_OnClose(object sender, CloseEventArgs e)
        {
            _exit = true;
            Console.WriteLine("Connection closed (" + e.Reason + ")");
        }

        private void Ws_OnOpen(object sender, EventArgs e)
        {
            _netConnection = new TerminalWebSocketConnection(WebSocket);

            _netConnection.SubscribeToPacket<Server.Packets.EnterWorldPacket>(handleEnterWorldPacket);
            _netConnection.SubscribeToPacket<Server.Packets.WaitForInputPacket>(handleWaitForInputPacket);

            _netConnection.SendPacket(new Server.Packets.LoginPacket()
            {
                username = "Hello",
                password = "World"
            });


            Console.WriteLine("Connection succcess");
        }


        private void handleEnterWorldPacket(TerminalWebSocketConnection sender, Server.Packets.EnterWorldPacket pkt)
        {
            Console.WriteLine("Got enter world packet");
        }

        private void handleWaitForInputPacket(TerminalWebSocketConnection sender, Server.Packets.WaitForInputPacket pkt)
        {
            if (pkt.lineInput)
            {
                Console.CursorVisible = true;
                var input = Console.ReadLine();
                Console.CursorVisible = false;

                Server.Packets.LineInputPacket inputPkt;
                inputPkt.input = input;

                _netConnection.SendPacket(inputPkt);
            }
        }
    }
}
