﻿using System;
using System.Threading;
using System.Runtime.InteropServices;
using Tarul;
using System.Diagnostics;
using System.Reflection;

namespace FastTestClient
{
    class Program
    {
        public const int Clients = 1;

        private static Thread _serverThread;

        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        private static EventHandler _handler;




        private static bool Handler(CtrlType sig)
        {
            StopServer();
            return true;
        }

        private static void StopServer()
        {
            Console.WriteLine("Application exit detected, attempting to shutdown properly before closing.");
            Server.TerminalServer.Singleton.Stop();
            Server.TerminalServer.Singleton.ExitWait.WaitOne();
            _serverThread.Join();
        }


        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "client")
            {
                Console.WriteLine("Starting client...");

                Server.Packets.PacketSystem.Initialise(Assembly.GetAssembly(typeof(Server.Packets.PacketSystem)));

                var c = new Client();
                c.Start();
                return;
            }

            _serverThread = new Thread(ServerThread);

            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);

            _serverThread.Start();

            Server.TerminalServer.Singleton.InitialisedWait.WaitOne();
   
            for (int i = 0; i < Clients; i++)
                Process.Start(System.Reflection.Assembly.GetExecutingAssembly().Location, "client");
        }

        private static void ServerThread()
        {
            Console.WriteLine("Starting server...");
            Server.TerminalServer.Singleton.Run();
        }
    }
}
