﻿using GameAPI;
using GameAPI.IO;
using System;
using System.Collections.Generic;

namespace Services.SSH
{
    [Service("irc")]
    public class IRCService : MarshalByRefObject, IService
    {
        public enum ChatRank
        {
            Guest,
            Member,
            Moderator,
            Admin
        }

        public string Name => "IRC";

        public ushort Port => 6697;

        public IServiceManager Manager { get; set; }

        public DatabaseContent RanksDatabase { get; private set; }

        private IList<IUser> users;

        public void OnStart()
        {
            users = new List<IUser>();

            var usersFile = Manager.LocalComputer.GetBootDriver().GetRootDirectory().FindOrAddFile("ranks.db", true);
            if (usersFile != null)
            {
                if (usersFile.GetContent() is DatabaseContent db)
                    RanksDatabase = db;
                else
                    RanksDatabase = new DatabaseContent();

                RanksDatabase.SetReference(usersFile);
            }
        }

        public void OnConnect(IUser user, bool isOwner)
        {
            if (!RanksDatabase.Contains(user.Username))
            {
                if (isOwner)
                    RanksDatabase.Add(user.Username, ChatRank.Admin);
                else
                    RanksDatabase.Add(user.Username, ChatRank.Guest);
            }

            user.Console.Clear();

            // Top and bottom bars
            user.Console.BackgroundColor = new FuckMarshalColor(32, 74, 135);
            user.Console.Write(("Welcome to chat.hack!").PadRight(user.Console.WindowWidth, ' '));

            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 3);
            user.Console.Write(new string(' ', user.Console.WindowWidth));
            user.Console.ResetColor();

            // Left Seperator
            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);
            for (int i = 1; i < user.Console.WindowHeight - 3; i++)
            {
                user.Console.SetCursorPosition(19, i);

                user.Console.Write("|");
            }
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);


            // Right Seperator
            user.Console.ForegroundColor = new FuckMarshalColor(32, 74, 135);
            for (int i = 1; i < user.Console.WindowHeight - 3; i++)
            {
                user.Console.SetCursorPosition(user.Console.WindowWidth - 14, i);

                user.Console.Write("│");
            }
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            SendWelcome(user, user);

            users.Add(user);

            UsersUpdate(user);

            SendInputBox(user);

            foreach (IUser other in users)
            {
                other.Console.SaveCursorPosition();

                if (other != user)
                {
                    if (other is IAIChatBehaviour chatBehaviour)
                        chatBehaviour.OnUserConnect(user);

                    UsersUpdate(other);
                    other.Console.MoveBufferArea(0, 2, other.Console.WindowWidth - 15, other.Console.WindowHeight - 5, 0, 1);
                    SendWelcome(other, user);
                }

                other.Console.RestoreCursorPosition();
            }
        }

        public void UsersUpdate(IUser user)
        {
            for (int i = 1; i < user.Console.WindowHeight - 3; i++)
            {
                user.Console.SetCursorPosition(user.Console.WindowWidth - 12, i);

                if (users.Count >= i)
                {
                    var other = users[i - 1];

                    var rank = RanksDatabase.GetValue<ChatRank>(other.Username);

                    user.Console.ForegroundColor = GetRankColor(rank);

                    if (other.Username.Length >= 11)
                        user.Console.Write(other.Username.Substring(0, 11));
                    else
                        user.Console.Write(other.Username);
                }
                else
                {
                    user.Console.Write(new string(' ', 12));
                }
            }
        }

        public void SendInputBox(IUser user)
        {
            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 2);
            user.Console.ClearLine();

            user.Console.ForegroundColor = new FuckMarshalColor(0, 180, 180);

            user.Console.Write($"[{user.Username}(e)] ");

            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            user.Console.ReadLine();

        }

        public void SendChatMessage(IUser user, IUser chatter, string message)
        {
            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 4);
            user.Console.Write(DateTime.Now.ToLongTimeString());

            var rank = RanksDatabase.GetValue<ChatRank>(chatter.Username);

            user.Console.ForegroundColor = GetRankColor(rank);

            user.Console.SetCursorPosition(18 - Math.Min(chatter.Username.Length, 9), user.Console.WindowHeight - 4);
            if (chatter.Username.Length >= 9)
                user.Console.Write(chatter.Username.Substring(0, 9));
            else
                user.Console.Write(chatter.Username);

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);
            user.Console.Write(" |");
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            user.Console.ForegroundColor = new FuckMarshalColor(230, 230, 230);

            user.Console.SetCursorPosition(21, user.Console.WindowHeight - 4);
            user.Console.Write(message);

            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);
        }

        public void SendSystemMessage(IUser user, string message)
        {
            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 4);
            user.Console.Write(DateTime.Now.ToLongTimeString());

            user.Console.ForegroundColor = new FuckMarshalColor(200, 0, 0);

            user.Console.SetCursorPosition(12, user.Console.WindowHeight - 4);
            user.Console.Write("System");

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);
            user.Console.Write(" |");
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            user.Console.ForegroundColor = new FuckMarshalColor(230, 230, 230);

            user.Console.SetCursorPosition(21, user.Console.WindowHeight - 4);
            user.Console.Write(message);

            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);
        }


        public void SendWelcome(IUser user, IUser joined)
        {
            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 4);
            user.Console.Write(DateTime.Now.ToLongTimeString());

            user.Console.ForegroundColor = new FuckMarshalColor(115, 210, 22);
            user.Console.SetCursorPosition(15, user.Console.WindowHeight - 4);
            user.Console.Write("-->");

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);
            user.Console.Write(" |");
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            user.Console.SetCursorPosition(21, user.Console.WindowHeight - 4);

            var rank = RanksDatabase.GetValue<ChatRank>(joined.Username);
            user.Console.ForegroundColor = GetRankColor(rank);

            user.Console.Write(joined.Username);

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);

            user.Console.Write(" has joined");

            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);
        }

        public void SendLeaveMessage(IUser user, IUser joined)
        {
            user.Console.SetCursorPosition(0, user.Console.WindowHeight - 4);
            user.Console.Write(DateTime.Now.ToLongTimeString());

            user.Console.ForegroundColor = new FuckMarshalColor(204, 0, 0);
            user.Console.SetCursorPosition(15, user.Console.WindowHeight - 4);
            user.Console.Write("<--");

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);
            user.Console.Write(" |");
            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);

            user.Console.SetCursorPosition(21, user.Console.WindowHeight - 4);

            var rank = RanksDatabase.GetValue<ChatRank>(joined.Username);
            user.Console.ForegroundColor = GetRankColor(rank);

            user.Console.Write(joined.Username);

            user.Console.ForegroundColor = new FuckMarshalColor(78, 154, 6);

            user.Console.Write(" has quit");

            user.Console.ForegroundColor = new FuckMarshalColor(255, 255, 255);
        }

        public FuckMarshalColor GetRankColor(ChatRank rank)
        {
            switch (rank)
            {
                case ChatRank.Admin:
                    return new FuckMarshalColor(196, 160, 0);
                case ChatRank.Guest:
                    return new FuckMarshalColor(78, 154, 6);
                case ChatRank.Member:
                    return new FuckMarshalColor(78, 154, 6);
                case ChatRank.Moderator:
                    return new FuckMarshalColor(78, 154, 6);
                default:
                    return new FuckMarshalColor(78, 154, 6);
            }

        }

        public void OnDisconnect(IUser user)
        {
            user.Console.Clear();

            //user.Console.WriteLine($"Goodbye.");
            users.Remove(user);

            foreach (IUser other in users)
            {
                other.Console.SaveCursorPosition();

                if (other is IAIChatBehaviour chatBehaviour)
                    chatBehaviour.OnUserDisconnect(user);

                UsersUpdate(other);
                other.Console.MoveBufferArea(0, 2, other.Console.WindowWidth - 15, other.Console.WindowHeight - 5, 0, 1);
                SendLeaveMessage(other, user);

                other.Console.RestoreCursorPosition();

            }
        }

        public void OnReceiveAPICall(IService caller, IApiPacket packet)
        {

        }

        public void HandleInput(IUser user, IUserInput input)
        {
            if (input is UserLineInput lineInput)
            {
                if (lineInput.Line.StartsWith("!"))
                {
                    if (lineInput.Line == "!logout")
                    {
                        ComputerAPI.LocalComputer.Disconnect(user);
                    }
                    else
                    {
                        user.Console.MoveBufferArea(0, 2, user.Console.WindowWidth - 15, user.Console.WindowHeight - 5, 0, 1);
                        SendSystemMessage(user, $"'{lineInput.Line}' is not a recognized command use !help for commands list");
                    }
                }
                else
                {
                    user.Console.MoveBufferArea(0, 2, user.Console.WindowWidth - 15, user.Console.WindowHeight - 5, 0, 1);
                    SendChatMessage(user, user, lineInput.Line);
                    SendInputBox(user);

                    foreach (IUser other in users)
                    {
                        if (other != user)
                        {
                            other.Console.SaveCursorPosition();
                            if (other is IAIChatBehaviour chatBehaviour && other != user)
                                chatBehaviour.OnIncomingChat(user, lineInput.Line);

                            other.Console.MoveBufferArea(0, 2, other.Console.WindowWidth - 15, other.Console.WindowHeight - 5, 0, 1);
                            SendChatMessage(other, user, lineInput.Line);
                            other.Console.RestoreCursorPosition();
                        }
                    }
                }
            }


            Log.Debug("Wooot");
        }

        public bool Equals(IService a, IService b) => a.Name == b.Name;
        public int GetHashCode(IService service) => base.GetHashCode() ^ service.GetHashCode();
    }
}
