﻿using System.Linq;
using System.Net;

namespace Services.BasicEmail
{
    public class EmailAddress
    {
        public string User { get; private set; }
        public IPAddress Address { get; private set; }

        private EmailAddress(string user, IPAddress address)
        {
            User = user;
            Address = address;
        }

        public override string ToString()
        {
            return $"{User}@{Address}";
        }

        public static EmailAddress Parse(string toParse)
        {
            if (toParse.Contains('@'))
            {
                var split = toParse.Split('@');

                if (split.Length == 2)
                {
                    if (IPAddress.TryParse(split[1], out IPAddress address))
                    {
                        return new EmailAddress(split[0], address);
                    }
                }
            }

            return null;
        }

        public static bool TryParse(string toParse, out EmailAddress result)
        {
            if (toParse.Contains('@'))
            {
                var split = toParse.Split('@');

                if (split.Length == 2)
                {
                    if (IPAddress.TryParse(split[1], out IPAddress address))
                    {
                        result = new EmailAddress(split[0], address);
                        return true;
                    }
                }
            }

            result = null;
            return false;
        }
    }
}
