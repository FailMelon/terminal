﻿using GameAPI;
using GameAPI.IO;
using System;
using System.Collections.Generic;
using Tarul;

namespace Services.BasicEmail
{
    public struct EmailPacket : IApiPacket
    {
        public string Author, To, Subject, Message;

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Author) && !string.IsNullOrEmpty(To) && !string.IsNullOrEmpty(Subject) && !string.IsNullOrEmpty(Message);
        }
    }

    [Service("smtp")]
    public class BasicEmailService : MarshalByRefObject, IService
    {
        public string Name => "SMTP";

        public ushort Port => 25;

        public IServiceManager Manager { get; set; }

        public DatabaseContent UserDatabase { get; private set; }

        private IDictionary<IUser, StateManager> stateManager;

        private bool setupMode;

        public BasicEmailService()
        {
            stateManager = new Dictionary<IUser, StateManager>();
        } 

        public void OnStart()
        {
            var usersFile = Manager.LocalComputer.GetBootDriver().GetRootDirectory().FindOrAddFile("users.db", true);
            if (usersFile != null)
            {
                if (usersFile.GetContent() is DatabaseContent db)
                    UserDatabase = db;
                else
                    UserDatabase = new DatabaseContent();

                UserDatabase.SetReference(usersFile);

                if (!UserDatabase.Contains("admin"))
                    setupMode = true;
            }
        }

        public IDirectory RegisterUser(string user, string password)
        {
            UserDatabase.Add(user, password);
            UserDatabase.Save();

            return Manager.LocalComputer.GetBootDriver().GetRootDirectory().AddDirectory(user, true);
        }

        public void OnConnect(IUser user, bool isOwner)
        {
            user.Console.SetSlowMode(true);

            user.Console.Clear();

            var stateMgr = new StateManager();

            var loggedInMenu = stateMgr.RegisterState<LoggedInMenuState>();
            loggedInMenu.User = user;
            loggedInMenu.Email = this;
            loggedInMenu.StateManager = stateMgr;

            var regLogMenu = stateMgr.RegisterState<RegLogMenuState>();
            regLogMenu.User = user;
            regLogMenu.Email = this;
            regLogMenu.StateManager = stateMgr;
            
            stateMgr.RegisterAction<LoggedInMenuState>("HandleInput", (state, data) => (state as LoggedInMenuState).HandleInput((IUserInput)data));
            stateMgr.RegisterAction<RegLogMenuState>("HandleInput", (state, data) => (state as RegLogMenuState).HandleInput((IUserInput)data));

            stateManager.Add(user, stateMgr);

            if (setupMode)
            {
                user.Console.WriteLine("Welcome to basic email setup, please enter your admin password to complete the setup");
                user.Console.WriteLine($"Username: admin");
                user.Console.Write($"Password: ");
                user.Console.ReadLine();
            }
            else
                stateMgr.FSMTransist<RegLogMenuState>();
        }

        public void OnDisconnect(IUser user)
        {
            stateManager.Remove(user);

            user.Console.SetSlowMode(false);
        }

        public void OnReceiveAPICall(IService caller, IApiPacket packet)
        {
            if (packet is EmailPacket emailPacket)
            {
                if (emailPacket.IsValid())
                {
                    Manager.LocalComputer.GetBootDriver().GetRootDirectory().FindDirectory(emailPacket.To.ToLower())?.
                        AddFile(() => Guid.NewGuid().ToString("N").Substring(0, 4) + ".mail", true).
                        SetContent(new EmailContent
                        {
                            Subject = emailPacket.Subject,
                            Author = emailPacket.Author,
                            Content = emailPacket.Message,
                            TimeStamp = DateTime.Now
                        });
                }
            }
        }

        public void HandleInput(IUser user, IUserInput input)
        {
            if (setupMode)
            {
                if (input is UserLineInput lineInput)
                {
                    RegisterUser("admin", lineInput.Line);
                    setupMode = false;
                    if (stateManager.TryGetValue(user, out StateManager mgr))
                        mgr.FSMTransist<RegLogMenuState>();
                }
            }
            else
            {
                if (stateManager.TryGetValue(user, out StateManager mgr))
                    mgr.CallAction("HandleInput", input);
            }

        }

        public bool Equals(IService a, IService b) => a.Name == b.Name;
        public int GetHashCode(IService service) => base.GetHashCode() ^ service.GetHashCode();
    }
}
