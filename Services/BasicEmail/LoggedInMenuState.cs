﻿using GameAPI;
using GameAPI.IO;
using System.Text;
using Tarul;

namespace Services.BasicEmail
{
    public class LoggedInMenuState : IState
    {
        private enum State
        {
            Menu,
            Read,
            WritingTo,
            WritingSubject,
            WritingMessage
        }

        public IUser User { get; set; }

        public BasicEmailService Email { get; set; }

        public StateManager StateManager { get; set; }

        public string Username { get; set; }

        private UI.Menu menu, adminMenu;

        private State currentState;

        private string to, subject;
        private StringBuilder message;

        public void OnActivate()
        {
            menu = new UI.Menu();
            menu.AddOption(1, "Send Email").
                AddOption(2, "List Emails").
                AddOption(3, "Read Email").
                AddOption(4, "Logout").
                AddOption(5, "Reply", true).
                AddOption(0, "Disconnect").
                OnSelect = (int option) =>
                {
                    switch (option)
                    {
                        case 1:
                            SendEmail();
                            break;
                        case 2:
                            ListEmails();
                            break;
                        case 3:
                            ReadEmail();
                            break;
                        case 4:
                            StateManager.FSMTransist<RegLogMenuState>();
                            break;
                        case 5:
                            Reply();
                            break;
                        case 0:
                            ComputerAPI.LocalComputer.Disconnect(User);
                            break;
                        default:
                            {
                                User.Console.Clear();
                                SetMenuState();
                            }
                            break;
                    }
                };

            adminMenu = new UI.Menu();
            adminMenu.AddOption(1, "Send Email").
                AddOption(2, "List Emails").
                AddOption(3, "Read Email").
                AddOption(4, "Logout").
                AddOption(5, "Logout to ssh").
                AddOption(6, "Reply", true).
                AddOption(0, "Disconnect").
                OnSelect = (int option) =>
                {
                    switch (option)
                    {
                        case 1:
                            SendEmail();
                            break;
                        case 2:
                            ListEmails();
                            break;
                        case 3:
                            ReadEmail();
                            break;
                        case 4:
                            StateManager.FSMTransist<RegLogMenuState>();
                            break;
                        case 5:
                            ComputerAPI.LocalComputer.Connect(User, 22);
                            break;
                        case 6:
                            Reply();
                            break;
                        case 0:
                            ComputerAPI.LocalComputer.Disconnect(User);
                            break;
                        default:
                            {
                                User.Console.Clear();
                                SetMenuState();
                            }
                            break;
                    }
                };

            to = null;
            subject = null;
            message = null;

            User.Console.Clear();
            User.Console.WriteLine($"Welcome {Username} to the your email account!");
            SetMenuState();
        }

        private void SendEmail()
        {
            User.Console.WriteLine("Sending an email");
            User.Console.Write($"To: ");

            currentState = State.WritingTo;

            User.Console.ReadLine();
        }

        private void ListEmails()
        {
            var emails = Email.Manager.LocalComputer.GetBootDriver().
                GetRootDirectory().
                FindDirectory(Username).Files;

            int emailCount = 0;

            var builder = new StringBuilder();
            foreach (var email in emails)
            {
                if (email.GetContent("Author", "Subject", "TimeStamp") is EmailContent emailContent)
                {
                    builder.AppendLine($"{email.Name} - {emailContent.Author} : {emailContent.Subject} : {emailContent.TimeStamp.ToShortDateString()}");
                    emailCount++;
                }
            }

            User.Console.Clear();

            if (emailCount > 0)
            {
                if (emailCount == 1)
                    User.Console.WriteLine($"You have 1 email!");
                else
                    User.Console.WriteLine($"You have {emailCount} emails!");

                User.Console.Write(builder.ToString());
            }
            else
                User.Console.WriteLine("You have no emails!");


            SetMenuState();
        }

        private void ReadEmail()
        {
            User.Console.WriteLine("Enter id of email you want to read");
            User.Console.Write($"ID: ");

            currentState = State.Read;

            User.Console.ReadLine();
        }


        private void Reply()
        {
            currentState = State.WritingMessage;

            message = new StringBuilder();

            subject = "RE:" + subject;

            User.Console.WriteLine("To: " + to);
            User.Console.WriteLine("Subject: " + subject);
            User.Console.ReadLine();
        }


        public void SetMenuState()
        {
            if (Username == "admin")
                User.Console.WriteLine("\n" + adminMenu.Build());
            else
                User.Console.WriteLine("\n" + menu.Build());
            User.Console.Write($">");
            User.Console.ReadLine();

            currentState = State.Menu;
        }


        public void HandleInput(IUserInput input)
        {
            if (input is UserLineInput lineInput)
            {
                if (currentState == State.Menu)
                {
                    if (Username == "admin")
                        adminMenu?.HandleInput(input);
                    else
                        menu?.HandleInput(input);
                }
                else if (currentState == State.Read)
                {
                    var content = Email.Manager.LocalComputer.GetBootDriver().
                                        GetRootDirectory().
                                        FindDirectory(Username).
                                        FindFile(lineInput.Line)?.
                                        GetContent();

                    User.Console.Clear();

                    if (content is EmailContent emailContent)
                    {
                        to = emailContent.Author;
                        subject = emailContent.Subject;

                        if (content != null)
                            User.Console.WriteLine($"Author: {emailContent.Author}\nSubject: {emailContent.Subject}\nDate: {emailContent.TimeStamp}\n{emailContent.Content}");
                        else
                            User.Console.WriteLine($"No email by the code {lineInput.Line}");

                        menu.ShowOption(5);
                    }

                    SetMenuState();
                }
                else if (currentState == State.WritingTo)
                {
                    to = lineInput.Line;

                    User.Console.Write($"Subject: ");

                    currentState = State.WritingSubject;

                    User.Console.ReadLine();
                }
                else if (currentState == State.WritingSubject)
                {
                    subject = lineInput.Line;

                    currentState = State.WritingMessage;

                    message = new StringBuilder();
                    User.Console.ReadLine();
                }
                else if (currentState == State.WritingMessage)
                {
                    if (lineInput.Line == "!")
                    {
                        if (to != null && subject != null)
                        {
                            if (EmailAddress.TryParse(to, out EmailAddress address))
                            {
                                EmailPacket packet;
                                packet.Author = Username + "@" + Email.Manager.LocalComputer.Address;
                                packet.To = address.User.ToLower();
                                packet.Subject = subject;
                                packet.Message = lineInput.Line;

                                var successEmail = ComputerAPI.LocalComputer.SendAPICallToService<BasicEmailService>(Email, address.Address, packet);

                                User.Console.Clear();

                                if (successEmail)
                                    User.Console.WriteLine("Email sent!");
                                else
                                    User.Console.WriteLine($"Email service({address.Address}) is offline or does not exist!");
                            }
                            else
                            {
                                User.Console.Clear();
                                User.Console.WriteLine($"Invalid email address {to}, example: admin@1.1.1.1");
                            }
                        }

                        SetMenuState();
                    }
                    else
                    {
                        message.AppendLine(lineInput.Line);
                        User.Console.ReadLine();
                    }
                }
            }
        }

        public void OnDeactivate()
        {

        }
    }
}
