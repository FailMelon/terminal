﻿using GameAPI;
using GameAPI.IO;
using System;
using Tarul;

namespace Services.BasicEmail
{
    public class RegLogMenuState : IState
    {
        private enum State
        {
            Intro,
            Register,
            Login
        }

        public IUser User { get; set; }

        public BasicEmailService Email { get; set; }

        public StateManager StateManager { get; set; }

        private State currentState;

        private UI.Menu menu;

        private bool inputPassword;
        private string username;

        public RegLogMenuState()
        {
            menu = new UI.Menu();
            menu.IntroMessage($"Welcome to the basic email service!").
                AddOption(1, "Register").
                AddOption(2, "Login").
                AddOption(0, "Disconnect").
                OnSelect = (int option) =>
                {
                    switch (option)
                    {
                        case 1:
                            {
                                currentState = State.Register;

                                User.Console.Clear();

                                User.Console.WriteLine("Registering a new user");
                                User.Console.Write($"Username: ");
                                User.Console.ReadLine();
                            }
                            break;
                        case 2:
                            {
                                currentState = State.Login;

                                User.Console.Clear();

                                User.Console.WriteLine("Log into an account");
                                User.Console.Write($"Username: ");
                                User.Console.ReadLine();
                            }
                            break;
                        case 0:
                            {
                                ComputerAPI.LocalComputer.Disconnect(User);
                            }
                            break;
                        default:
                            {
                                User.Console.Clear();
                                User.Console.WriteLine(menu.Build());

                                User.Console.Write($">");
                                User.Console.ReadLine();
                            }
                            break;
                    }
                };
        }

        public void OnActivate()
        {
            inputPassword = false;
            username = null;

            currentState = State.Intro;

            User.Console.Clear();
            User.Console.WriteLine(menu.Build());

            User.Console.Write($">");
            User.Console.ReadLine();
        }


        public void HandleInput(IUserInput input)
        {
            if (input is UserLineInput lineInput)
            {
                if (currentState == State.Intro)
                {
                    menu.HandleInput(input);
                }
                else if (currentState == State.Register)
                {
                    if (inputPassword)
                    {
                        if (string.IsNullOrEmpty(lineInput.Line))
                        {
                            inputPassword = false;
                            username = null;

                            currentState = State.Intro;

                            User.Console.Clear();
                            User.Console.WriteLine("Invalid password or empty\n");
                            User.Console.WriteLine(menu.Build());

                            User.Console.Write($">");
                            User.Console.ReadLine();
                        }
                        else
                        {
                            if (Email.UserDatabase != null && !Email.UserDatabase.Contains(username))
                            {
                                Email.RegisterUser(username, lineInput.Line).
                                    AddFile(Guid.NewGuid().ToString("N").Substring(0, 4), true).
                                    SetContent(new EmailContent
                                    {
                                        Subject = "Welcome to your new email account!",
                                        Author = $"admin@{Email.Manager.LocalComputer.Address}",
                                        Content = $"Welcome to your new email account, your reference address is {username}@{Email.Manager.LocalComputer.Address}\nthis is what will show on all emails you send from this service.",
                                        TimeStamp = DateTime.Now
                                    });
                            }

                            OnActivate();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(lineInput.Line))
                        {
                            inputPassword = false;
                            username = null;

                            currentState = State.Intro;

                            User.Console.Clear();
                            User.Console.WriteLine("Invalid username or empty\n");
                            User.Console.WriteLine(menu.Build());

                            User.Console.Write($">");
                            User.Console.ReadLine();
                        }
                        else
                        {
                            inputPassword = true;
                            username = lineInput.Line.ToLower();

                            User.Console.WriteLine($"Password: ");
                            User.Console.ReadLine();
                        }
                    }
                }
                else if (currentState == State.Login)
                {
                    if (inputPassword)
                    {
                        if (Email.UserDatabase != null && Email.UserDatabase.TryGetValue(username, out string password))
                        {
                            if (password == lineInput.Line)
                            {
                                StateManager.Get<LoggedInMenuState>().Username = username;
                                StateManager.FSMTransist<LoggedInMenuState>();
                                return;
                            }
                        }


                        OnActivate();
                    }
                    else
                    {
                        inputPassword = true;
                        username = lineInput.Line;

                        User.Console.Write($"Password: ");
                        User.Console.ReadLine();
                    }
                }
            }
        }

        public void OnDeactivate()
        {

        }
    }
}
