﻿using GameAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.UI
{
    public class Menu : IUIElement
    {
        private string introMessage;

        private Dictionary<int, string> options;

        public Action<int> OnSelect;

        private Dictionary<int, bool> hiddenOptions;

        public Menu()
        {

            options = new Dictionary<int, string>();
            hiddenOptions = new Dictionary<int, bool>();
        }

        public Menu IntroMessage(string message)
        {
            introMessage = message;
            return this;
        }

        public Menu AddOption(int id, string option, bool secret = false)
        {
            hiddenOptions.Add(id, secret);
            options.Add(id, option);
            return this;
        }

        public Menu ShowOption(int option)
        {
            if (hiddenOptions.ContainsKey(option))
                hiddenOptions[option] = false;

            return this;
        }

        public Menu HideOption(int option)
        {
            if (hiddenOptions.ContainsKey(option))
                hiddenOptions[option] = true;

            return this;
        }

        public void HandleInput(IUserInput input)
        {
            if (input is UserLineInput lineInput)
            {
                if (int.TryParse(lineInput.Line, out int result))
                {
                    if (options.ContainsKey(result))
                    {
                        OnSelect?.Invoke(result);
                        return;
                    }
                }
            }

            OnSelect?.Invoke(-1);
        }

        public string Build()
        {
            StringBuilder builder = new StringBuilder();

            if (!string.IsNullOrEmpty(introMessage))
                builder.AppendLine(introMessage);

            foreach(var option in options)
            {
                if (option.Key != 0)
                    builder.AppendLine($"{option.Key}: {option.Value}");
            }
            if (options.TryGetValue(0, out string val))
                builder.AppendLine($"0: {val}");

            return builder.ToString();
        }
    }
}
