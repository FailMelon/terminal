﻿using GameAPI;

namespace Services.UI
{
    public interface IUIElement
    {
        void HandleInput(IUserInput input);
        string Build();
    }
}
